﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;

namespace CloudManager
{
    /// <summary>
    /// This will initiate the datarestoration in local system. This will be called very rarely even in most cases never.
    /// </summary>
    public static class RestoreFromServer
    {
        public static void Start()
        {
            //Testing
            //frmCustomSyncSettingParams frm = new frmCustomSyncSettingParams();
            //syncSettingsMaster frm = new syncSettingsMaster();
            //frm.ShowDialog();
            //return;
            //CloudSyncManager.SynchronizeAll();

            //Testing



            bool insertStatus = false;
            //start infinite loop for receiving unknown amount of data. and break it once done.
            string server_user_id = "0";

            //Added by Sumit for GTL#276   ---START
            int maxUniqueIDonServer = GetBsonFromServer.GetMaxUniqueIDFromServer();
            //Added by Sumit for GTL#276   ---END

            //Condition edited by Sumit for GTL#276   ---START
            //for (int uniqueId=1; true; uniqueId++)
            //int tempInt = 0;//TestingSumit 
            for (int uniqueId = 1; uniqueId <= maxUniqueIDonServer; uniqueId++)
            //Condition edited by Sumit for GTL#276   ---END
            {
                //TestingSumit START
                //tempInt++;
                //if(tempInt>8)
                //{

                //}
                //TestingSumit END
                DBInConsistencyResolved:
                //-------------Added By Sumit on 31-Dec-18 for GSP-983 START-------------
                if (uniqueId != 1)
                {
                    //it will fetch the last max avalable User_ID field of web server we are storing as server_user_id in local DB
                    server_user_id = GlobalItems.Get_Max_Server_User_ID(GlobalItems.MY_Reg_User_ID, GlobalItems.MY_StallID);
                }
                //-------------Added By Sumit on 31-Dec-18 for GSP-983 END---------------


                ////===First table patientdetails.
                //Patch Applied by Sumit for GTL#276   ---START          design need to be rectified instead of this.
                //Patch:
                //Patch Applied by Sumit for GTL#276   ---END
                string bsonPatient = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqPatientJson, uniqueId.ToString(), CloudUrls.PatientDetailsGetFromUrl, server_user_id);

                //================Edited by sumit for GSP-983 START============
                //if(bsonPatient.Contains("Data Doesnot exist"))
                //{
                //    break;
                //}
                if (bsonPatient.Contains("Data Doesnot exist") || bsonPatient.Contains("Failed due to Insufficient Data"))
                {
                    //Patch Applied by Sumit for GTL#276   ---START     design need to be rectified instead of this.
                    //break;
                    continue;
                    //if (uniqueId < 250)
                    //{
                    //    uniqueId++;
                    //    server_user_id = "0";
                    //    goto Patch;
                    //}
                    //else
                    //{
                    //    break;
                    //}
                    //Patch Applied by Sumit for GTL#276   ---END
                }
                //================Edited by sumit for GSP-983 END============



                if (bsonPatient.Length > 0)//--Added by Rajnish for GSP-676--//
                {
                    DataTable dtPat = BsonParser.SpliterPatientDetails(bsonPatient);//PatientDetailsBSONtoDT(bsonPatient);

                    //Added by Sumit on 06-Dec-2018 GSP-944---------------START
                    try
                    {
                        foreach (DataRow dr in dtPat.Rows)
                        {
                            //Added by Sumit GSP-1017---------START
                            //PatientID                           


                            //PatientId column
                            dr["PatientId"] = SplCharHandler.GetNormalString(dr["PatientId"].ToString());
                            if (dr["PatientId"].ToString().Contains("'"))
                            {
                                dr["PatientId"] = dr["PatientId"].ToString().Replace("'", @"''");
                            }
                            //Name column
                            dr["Name"] = SplCharHandler.GetNormalString(dr["Name"].ToString());
                            if (dr["Name"].ToString().Contains("'"))
                            {
                                dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                            }
                            //Comment Column

                            //Added by Sumit GTL#353   ---START
                            try
                            {
                                uniqueId = Convert.ToInt32(dr["uniqueId"].ToString());
                            }
                            catch (Exception strToIntEx)
                            {

                            }
                            finally
                            {

                            }
                            //Added by Sumit GTL#353   ---END

                            //Added by Sumit GSP-1017---------START
                            //PatientID
                            //Name
                            //comment
                            dr["Comment"] = SplCharHandler.GetNormalString(dr["Comment"].ToString());
                            string cmt = dr["Comment"].ToString();
                            string[] splter = { "xSTARTxcxoxmxmxexnxtx", "xENDxcxoxmxmxexnxtx" };
                            List<string> lstCmtParts = new List<string>(cmt.Split(splter, StringSplitOptions.RemoveEmptyEntries));
                            if (lstCmtParts.Count >= 3)
                            {
                                dr["Comment"] = lstCmtParts[lstCmtParts.Count - 2];
                            }
                            //Added by Sumit GSP-1017---------END
                            if (dr["Comment"].ToString().Contains("'"))
                            {
                                dr["Comment"] = dr["Comment"].ToString().Replace("'", @"''");
                            }
                        }
                    }
                    catch (Exception e1)
                    { }
                    //Added by Sumit on 06-Dec-2018 GSP-944---------------END


                    //-------------Added By Sumit on 28-Dec-18 for GSP-983 START-------------

                    for (int clNum = 0; clNum < dtPat.Columns.Count; clNum++)
                    {//Sumit: Code can be improved further, loop can be removed
                        if (dtPat.Columns[clNum].ColumnName.ToUpper() == "USER_ID")
                        {
                            dtPat.Columns[clNum].ColumnName = "SERVER_USER_ID";
                            break;
                        }
                    }
                    //-------------Added By Sumit on 28-Dec-18 for GSP-983 END---------------

                    string pdqr = DBHandler.QryConverter(dtPat, Tables.PatientDetails.ToString());
                    ////===insert into database 

                    insertStatus = DBHandler.ExecQry(pdqr, uniqueId.ToString(), Tables.PatientDetails);

                    try
                    {
                        // if(insertStatus)
                        {
                            string bsonkneedown = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqKneeDownJson, uniqueId.ToString(), CloudUrls.FrontBodyPositionKneeDownGetFromUrl, server_user_id);
                            DataTable dtknee = BsonParser.SpliterFrontBodyPositionKneedown(bsonkneedown);
                            ////===convert table to qry
                            string q = DBHandler.QryConverter(dtknee, Tables.FrontBodyPositionKneeDown.ToString());
                            ////===insert into database 
                            insertStatus = DBHandler.ExecQry(q, uniqueId.ToString(), Tables.FrontBodyPositionKneeDown);


                            string bsonstanding = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqStandingJson, uniqueId.ToString(), CloudUrls.FrontBodyPositionStandingGetFromUrl, server_user_id);
                            //DataTable dtStanding = BsonParser.ColumnNamesFrontBodyPositionStanding(bsonstanding);
                            DataTable dtStanding = BsonParser.SpliterFrontBodyPositionStanding(bsonstanding);
                            string r = DBHandler.QryConverter(dtStanding, Tables.FrontBodyPositionStanding.ToString());
                            //insert into database 
                            //**************
                            insertStatus = DBHandler.ExecQry(r, uniqueId.ToString(), Tables.FrontBodyPositionStanding);//**************


                            string bsonSideBody = GetBsonFromServer.getBsonFromServer(GetBsonFromServer.initReqsideJson, uniqueId.ToString(), CloudUrls.SideBodyPositionGetFromUrl, server_user_id);
                            //DataTable dtSideBody = BsonParser.PatientDetailsBSONtoDT(bsonSideBody);
                            DataTable dtSideBody = BsonParser.SpliterSideBodyPosition(bsonSideBody);
                            string s = DBHandler.QryConverter(dtSideBody, Tables.SideBodyPosition.ToString());
                            //insert into database 
                            insertStatus = DBHandler.ExecQry(s, uniqueId.ToString(), Tables.SideBodyPosition);

                        }
                    }
                    catch (Exception e2)
                    { }
                    finally
                    {

                    }
                }
                //else
                //{
                //    throw new Exception("sqlite db insert/update error");
                //}
                //Save 

                //Added by Sumit for GTL#353   ---START
                if (Utility.StabilizeSQLiteDatabase())
                {
                    goto DBInConsistencyResolved;
                }
                //Added by Sumit for GTL#353   ---END
            }
        }

        public static DataTable DownloadInactiveLicReports()
        {
            bool insertStatus = false;
            DataTable dtInactData = null;
            //start infinite loop for receiving unknown amount of data. and break it once done.

            int uniqueId = 0;
            string server_user_id = "0";

            bool bFirstTimeOnly = true;
            NextReport:
            string initReqPatientJsonInact = @"[{""TableName"":""" + Tables.PatientDetails.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";

            initReqPatientJsonInact = initReqPatientJsonInact.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                    .Replace("activationKeykgjyftyfjvjhg", "")
                                    .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                    .Replace("install_idkgjyftyfjvjhg", "0")
                                    .Replace("user_idkgjyftyfjvjhg", server_user_id);

            string bsonPatient = getBsonFromServerInact(initReqPatientJsonInact, uniqueId.ToString(), CloudUrls.PatientDetailsRecdinact, server_user_id);

            if (bsonPatient.Contains("Data Doesnot exist") || bsonPatient.Contains("Failed due to Insufficient Data"))
            {
                return dtInactData;
            }

            if (bsonPatient.Length > 0)
            {
                DataTable dtPat = BsonParser.SpliterPatientDetailsInact(bsonPatient);
                try
                {
                    foreach (DataRow dr in dtPat.Rows)
                    {
                        dr["PatientId"] = SplCharHandler.GetNormalString(dr["PatientId"].ToString());
                        if (dr["PatientId"].ToString().Contains("'"))
                        {
                            dr["PatientId"] = dr["PatientId"].ToString().Replace("'", @"''");
                        }

                        dr["Name"] = SplCharHandler.GetNormalString(dr["Name"].ToString());
                        if (dr["Name"].ToString().Contains("'"))
                        {
                            dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                        }

                        try
                        {
                            uniqueId = 0;//Convert.ToInt32(dr["uniqueId"].ToString());
                        }
                        catch (Exception strToIntEx)
                        {

                        }
                        finally
                        {

                        }

                        dr["Comment"] = SplCharHandler.GetNormalString(dr["Comment"].ToString());
                        string cmt = dr["Comment"].ToString();
                        string[] splter = { "xSTARTxcxoxmxmxexnxtx", "xENDxcxoxmxmxexnxtx" };
                        List<string> lstCmtParts = new List<string>(cmt.Split(splter, StringSplitOptions.RemoveEmptyEntries));
                        if (lstCmtParts.Count >= 3)
                        {
                            dr["Comment"] = lstCmtParts[lstCmtParts.Count - 2];
                        }

                        if (dr["Comment"].ToString().Contains("'"))
                        {
                            dr["Comment"] = dr["Comment"].ToString().Replace("'", @"''");
                        }
                    }
                }
                catch (Exception e1)
                { }

                server_user_id = dtPat.Rows[dtPat.Rows.Count - 1]["user_id"].ToString();

                if (bFirstTimeOnly)
                {
                    dtInactData = dtPat.Clone();
                    foreach (DataRow drPattbl in dtPat.Rows)
                    {
                        dtInactData.Rows.Add(drPattbl.ItemArray);
                        bFirstTimeOnly = false;
                    }
                }
                else
                {
                    foreach (DataRow drPattbl in dtPat.Rows)
                    {
                        dtInactData.Rows.Add(drPattbl.ItemArray);
                    }
                }
                goto NextReport;
            }
            return dtInactData;
        }

        public static string getBsonFromServerInact(string jsonQuery, string uniqueID, string url, string server_user_id = "")
        {
            var queryString = jsonQuery;
            if (queryString.Contains("#") && uniqueID != "0")// && server_user_id.Length==0)
            {
                queryString = queryString.Replace("#", uniqueID);
                queryString = queryString.Replace("user_idkgjyftyfjvjhg", server_user_id);
            }
            else
            {
                queryString = queryString.Replace("#", uniqueID);
                queryString = queryString.Replace("user_idkgjyftyfjvjhg", server_user_id);
                //Call is from Other PC's data (Multilicense)
            }

            #region sample JSON
            //var studentObject = queryString;//Newtonsoft.Json.JsonConvert.DeserializeObject(@"[{""TableName"":""PatientDetails"",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""1""}]");//queryString);
            #endregion
            //3.
            TryAgain:
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
            JsonSerializer jsonSerializer = new JsonSerializer();
            //4.
            MemoryStream objBsonMemoryStream = new MemoryStream();
            //5.
            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
            //6.
            jsonSerializer.Serialize(bsonWriterObject, studentObject); //studentObject);

            byte[] requestByte = objBsonMemoryStream.ToArray();

            WebRequest webRequest = WebRequest.Create(url);//@"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data");//CloudUrls.FrontBodyPositionKneeDownSendToUrl);
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;

            // create our stram to send
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {
                throw ex;
                goto TryAgain;
            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();

            string tp = webResponse.ContentType;
            try
            {
                webDataStream = webResponse.GetResponseStream();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();
            return responseFromServer;
        }
    }
}
