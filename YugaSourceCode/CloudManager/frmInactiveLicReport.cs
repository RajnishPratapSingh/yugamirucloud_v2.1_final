﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CloudManager
{
    public partial class frmInactiveLicReport : Form
    {
        public frmInactiveLicReport()
        {
            InitializeComponent();
        }
        DataTable dtRecords = new DataTable();
        private void frmInactiveLicReport_Load(object sender, EventArgs e)
        {
            try
            {
                DBHandler.IsDownloadingInactRept = false;
                chkSelectAll.Text = CloudManager.Properties.Resources.INACT_SELECTALL;
                btnDownload.Text = CloudManager.Properties.Resources.INACT_DOWNLOAD;
                lblName.Text = CloudManager.Properties.Resources.INACT_NAME;
                lblComputerName.Text = CloudManager.Properties.Resources.PC_NAME;
                lblInstallationName.Text = CloudManager.Properties.Resources.INSTALLATION_NAME;

                Cursor.Current = Cursors.WaitCursor;
                dtRecords = CloudManager.RestoreFromServer.DownloadInactiveLicReports();
                if (dtRecords.Rows.Count > 0)
                {
                    DataView view = new DataView(dtRecords);
                    view.Sort = "Measurementtime DESC";
                    dataGridView1.DataSource = view;

                    foreach (DataGridViewRow row in dataGridView1.Rows)
                    {
                        row.Cells["UniqueId"].Value = (row.Index + 1).ToString();
                        row.Cells["colchkbx"].Value = false;
                    }                        

                    //dataGridView1.DataSource = dtRecords;
                    SetDGV_InactRept();
                }
                Cursor.Current = Cursors.Default;
            }
            catch { }
        }

        public void SetDGV_InactRept()
        {
            dataGridView1.Columns["colchkbx"].DisplayIndex = 0;
            dataGridView1.Columns["colchkbx"].Width = 50;
            //dataGridView1.Columns["colchkbx"].HeaderText = "SrNo.";

            dataGridView1.Columns["UniqueId"].DisplayIndex = 1;
            dataGridView1.Columns["UniqueId"].HeaderText = "SrNo.";
            dataGridView1.Columns["UniqueId"].Width = 50;

            dataGridView1.Columns["PatientId"].DisplayIndex = 2;
            dataGridView1.Columns["PatientId"].HeaderText = "Id";
            dataGridView1.Columns["PatientId"].Width = 70;

            dataGridView1.Columns["Name"].DisplayIndex = 3;
            dataGridView1.Columns["Name"].HeaderText = "Name";
            dataGridView1.Columns["Name"].Width = 100;

            dataGridView1.Columns["MeasurementTime"].DisplayIndex = 4;
            dataGridView1.Columns["MeasurementTime"].HeaderText = "Date of Creation";
            dataGridView1.Columns["MeasurementTime"].Width = 70;

            dataGridView1.Columns["Computername"].DisplayIndex = 5;
            dataGridView1.Columns["Computername"].HeaderText = "PC Name";
            dataGridView1.Columns["Computername"].Width = 130;

            dataGridView1.Columns["Installation_name"].DisplayIndex = 6;
            dataGridView1.Columns["Installation_name"].HeaderText = "Installation Name";
            dataGridView1.Columns["Installation_name"].Width = 130;

            dataGridView1.Columns["user_id"].Visible = false;
            dataGridView1.Columns["reg_user_id"].Visible = false;
            dataGridView1.Columns["update_counter"].Visible = false;
            dataGridView1.Columns["BenchmarkDistance"].Visible = false;
            dataGridView1.Columns["comment"].Visible = false;
            dataGridView1.Columns["DOB"].Visible = false;
            dataGridView1.Columns["Date"].Visible = false;
            dataGridView1.Columns["Gender"].Visible = false;
            dataGridView1.Columns["Height"].Visible = false;
            dataGridView1.Columns["Month"].Visible = false;
            dataGridView1.Columns["Year"].Visible = false;
            dataGridView1.Columns["Activationkey"].Visible = false;
        }

        private void btnDownload_Click(object sender, EventArgs e)
        {
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                bool insertStatus = false;
                bool IsReportdownloaded = false;
                for (int i = 0; i < dataGridView1.Rows.Count; i++)
                {
                    bool IsRowchecked = Convert.ToBoolean(dataGridView1.Rows[i].Cells["colchkbx"].Value.ToString());
                    if (IsRowchecked)
                    {
                        string maxLocalUniqueID = DBHandler.GetMax_UniqueID().Trim();
                        string nextMaxUID = (Convert.ToUInt32(maxLocalUniqueID) + 1).ToString();
                        string uniqueId = nextMaxUID;//dataGridView1.Rows[i].Cells["uniqueId"].Value.ToString();
                        string server_user_id = dataGridView1.Rows[i].Cells["user_id"].Value.ToString();

                        string initReqPatientJsonInact = @"[{""TableName"":""" + Tables.PatientDetails.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";

                        initReqPatientJsonInact = initReqPatientJsonInact.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                            .Replace("activationKeykgjyftyfjvjhg", "")
                            .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                            .Replace("install_idkgjyftyfjvjhg", "0")
                            .Replace("user_idkgjyftyfjvjhg", server_user_id);

                        string bsonPatient = CloudManager.RestoreFromServer.getBsonFromServerInact(initReqPatientJsonInact, uniqueId.ToString(), CloudUrls.PatientDetailsRecdinactbyserverid, server_user_id);

                        if (bsonPatient.Contains("Data Doesnot exist") || bsonPatient.Contains("Failed due to Insufficient Data"))
                        {
                            continue;
                        }
                        if (bsonPatient.Length > 0)
                        {
                            DataTable dtPat = BsonParser.SpliterPatientDetails(bsonPatient);//PatientDetailsBSONtoDT(bsonPatient);
                            try
                            {
                                foreach (DataRow dr in dtPat.Rows)
                                {
                                    dr["PatientId"] = SplCharHandler.GetNormalString(dr["PatientId"].ToString());
                                    if (dr["PatientId"].ToString().Contains("'"))
                                    {
                                        dr["PatientId"] = dr["PatientId"].ToString().Replace("'", @"''");
                                    }
                                    //Name column
                                    dr["Name"] = SplCharHandler.GetNormalString(dr["Name"].ToString());
                                    if (dr["Name"].ToString().Contains("'"))
                                    {
                                        dr["Name"] = dr["Name"].ToString().Replace("'", @"''");
                                    }
                                    //Comment Column
                                    try
                                    {
                                        dr["UniqueId"] = nextMaxUID;
                                        uniqueId = nextMaxUID;
                                    }
                                    catch (Exception strToIntEx)
                                    {

                                    }
                                    //comment
                                    dr["Comment"] = SplCharHandler.GetNormalString(dr["Comment"].ToString());
                                    string cmt = dr["Comment"].ToString();
                                    string[] splter = { "xSTARTxcxoxmxmxexnxtx", "xENDxcxoxmxmxexnxtx" };
                                    List<string> lstCmtParts = new List<string>(cmt.Split(splter, StringSplitOptions.RemoveEmptyEntries));
                                    if (lstCmtParts.Count >= 3)
                                    {
                                        dr["Comment"] = lstCmtParts[lstCmtParts.Count - 2];
                                    }

                                    if (dr["Comment"].ToString().Contains("'"))
                                    {
                                        dr["Comment"] = dr["Comment"].ToString().Replace("'", @"''");
                                    }
                                }
                            }
                            catch (Exception e1)
                            { }

                            for (int clNum = 0; clNum < dtPat.Columns.Count; clNum++)
                            {//Sumit: Code can be improved further, loop can be removed
                                if (dtPat.Columns[clNum].ColumnName.ToUpper() == "USER_ID")
                                {
                                    dtPat.Columns[clNum].ColumnName = "SERVER_USER_ID";
                                    break;
                                }
                            }

                            DBHandler.IsDownloadingInactRept = true;
                            string pdqr = DBHandler.QryConverter(dtPat, Tables.PatientDetails.ToString());
                            ////===insert into database 
                            insertStatus = DBHandler.ExecQry(pdqr, uniqueId.ToString(), Tables.PatientDetails);
                            try
                            {
                                if (insertStatus)
                                {
                                    string initReqKneeDownJsonInact = @"[{""TableName"":""" + Tables.FrontBodyPositionKneeDown.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";

                                    initReqKneeDownJsonInact = initReqKneeDownJsonInact.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                        .Replace("activationKeykgjyftyfjvjhg", "")
                                        .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                        .Replace("install_idkgjyftyfjvjhg", "0")
                                        .Replace("user_idkgjyftyfjvjhg", server_user_id);

                                    string bsonkneedown = CloudManager.RestoreFromServer.getBsonFromServerInact(initReqKneeDownJsonInact, uniqueId.ToString(), CloudUrls.FrontBodyPositionKneeDownRecdinactbyserverid, server_user_id);
                                    DataTable dtknee = BsonParser.SpliterFrontBodyPositionKneedown(bsonkneedown);

                                    foreach (DataRow dr in dtknee.Rows)
                                    {
                                        dr["UniqueId"] = nextMaxUID;
                                    }

                                    ////===convert table to qry
                                    string q = DBHandler.QryConverter(dtknee, Tables.FrontBodyPositionKneeDown.ToString());
                                    ////===insert into database 
                                    insertStatus = DBHandler.ExecQry(q, uniqueId.ToString(), Tables.FrontBodyPositionKneeDown);

                                    string initReqStandingJsonInact = @"[{""TableName"":""" + Tables.FrontBodyPositionStanding.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";

                                    initReqStandingJsonInact = initReqStandingJsonInact.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                        .Replace("activationKeykgjyftyfjvjhg", "")
                                        .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                        .Replace("install_idkgjyftyfjvjhg", "0")
                                        .Replace("user_idkgjyftyfjvjhg", server_user_id);

                                    string bsonstanding = CloudManager.RestoreFromServer.getBsonFromServerInact(initReqStandingJsonInact, uniqueId.ToString(), CloudUrls.FrontBodyPositionStandingRecdinactbyserverid, server_user_id);
                                    //DataTable dtStanding = BsonParser.ColumnNamesFrontBodyPositionStanding(bsonstanding);
                                    DataTable dtStanding = BsonParser.SpliterFrontBodyPositionStanding(bsonstanding);

                                    foreach (DataRow dr in dtStanding.Rows)
                                    {
                                        dr["UniqueId"] = nextMaxUID;
                                    }
                                    string r = DBHandler.QryConverter(dtStanding, Tables.FrontBodyPositionStanding.ToString());
                                    //insert into database 
                                    //**************
                                    insertStatus = DBHandler.ExecQry(r, uniqueId.ToString(), Tables.FrontBodyPositionStanding);//**************

                                    string initReqsideJsonInact = @"[{""TableName"":""" + Tables.SideBodyPosition.ToString() + @""",""Computerid"":""ComputerIdhgjyfjhg"",""Activationkey"":""activationKeykgjyftyfjvjhg"",""UniqueId"":""#"",""user_id"":""user_idkgjyftyfjvjhg"",""reg_user_id"":""reg_user_idkgjyftyfjvjhg"",""Install_id"":""install_idkgjyftyfjvjhg""}]";

                                    initReqsideJsonInact = initReqsideJsonInact.Replace("ComputerIdhgjyfjhg", GlobalItems.ComputerID)
                                        .Replace("activationKeykgjyftyfjvjhg", "")
                                        .Replace("reg_user_idkgjyftyfjvjhg", GlobalItems.MY_Reg_User_ID)
                                        .Replace("install_idkgjyftyfjvjhg", "0")
                                        .Replace("user_idkgjyftyfjvjhg", server_user_id);

                                    string bsonSideBody = CloudManager.RestoreFromServer.getBsonFromServerInact(initReqsideJsonInact, uniqueId.ToString(), CloudUrls.SideBodyPositionRecdinactbyserverid, server_user_id);
                                    //DataTable dtSideBody = BsonParser.PatientDetailsBSONtoDT(bsonSideBody);
                                    DataTable dtSideBody = BsonParser.SpliterSideBodyPosition(bsonSideBody);

                                    foreach (DataRow dr in dtSideBody.Rows)
                                    {
                                        dr["UniqueId"] = nextMaxUID;
                                    }

                                    string s = DBHandler.QryConverter(dtSideBody, Tables.SideBodyPosition.ToString());
                                    //insert into database 
                                    insertStatus = DBHandler.ExecQry(s, uniqueId.ToString(), Tables.SideBodyPosition);
                                    IsReportdownloaded = true;
                                }

                                //--Added by Rajnish for GSP-1465----------Start---//
                                try
                                {                                    
                                    if (WebComCation.Utility.IsInternetConnected())
                                    {
                                        CloudManager.StartUpdateToServer.StartSending();
                                    }
                                }
                                catch (Exception ex)
                                {
                                    //exception at taking cloud backup
                                }
                                //--Added by Rajnish for GSP-1465----------END---//
                            }
                            catch (Exception e2)
                            { }
                            finally
                            {

                            }
                        }
                    }
                }
                Cursor.Current = Cursors.Default;
                if (IsReportdownloaded)
                {
                    MessageBox.Show(CloudManager.Properties.Resources.INACT_REPORTS_DOWNLOADED);
                    DBHandler.IsDownloadingInactRept = false;
                    this.Close();
                }

            }
            catch { }
        }

        private void chkSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                //Necessary to end the edit mode of the Cell.
                dataGridView1.EndEdit();
                //Loop and check and uncheck all row CheckBoxes based on Header Cell CheckBox.
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    DataGridViewCheckBoxCell checkBox = (row.Cells["colchkbx"] as DataGridViewCheckBoxCell);
                    checkBox.Value = chkSelectAll.Checked;
                }
            }
            catch (Exception ex)
            { }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //--Added By Rajnish for GTL #442---Start--//
                var textboxSender = (TextBox)sender;
                var cursorPosition = textboxSender.SelectionStart;
                textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");
                textboxSender.SelectionStart = cursorPosition;
                //--Added By Rajnish for GTL #442---Start--//

                if (dtRecords.Rows.Count > 0)
                {
                    DataView view = new DataView(dtRecords);
                    string str = string.Empty;
                    if (textBox2.Text != "")
                        str = "name Like '%" + textBox2.Text + "%' and ";
                    view.RowFilter = str + "PatientId Like '%" + textBox1.Text + "%'";
                    view.Sort = "Measurementtime DESC";
                    dataGridView1.DataSource = view;
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                        row.Cells["UniqueId"].Value = (row.Index + 1).ToString();
                    SetDGV_InactRept();
                }
            }
            catch { }
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //--Added By Rajnish for GTL #442---Start--//
                var textboxSender = (TextBox)sender;
                var cursorPosition = textboxSender.SelectionStart;
                textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
                //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");
                textboxSender.SelectionStart = cursorPosition;
                //--Added By Rajnish for GTL #442---Start--//

                if (dtRecords.Rows.Count > 0)
                {
                    DataView view = new DataView(dtRecords);
                    string str = string.Empty;
                    if (textBox1.Text != "")
                        str = "patientid like '%" + textBox1.Text + "%' and ";
                    view.RowFilter = str + "name Like '%" + textBox2.Text + "%'";
                    view.Sort = "Measurementtime DESC";
                    dataGridView1.DataSource = view;
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                        row.Cells["UniqueId"].Value = (row.Index + 1).ToString();
                    SetDGV_InactRept();
                }
            }
            catch { }
        }

        private void txtCompName_TextChanged(object sender, EventArgs e)
        {
            try
            {
                //--Added By Rajnish for GTL #442---Start--//
                var textboxSender = (TextBox)sender;
                var cursorPosition = textboxSender.SelectionStart;
                textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");
                textboxSender.SelectionStart = cursorPosition;
                //--Added By Rajnish for GTL #442---Start--//

                if (dtRecords.Rows.Count > 0)
                {
                    DataView view = new DataView(dtRecords);
                    string str = string.Empty;
                    if (txtInstallationName.Text != "")
                        str = "Installation_name Like '%" + txtInstallationName.Text + "%' and ";
                    view.RowFilter = str + "Computername Like '%" + txtCompName.Text + "%'";
                    view.Sort = "Measurementtime DESC";
                    dataGridView1.DataSource = view;
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                        row.Cells["UniqueId"].Value = (row.Index + 1).ToString();
                    SetDGV_InactRept();
                }
            }
            catch { }
        }

        private void txtInstallationName_TextChanged(object sender, EventArgs e)
        {
            try
            { 
                //--Added By Rajnish for GTL #442---Start--//
                var textboxSender = (TextBox)sender;
                var cursorPosition = textboxSender.SelectionStart;
                textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
                //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");
                textboxSender.SelectionStart = cursorPosition;
                //--Added By Rajnish for GTL #442---Start--//

                if (dtRecords.Rows.Count > 0)
                {
                    DataView view = new DataView(dtRecords);
                    string str = string.Empty;
                    if (txtCompName.Text != "")
                        str = "Computername Like '%" + txtCompName.Text + "%' and ";
                    view.RowFilter = str + "Installation_name Like '%" + txtInstallationName.Text + "%'";
                    view.Sort = "Measurementtime DESC";
                    dataGridView1.DataSource = view;
                    foreach (DataGridViewRow row in dataGridView1.Rows)
                        row.Cells["UniqueId"].Value = (row.Index + 1).ToString();
                    SetDGV_InactRept();
                }
            }
            catch { }
        }
    }
}
