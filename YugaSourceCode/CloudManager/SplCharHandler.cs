﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CloudManager
{
    /// <summary>
    /// Class added by Sumit Srivastava on 06-Dec-2018 to handle Special Characters with simple code
    /// </summary>
    public static class SplCharHandler
    {
        static DataTable dt;
        /// <summary>
        /// Static Constructor: Can add or edit any special characters in future
        /// </summary>
        static SplCharHandler()
        {
            Type type = Type.GetType("System.String");
            dt = new DataTable();

            dt.Columns.Add("splCharValue", type);
            dt.Columns.Add("splCharCode", type);
            dt.Rows.Add("`", "Z1XXXX1Z");
            dt.Rows.Add("~", "Z2XXXX2Z");
            dt.Rows.Add("!", "Z3XXXX3Z");
            dt.Rows.Add("@", "Z4XXXX4Z");
            dt.Rows.Add("#", "Z5XXXX5Z");
            dt.Rows.Add("$", "Z6XXXX6Z");
            dt.Rows.Add("%", "Z7XXXX7Z");
            dt.Rows.Add("^", "Z8XXXX8Z");
            dt.Rows.Add("&", "Z9XXXX9Z");
            dt.Rows.Add("*", "Z10XXXX10Z");
            dt.Rows.Add("(", "Z11XXXX11Z");
            dt.Rows.Add(")", "Z12XXXX12Z");
            dt.Rows.Add("_", "Z13XXXX13Z");
            dt.Rows.Add("+", "Z14XXXX14Z");
            dt.Rows.Add("=", "Z15XXXX15Z");
            dt.Rows.Add("[", "Z16XXXX16Z");
            dt.Rows.Add("{", "Z17XXXX17Z");
            dt.Rows.Add("]", "Z18XXXX18Z");
            dt.Rows.Add("}", "Z19XXXX19Z");
            dt.Rows.Add(":", "Z20XXXX20Z");
            dt.Rows.Add(";", "Z21XXXX21Z");
            dt.Rows.Add("'", "Z22XXXX22Z");
            dt.Rows.Add('"'.ToString(), "Z23XXXX23Z");
            dt.Rows.Add("<", "Z24XXXX24Z");
            dt.Rows.Add(",", "Z25XXXX25Z");
            dt.Rows.Add(".", "Z26XXXX26Z");
            dt.Rows.Add(">", "Z27XXXX27Z");
            dt.Rows.Add("/", "Z28XXXX28Z");
            dt.Rows.Add("?", "Z29XXXX29Z");
            dt.Rows.Add("-", "Z30XXXX30Z");
            dt.Rows.Add("|", "Z31XXXX31Z");
            dt.Rows.Add("\\", "Z32XXXX32Z");

            //GSP-1017: Added by Sumit for improved robustness.-------START

            //Nodes in Incoming BSON content
            dt.Rows.Add("Status", "kSktkaktkuksk");
            dt.Rows.Add("TableName", "kTkakbklkekNkakmkek");
            dt.Rows.Add("Computerid", "kCkokmkpkuktkekrkikdk");
            dt.Rows.Add("Activationkey", "kAkcktkikvkaktkikoknkkkekyk");
            dt.Rows.Add("UniqueId", "kUknkikqkukekIkdk");
            dt.Rows.Add("user_id", "kukskekrk_kikdk");
            dt.Rows.Add("reg_user_id", "krkekgk_kukskekrk_kikdk");

            //PatientDetails Table
            dt.Rows.Add("update_counter", "kukpkdkaktkek_kckokuknktkekrk");
            dt.Rows.Add("BenchmarkDistance", "kBkeknkckhkmkakrkkkDkiksktkaknkckek");
            dt.Rows.Add("Comment", "kCkokmkmkeknktk");
            dt.Rows.Add("DOB", "kDkOkBk");
            dt.Rows.Add("Date", "kDkOkBk");
            dt.Rows.Add("Gender", "kGkeknkdkekrk");
            dt.Rows.Add("Height", "kHkekikgkhktk");
            dt.Rows.Add("MeasurementTime", "kMkekakskukrkekmkeknktkTkikmkek");
            dt.Rows.Add("Month", "kMkoknktkhk");
            dt.Rows.Add("Name", "kNkakmkek");
            dt.Rows.Add("PatientId", "kPkaktkikeknktkIkdk");
            dt.Rows.Add("Year", "kYkekakrk");
            //GSP-1017: -----END

        }

        /// <summary>
        /// Converts the User input string (with or without special characters) into coded string
        /// </summary>
        /// <param name="UserInputString"></param>
        /// <returns></returns>
        public static string GetCodedString(string UserInputString)
        {
            string struip = UserInputString;
            foreach (DataRow dr in dt.Rows)
            {
                string splCharVal = dr[0].ToString();
                string splCharCode = dr[1].ToString();
                struip = struip.Replace(splCharVal, splCharCode);
            }
            return struip;
        }


        /// <summary>
        /// Converts the string which contains coded string (with or without special characters)
        /// into normal (original string which was entered by user)
        /// </summary>
        /// <param name="CodedString">The string which was coded with this class's GetCodedString function</param>
        /// <returns></returns>
        public static string GetNormalString(string CodedString)
        {

            string struip = CodedString;
            foreach (DataRow dr in dt.Rows)
            {
                string splCharVal = dr[0].ToString();
                string splCharCode = dr[1].ToString();
                struip = struip.Replace(splCharCode, splCharVal);
            }
            return struip;
        }
    }
}
