﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CloudManager
{
    public static class StartUpdateToServer
    {
        public static bool IsAnForeignRecord { get; set; } //Added by RPS for GSP-1104    
        
        public static void StartSending()
        {
            //get updated record's ID from local database
            //List<string> lstids = AppDataReader.GetUniqueIDsNeedToUpdate();//Commented by RPS for GSP-1104
            //--Added by Rajnish for GSP-1104--//
            List<string> lstids = null;
            //get updated record's ID from local database
            //Edited by Sumit for GSP-1161   START
            //if (IsAnForeignRecord)
            //    lstids = AppDataReader.GetOwnersUniqueIDsNeedToUpdate();
            //else
            //    lstids = AppDataReader.GetUniqueIDsNeedToUpdate();

            lstids = AppDataReader.GetUniqueIDsNeedToUpdate();

            //Edited by Sumit for GSP-1161   END
            //--------------------------------//
            if (lstids.Count == 0)
                return;

            //do for each id
            foreach (string id in lstids)
            {
                //get updated row of this id from 4 tables
                //Edited by Sumit for GSP-1161   START
                //Check if this is a foreign record


                //Edited by Sumit for GSP-1161   END


                DataTable dtPatient = AppDataReader.GetPatientDetails(id);


                //get json and send to server one by one (to reduce memory consumption)
                //upload patient table
                //Added by Sumit for GSP-1161   ---START
                //Set ID as per record local or foreign status
                try
                {
                    dtPatient.Rows[0]["UniqueId"] = AppDataReader.GetOwnersUniqueID(id);
                }
                catch(Exception ex1)
                {
                    throw ex1;
                }
                //Added by Sumit for GSP-1161   ---END

                //Edited by Sumit GSP-1161   ---START
                //ConvertTableToJSON convertPatient = new ConvertTableToJSON(dtPatient, Tables.PatientDetails, RowNumToJSON.FirstRow);
                ConvertTableToJSON convertPatient = new ConvertTableToJSON(dtPatient, Tables.PatientDetails, RowNumToJSON.FirstRow, id);
                //Edited by Sumit GSP-1161   ---START

                bool uploadPatientResult = JSONManager.JsonTOBsonSender(convertPatient.strJonToSend, CloudUrls.PatientDetailsSendToUrl, id);
                if (uploadPatientResult)
                {
                    //Commented by Sumit for GSP-1161   ---START
                    //if (StartUpdateToServer.IsAnForeignRecord)
                      //  GlobalItems.SetForignInstallIDs(id);
                    //Commented by Sumit for GSP-1161   ---END

                    DataTable dtKneeDown = AppDataReader.GetFrontBodyPositionKneeDown(id);
                    //upload kneedown table.

                    //Sumit V2.0 on 08-May-18 
                    //add some string values to imagesbytes data (at data start and at data end) 
                    dtKneeDown = DBHandler.AddToImageBytesBeforeSend(dtKneeDown);




                    //Added by Sumit for GSP-1161   ---START
                    //Set ID as per record local or foreign status
                    dtKneeDown.Rows[0]["UniqueId"] = AppDataReader.GetOwnersUniqueID(id);
                    //Added by Sumit for GSP-1161   ---END

                    ConvertTableToJSON convertkneedown = new ConvertTableToJSON(dtKneeDown, Tables.FrontBodyPositionKneeDown, RowNumToJSON.FirstRow,id); //Edited by Sumit added 'id' argument
                    bool uploadkneedownResult = JSONManager.JsonTOBsonSender(convertkneedown.strJonToSend, CloudUrls.FrontBodyPositionKneeDownSendToUrl);





                    //upload standing table
                    DataTable dtStanding = AppDataReader.GetFrontBodyPositionStanding(id);

                    //Sumit v2.0
                    dtStanding = DBHandler.AddToImageBytesBeforeSend(dtStanding);
                    //v2.0

                    //Added by Sumit for GSP-1161   ---START
                    //Set ID as per record local or foreign status
                    dtStanding.Rows[0]["UniqueId"] = AppDataReader.GetOwnersUniqueID(id);
                    //Added by Sumit for GSP-1161   ---END

                    ConvertTableToJSON convertStanding = new ConvertTableToJSON(dtStanding, Tables.FrontBodyPositionStanding, RowNumToJSON.FirstRow, id); //Edited by Sumit added 'id' argument);
                    bool uploadStandingResult = JSONManager.JsonTOBsonSender(convertStanding.strJonToSend, CloudUrls.FrontBodyPositionStandingSendToUrl);



                    DataTable dtSideBody = AppDataReader.GetSideBodyPosition(id);

                    //sumit v2.0
                    dtSideBody = DBHandler.AddToImageBytesBeforeSend(dtSideBody);
                    //v2.0

                    //Added by Sumit for GSP-1161   ---START
                    //Set ID as per record local or foreign status
                    dtSideBody.Rows[0]["UniqueId"] = AppDataReader.GetOwnersUniqueID(id);
                    //Added by Sumit for GSP-1161   ---END
                    //upload side position table
                    ConvertTableToJSON convertSidebody = new ConvertTableToJSON(dtSideBody, Tables.SideBodyPosition, RowNumToJSON.FirstRow, id); //Edited by Sumit added 'id' argument);
                    bool uploadSidebodyResult = JSONManager.JsonTOBsonSender(convertSidebody.strJonToSend, CloudUrls.SideBodyPositionSendToUrl);

                    //Mark relevant record, update done
                    DBHandler.ResetLut(id);

                }
                //string patientUploadResult=
            }


        }
    }
}
