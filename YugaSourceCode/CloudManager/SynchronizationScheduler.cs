﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    
    public static class SyncSchedulekeyConfigValues
    {
        public static string Manual;
        public static string Automatic;
        public static string Custom;
        public static string TakeLatest_Before_Open;
        public static string Block_Edit_NonLocal_Reports;
        public static string EditAndSave_As_New;
        public static string Offilne_Block_Edit_NonLocal_Reports;
        public static string Offline_Allow_Edit_But_Save_As_New;
        public static string If_Application_Is_Idle_For_More_Than_Minute;
        public static string If_Application_Is_Idle_Minute_Value;
        public static string Between_From_To_Date;
        public static string Between_From_Time_Value;
        public static string Between_To_Time_Value;
        public static string Closing_Application;
        public static string Starting_Application;
        public static System.Timers.Timer timer;
        static object locker = new object();
        //Added  by Sumit for GSP-1161   ---START
        static object manualSyncLocker = new object();
        static object autoSyncLocker = new object();
        static object timerLocker = new object();
        //Added by Sumit for GSP-1161   ---END
        static System.Threading.Thread t;  //Moved here by Sumit GSP-1161

        private static string selectAllQuery = "select id, frm_reference, keyname, defaultstatus, bestspeed, user1, user2, activestatus  from KeyConfig;";

        //Edited by Sumit for GTL#276   ---START   removing the primary constructor to avoid the possibility of start timer earlier than we are ready
        //static SyncSchedulekeyConfigValues()
        private static void MakeItReady()
        {
            try
            {

                SQLiteConnection sqlite;
                //Edited by Sumit for GSP-1181   ---START
                //sqlite = new SQLiteConnection("Data Source=" + initCloudSystem.db_file);
                sqlite = Utility.SQLiteGetSecureCon("Data Source=" + initCloudSystem.db_file);
                //Edited by Sumit for GSP-1181   ---END
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();

                cmd.CommandText = selectAllQuery;
                DataTable dt = new DataTable();
                SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
                da.Fill(dt);
                if (dt.Rows != null && dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["keyname"].ToString().ToUpper().Trim() == "MANUAL")
                        {
                            Manual = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Automatic".ToUpper())
                        {
                            Automatic = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Custom".ToUpper())
                        {
                            Custom = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "TakeLatest_Before_Open".ToUpper())
                        {
                            TakeLatest_Before_Open = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Block_Edit_NonLocal_Reports".ToUpper())
                        {
                            Block_Edit_NonLocal_Reports = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "EditAndSave_As_New".ToUpper())
                        {
                            EditAndSave_As_New = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Offilne_Block_Edit_NonLocal_Reports".ToUpper())
                        {
                            Offilne_Block_Edit_NonLocal_Reports = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Offline_Allow_Edit_But_Save_As_New".ToUpper())
                        {
                            Offline_Allow_Edit_But_Save_As_New = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "If_Application_Is_Idle_For_More_Than_Minute".ToUpper())
                        {
                            If_Application_Is_Idle_For_More_Than_Minute = dr["ActiveStatus"].ToString().ToUpper().Split('_')[0];
                            If_Application_Is_Idle_Minute_Value = dr["ActiveStatus"].ToString().ToUpper().Split('_')[1];
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Between_From_To_Date".ToUpper())
                        {
                            Between_From_To_Date = dr["ActiveStatus"].ToString().ToUpper().Split('_')[0];
                            try
                            {
                                Between_From_Time_Value = dr["ActiveStatus"].ToString().ToUpper().Split('_')[1];
                                Between_To_Time_Value = dr["ActiveStatus"].ToString().ToUpper().Split('_')[2];
                            }
                            catch
                            {
                                Between_From_Time_Value = "0";
                                Between_To_Time_Value = "0";
                            }
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Closing_Application".ToUpper())
                        {
                            Closing_Application = dr["ActiveStatus"].ToString().ToUpper();
                        }
                        else if (dr["keyname"].ToString().ToUpper().Trim() == "Starting_Application".ToUpper())
                        {
                            Starting_Application = dr["ActiveStatus"].ToString().ToUpper();
                        }
                    }
                }
                //edited by Sumit for GSP-1161   ---START
                //if (Automatic.ToBool() || (Custom.ToBool() && (Between_From_To_Date.ToBool() || If_Application_Is_Idle_For_More_Than_Minute.ToBool())))
                //edited by Sumit for GSP-1161   ---START
                timer = new System.Timers.Timer(1000 * 10);// Current Timer(1000 * 10)=10 seconds
                timer.AutoReset = true;// false;
                timer.Elapsed += Timer_Elapsed;
                timer.Start();
            }
            catch(Exception ex)
            {

            }
            
        }

        //Added by Sumit for GSP-1161   ---START
        /// <summary>
        /// This method ensures immediate call of static constructor so that synchronization starts
        /// </summary>
        /// <returns></returns>
        public static int InintSync()
        {
            MakeItReady();
            //Added by Sumit for GTL#270   ---START
            //Edited IF condition by Sumit for GTL#433   ---START
            //if (CloudManager.SyncSchedulekeyConfigValues.Starting_Application.ToBool())
            if (CloudManager.SyncSchedulekeyConfigValues.Custom.ToBool() && CloudManager.SyncSchedulekeyConfigValues.Starting_Application.ToBool())
            //Edited IF condition by Sumit for GTL#433   ---END
            {
                timer.Stop();                
                CloudManager.CloudSyncManager.SynchronizeAll();
                //Added by Sumit GSP-1161   ---START
                SyncExistingRecords();
                //Added by Sumit GSP-1161   ---END
                timer.Start();
            }
            //Added by Sumit for GTL#270   ---START
            return 1;
        }
        //Added by Sumit for GSP-1161   ---START

        private static void Timer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            if (!WebComCation.Utility.IsInternetConnected())
            {
                return ;
            }
            //Test System.Windows.Forms.MessageBox.Show("Timer Ticked");
            timer.Stop();
            lock (timerLocker)
            {
                try
                {
                    //timer.Stop();
                    if (CloudManager.SyncSchedulekeyConfigValues.Automatic.ToBool())
                    {
                        DoAutoSync();
                        System.Threading.Thread.Sleep(10 * 1000);// give halt of 10 seconds to fire timer again 1000 ms =1 second
                        timer.Start();
                    }
                    else if (CloudManager.SyncSchedulekeyConfigValues.Custom.ToBool())
                    {
                        //Edited by Sumit GSP-1161   ---START
                        //System.Threading.Thread t = new System.Threading.Thread(DoSyncAfterIdle);
                        if (t != null && !t.IsAlive)
                        {
                            t = new System.Threading.Thread(DoCustomSync);
                            //System.Threading.Thread.Sleep(50000);// give haltof 50 seconds to fire timer again
                            t.Start();
                        }
                        else if (t == null)
                        {
                            t = new System.Threading.Thread(DoCustomSync);
                            //System.Threading.Thread.Sleep(50000);// give haltof 50 seconds to fire timer again
                            t.Start();
                        }
                        //Edited by Sumit GSP-1161   ---END
                    }
                    //else
                    //{
                    //    t.Start();
                    //}

                }
                finally
                {
                    timer.Start();
                }
            }
        }

        public static void DoCustomSync()
        {
            if (!WebComCation.Utility.IsInternetConnected())
            {
                return ;
            }
            lock (locker)
            {
                try
                {
                    TimeSpan tpLastUpdate = DateTime.Now - CloudManager.SynchronizationScheduler.lastUpdatedTime;
                    if (tpLastUpdate.TotalSeconds < 10)
                    {
                        return;
                    }

                    if (CloudManager.SyncSchedulekeyConfigValues.If_Application_Is_Idle_For_More_Than_Minute.ToBool())
                    {
                        TimeSpan tplastActive = DateTime.Now - CloudManager.SynchronizationScheduler.lastActivityTime;
                        if (Math.Round(tplastActive.TotalMinutes) > Convert.ToInt16(CloudManager.SyncSchedulekeyConfigValues.If_Application_Is_Idle_Minute_Value))
                        {
                            CloudManager.CloudSyncManager.SynchronizeAll();
                            //Added by Sumit GSP-1161   ---START
                            SyncExistingRecords();
                            CloudManager.SynchronizationScheduler.lastActivityTime = DateTime.Now;
                            //Added by Sumit GSP-1161   ---END
                        }
                    }
                    else if (CloudManager.SyncSchedulekeyConfigValues.Between_From_To_Date.ToBool())
                    {
                        try
                        {
                            int fromHour = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[0] +
                                                                CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[1]);
                            int fromMinute = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[2] +
                                                                CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value.ToCharArray()[3]);

                            int toHour = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[0] +
                                                                CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[1]);
                            int toMinute = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[2] +
                                                                CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value.ToCharArray()[3]);
                            int curHour = DateTime.Now.Hour;
                            int curMinute = DateTime.Now.Minute;

                            //Edited by Sumit for GTL#270   ---START
                            {
                                //Edited by Sumit for GSP-1161   ---START
                                //if( (fromHour <= curHour && curHour <= toHour) && (fromMinute <= curMinute && curMinute <= toMinute))
                                //{

                                //        CloudManager.CloudSyncManager.SynchronizeAll();
                                //        //Added by Sumit GSP-1161   ---START
                                //        SyncExistingRecords();
                                //        //Added by Sumit GSP-1161   ---END
                                //}

                                //Edited by Sumit for GSP-1161   ---END
                            }
                            try
                            {
                                int fromTimeSnap = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_From_Time_Value);
                                int curTimeSnap = Convert.ToInt32(DateTime.Now.Hour.ToString("00") + DateTime.Now.Minute.ToString("00"));
                                int toTimeSnap = Convert.ToInt32(CloudManager.SyncSchedulekeyConfigValues.Between_To_Time_Value);
                                if (fromTimeSnap < curTimeSnap && curTimeSnap < toTimeSnap)
                                {
                                    CloudManager.CloudSyncManager.SynchronizeAll();
                                    //Added by Sumit GSP-1161   ---START
                                    SyncExistingRecords();
                                    //Added by Sumit GSP-1161   ---END
                                }
                            }
                            catch
                            {
                                CloudManager.CloudSyncManager.SynchronizeAll();
                                //Added by Sumit GSP-1161   ---START
                                SyncExistingRecords();
                                //Added by Sumit GSP-1161   ---END
                            }
                            //Edited by Sumit for GTL#270   ---END
                        }
                        catch (Exception ex)
                        {
                            //edited by sumit for GSP-1161   ---START   ---START                    
                            //throw ex;
                            CloudManager.CloudSyncManager.SynchronizeAll();
                            //edited by sumit for GSP-1161   ---START   ---END
                        }
                    }
                    //CloudManager.SynchronizationScheduler.lastActivityTime = DateTime.Now;  
                }
                finally
                {
                    //if(timer.en
                    timer.Start();
                }
            }
        }

        /// <summary>
        /// Added by Sumit for GSP-1161
        /// </summary>
        public static void DoAutoSync()
        {
            lock (autoSyncLocker)   //locker Added by Sumit GSP-1161
            {
                TimeSpan tpLastUpdate = DateTime.Now - CloudManager.SynchronizationScheduler.lastUpdatedTime;
                if (tpLastUpdate.TotalMinutes < 1)
                {
                    return;
                }

                //Added by Sumit GSP-1161   ---START
                SyncExistingRecords();
                //Added by Sumit GSP-1161   ---END
            }
        }

        /// <summary>
        /// Function Added by Sumit GSP-1161
        /// </summary>
        /// <returns></returns>
        public static bool SyncExistingRecords()
        {

            if (!WebComCation.Utility.IsInternetConnected())
            {
                return false;
            }


            bool retval = false;
            //Added by Sumit GSP-1161   ---START
            CloudManager.CloudSyncManager.SynchronizeAll();
            try
            {
                List<PC> PCs = CloudSyncManager.GetAllPCsToBeSynced(true);
                foreach (PC p in PCs)
                {
                    CloudSyncManager.SyncSelectedOne(p);
                }
                retval = true;
            }
            catch(Exception ex)
            {
                if (ex.Message.ToUpper().Contains("CONNECTION"))
                {
                    //System.Windows.Forms.MessageBox.Show("Data may not be synced as there was some net connectivity issue"); //commented by rohini for GTL#294
#if DEBUG
                    System.Windows.Forms.MessageBox.Show("DEBUG: "+CloudManager.Properties.Resources.DATA_MAY_NOTBE_SYNCED_ASTHEREWAS_SOME_NET_ISSUE); //Added by rohini for GTL#294
#endif
                }
                else
                {
                    retval = false;
                    throw ex;
                }
            }
            finally
            {

            }
            //Added by Sumit GSP-1161   ---END
            return true;
        }
    }


    public static class SynchronizationScheduler
    {
        public static DateTime lastActivityTime { get; set; }
        public static DateTime lastUpdatedTime { get; set; }
        public static DateTime NextUpdateToBeAtTime { get; set; }        
    }
    
    
}
