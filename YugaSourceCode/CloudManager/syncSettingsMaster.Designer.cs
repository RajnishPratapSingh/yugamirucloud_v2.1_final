namespace CloudManager
{
    partial class syncSettingsMaster
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(syncSettingsMaster));
            this.gbDownloadData = new System.Windows.Forms.GroupBox();
            this.rdbCustom = new System.Windows.Forms.RadioButton();
            this.rdbAutomatic = new System.Windows.Forms.RadioButton();
            this.rdbManual = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.gbOverWriting = new System.Windows.Forms.GroupBox();
            this.chkEditSaveNew = new System.Windows.Forms.CheckBox();
            this.chkBlockEdit = new System.Windows.Forms.CheckBox();
            this.chkTakeLatestOnOpen = new System.Windows.Forms.CheckBox();
            this.gbNetConnection = new System.Windows.Forms.GroupBox();
            this.chkOfflineEditSaveAsNew = new System.Windows.Forms.CheckBox();
            this.chkOfflineBlockEdit = new System.Windows.Forms.CheckBox();
            this.btnRestoreFactorySettings = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.gbDownloadData.SuspendLayout();
            this.gbOverWriting.SuspendLayout();
            this.gbNetConnection.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbDownloadData
            // 
            this.gbDownloadData.Controls.Add(this.rdbCustom);
            this.gbDownloadData.Controls.Add(this.rdbAutomatic);
            this.gbDownloadData.Controls.Add(this.rdbManual);
            this.gbDownloadData.Controls.Add(this.label1);
            this.gbDownloadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbDownloadData.Location = new System.Drawing.Point(5, 23);
            this.gbDownloadData.Margin = new System.Windows.Forms.Padding(4);
            this.gbDownloadData.Name = "gbDownloadData";
            this.gbDownloadData.Padding = new System.Windows.Forms.Padding(4);
            this.gbDownloadData.Size = new System.Drawing.Size(326, 164);
            this.gbDownloadData.TabIndex = 0;
            this.gbDownloadData.TabStop = false;
            this.gbDownloadData.Text = "Download Patient Records From Cloud";
            // 
            // rdbCustom
            // 
            this.rdbCustom.AutoSize = true;
            this.rdbCustom.Location = new System.Drawing.Point(171, 50);
            this.rdbCustom.Name = "rdbCustom";
            this.rdbCustom.Size = new System.Drawing.Size(76, 21);
            this.rdbCustom.TabIndex = 9;
            this.rdbCustom.Text = "Custom";
            this.rdbCustom.UseVisualStyleBackColor = true;
            this.rdbCustom.Visible = false;
            this.rdbCustom.CheckedChanged += new System.EventHandler(this.rdbCustom_CheckedChanged);
            // 
            // rdbAutomatic
            // 
            this.rdbAutomatic.AutoSize = true;
            this.rdbAutomatic.Location = new System.Drawing.Point(12, 100);
            this.rdbAutomatic.Name = "rdbAutomatic";
            this.rdbAutomatic.Size = new System.Drawing.Size(91, 21);
            this.rdbAutomatic.TabIndex = 8;
            this.rdbAutomatic.Text = "Automatic";
            this.rdbAutomatic.UseVisualStyleBackColor = true;
            // 
            // rdbManual
            // 
            this.rdbManual.AutoSize = true;
            this.rdbManual.Checked = true;
            this.rdbManual.Location = new System.Drawing.Point(12, 50);
            this.rdbManual.Name = "rdbManual";
            this.rdbManual.Size = new System.Drawing.Size(75, 21);
            this.rdbManual.TabIndex = 7;
            this.rdbManual.TabStop = true;
            this.rdbManual.Text = "Manual";
            this.rdbManual.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Location = new System.Drawing.Point(168, 102);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "(Highly Recommended)";
            // 
            // gbOverWriting
            // 
            this.gbOverWriting.Controls.Add(this.chkEditSaveNew);
            this.gbOverWriting.Controls.Add(this.chkBlockEdit);
            this.gbOverWriting.Controls.Add(this.chkTakeLatestOnOpen);
            this.gbOverWriting.Location = new System.Drawing.Point(337, 28);
            this.gbOverWriting.Margin = new System.Windows.Forms.Padding(4);
            this.gbOverWriting.Name = "gbOverWriting";
            this.gbOverWriting.Padding = new System.Windows.Forms.Padding(4);
            this.gbOverWriting.Size = new System.Drawing.Size(279, 196);
            this.gbOverWriting.TabIndex = 1;
            this.gbOverWriting.TabStop = false;
            this.gbOverWriting.Text = "Report Over Writing";
            // 
            // chkEditSaveNew
            // 
            this.chkEditSaveNew.AutoSize = true;
            this.chkEditSaveNew.Location = new System.Drawing.Point(23, 150);
            this.chkEditSaveNew.Margin = new System.Windows.Forms.Padding(4);
            this.chkEditSaveNew.Name = "chkEditSaveNew";
            this.chkEditSaveNew.Size = new System.Drawing.Size(164, 21);
            this.chkEditSaveNew.TabIndex = 4;
            this.chkEditSaveNew.Text = "Edit and save as new";
            this.chkEditSaveNew.UseVisualStyleBackColor = true;
            // 
            // chkBlockEdit
            // 
            this.chkBlockEdit.AutoSize = true;
            this.chkBlockEdit.Checked = true;
            this.chkBlockEdit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkBlockEdit.Location = new System.Drawing.Point(23, 102);
            this.chkBlockEdit.Margin = new System.Windows.Forms.Padding(4);
            this.chkBlockEdit.Name = "chkBlockEdit";
            this.chkBlockEdit.Size = new System.Drawing.Size(223, 21);
            this.chkBlockEdit.TabIndex = 3;
            this.chkBlockEdit.Text = "Block edit for non-local reports";
            this.chkBlockEdit.UseVisualStyleBackColor = true;
            // 
            // chkTakeLatestOnOpen
            // 
            this.chkTakeLatestOnOpen.AutoSize = true;
            this.chkTakeLatestOnOpen.Location = new System.Drawing.Point(23, 50);
            this.chkTakeLatestOnOpen.Margin = new System.Windows.Forms.Padding(4);
            this.chkTakeLatestOnOpen.Name = "chkTakeLatestOnOpen";
            this.chkTakeLatestOnOpen.Size = new System.Drawing.Size(181, 21);
            this.chkTakeLatestOnOpen.TabIndex = 2;
            this.chkTakeLatestOnOpen.Text = "Take latest before open";
            this.chkTakeLatestOnOpen.UseVisualStyleBackColor = true;
            // 
            // gbNetConnection
            // 
            this.gbNetConnection.Controls.Add(this.chkOfflineEditSaveAsNew);
            this.gbNetConnection.Controls.Add(this.chkOfflineBlockEdit);
            this.gbNetConnection.Controls.Add(this.btnRestoreFactorySettings);
            this.gbNetConnection.Location = new System.Drawing.Point(624, 28);
            this.gbNetConnection.Margin = new System.Windows.Forms.Padding(4);
            this.gbNetConnection.Name = "gbNetConnection";
            this.gbNetConnection.Padding = new System.Windows.Forms.Padding(4);
            this.gbNetConnection.Size = new System.Drawing.Size(279, 196);
            this.gbNetConnection.TabIndex = 5;
            this.gbNetConnection.TabStop = false;
            this.gbNetConnection.Text = "Working Offline";
            // 
            // chkOfflineEditSaveAsNew
            // 
            this.chkOfflineEditSaveAsNew.AutoSize = true;
            this.chkOfflineEditSaveAsNew.Checked = true;
            this.chkOfflineEditSaveAsNew.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOfflineEditSaveAsNew.Location = new System.Drawing.Point(27, 102);
            this.chkOfflineEditSaveAsNew.Margin = new System.Windows.Forms.Padding(4);
            this.chkOfflineEditSaveAsNew.Name = "chkOfflineEditSaveAsNew";
            this.chkOfflineEditSaveAsNew.Size = new System.Drawing.Size(195, 21);
            this.chkOfflineEditSaveAsNew.TabIndex = 4;
            this.chkOfflineEditSaveAsNew.Text = "Allow edit but save as new";
            this.chkOfflineEditSaveAsNew.UseVisualStyleBackColor = true;
            // 
            // chkOfflineBlockEdit
            // 
            this.chkOfflineBlockEdit.AutoSize = true;
            this.chkOfflineBlockEdit.Checked = true;
            this.chkOfflineBlockEdit.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkOfflineBlockEdit.Location = new System.Drawing.Point(27, 50);
            this.chkOfflineBlockEdit.Margin = new System.Windows.Forms.Padding(4);
            this.chkOfflineBlockEdit.Name = "chkOfflineBlockEdit";
            this.chkOfflineBlockEdit.Size = new System.Drawing.Size(223, 21);
            this.chkOfflineBlockEdit.TabIndex = 3;
            this.chkOfflineBlockEdit.Text = "Block edit for non-local reports";
            this.chkOfflineBlockEdit.UseVisualStyleBackColor = true;
            // 
            // btnRestoreFactorySettings
            // 
            this.btnRestoreFactorySettings.Location = new System.Drawing.Point(27, 150);
            this.btnRestoreFactorySettings.Margin = new System.Windows.Forms.Padding(4);
            this.btnRestoreFactorySettings.Name = "btnRestoreFactorySettings";
            this.btnRestoreFactorySettings.Size = new System.Drawing.Size(207, 28);
            this.btnRestoreFactorySettings.TabIndex = 6;
            this.btnRestoreFactorySettings.Text = "Restore Factory Settings";
            this.btnRestoreFactorySettings.UseVisualStyleBackColor = true;
            this.btnRestoreFactorySettings.Click += new System.EventHandler(this.btnRestoreFactorySettings_Click);
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(220, 202);
            this.btnSave.Margin = new System.Windows.Forms.Padding(4);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(100, 28);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(81, 202);
            this.btnCancel.Margin = new System.Windows.Forms.Padding(4);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(100, 28);
            this.btnCancel.TabIndex = 8;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // syncSettingsMaster
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(336, 243);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.gbNetConnection);
            this.Controls.Add(this.gbOverWriting);
            this.Controls.Add(this.gbDownloadData);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(354, 290);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(354, 290);
            this.Name = "syncSettingsMaster";
            this.Text = "Yugamiru";
            this.Load += new System.EventHandler(this.syncSettingsMaster_Load);
            this.gbDownloadData.ResumeLayout(false);
            this.gbDownloadData.PerformLayout();
            this.gbOverWriting.ResumeLayout(false);
            this.gbOverWriting.PerformLayout();
            this.gbNetConnection.ResumeLayout(false);
            this.gbNetConnection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbDownloadData;
        private System.Windows.Forms.GroupBox gbOverWriting;
        private System.Windows.Forms.CheckBox chkEditSaveNew;
        private System.Windows.Forms.CheckBox chkBlockEdit;
        private System.Windows.Forms.CheckBox chkTakeLatestOnOpen;
        private System.Windows.Forms.GroupBox gbNetConnection;
        private System.Windows.Forms.CheckBox chkOfflineEditSaveAsNew;
        private System.Windows.Forms.CheckBox chkOfflineBlockEdit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnRestoreFactorySettings;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.RadioButton rdbManual;
        private System.Windows.Forms.RadioButton rdbCustom;
        private System.Windows.Forms.RadioButton rdbAutomatic;
    }
}