﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public class AppDataReader
    {
        private static string conString = "Data Source=" + initCloudSystem.db_file;


        public static DataTable GetPatientDetails(string UniqueId)
        {
            DataTable dt = new DataTable();
            SQLiteCommand cmd;

            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select * from PatientDetails where uniqueid=" + UniqueId + ";";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            //Added by Sumit on 06-Dec-2018 For GSP-944-------------START
            foreach(DataRow dr in dt.Rows)
            {
                //PatientId column
                dr["PatientId"] = SplCharHandler.GetCodedString(dr["PatientId"].ToString());
                //Name column 
                dr["Name"] = SplCharHandler.GetCodedString(dr["Name"].ToString());
                //Comment Column
                dr["Comment"] = SplCharHandler.GetCodedString(dr["Comment"].ToString());

            }
            //Added by Sumit on 06-Dec-2018 For GSP-944-------------END

            //=========Added by Sumit for GSP-983 START=========
            for (int i = 0; i < dt.Columns.Count; i++)
            {
                if(dt.Columns[i].ColumnName.ToUpper()=="Server_User_id".ToUpper())
                {
                    dt.Columns[i].ColumnName = "user_id";
                    break;
                }
            }
            //=========Added by Sumit for GSP-983 END=========


            return dt;
        }
        public static DataTable GetFrontBodyPositionKneeDown(string UniqueId)
        {
            DataTable dt = new DataTable();
            SQLiteCommand cmd;

            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select * from FrontBodyPositionKneeDown where uniqueid=" + UniqueId + ";";
            //cmd.CommandText = "select imagebytes from FrontBodyPositionKneeDown where uniqueid=" + UniqueId + ";";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();
            return dt;
        }
        public static DataTable GetFrontBodyPositionStanding(string UniqueId)
        {
            DataTable dt = new DataTable();
            SQLiteCommand cmd;

            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select * from FrontBodyPositionStanding where uniqueid=" + UniqueId + ";";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();
            return dt;
        }
        public static DataTable GetSideBodyPosition(string UniqueId)
        {
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select * from SideBodyPosition where uniqueid=" + UniqueId + ";";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();
            return dt;
        }


        public DataTable ReadTableData(Tables tableName)
        {
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select * from " + tableName.ToString() + ";";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();
            return dt;
        }


        public static List<string> GetUniqueIDsNeedToUpdate()
        {
            //Edited by Sumit for GSP-1161   START
            #region Fetch_All_Not_Uploaded_LocalIDs
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select uniqueid from PatientDetails where lut != \"\" ";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            List<string> uniqueIDs = new List<string>();

            foreach (DataRow dr in dt.Rows)
            {
                uniqueIDs.Add(dr["uniqueid"].ToString());
            }
            #endregion
            {
                /*
                List<string> uniqueIDs = new List<string>();
                {
                    DataTable dt = new DataTable();
                    SQLiteCommand cmd;
                    SQLiteConnection AppDBCon = new SQLiteConnection(conString);
                    cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
                    cmd.CommandText = "select uniqueid from PatientDetails where lut != \"\" AND owners_unique_Id='-1' "; //For Self/local PC Records
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    dt = new DataTable();
                    ad.Fill(dt); //fill the datasource
                                 //close connection
                    AppDBCon.Close();               

                    foreach (DataRow dr in dt.Rows)
                    {
                        uniqueIDs.Add(dr["uniqueid"].ToString());
                    }
                }
                {
                    DataTable dt = new DataTable();
                    SQLiteCommand cmd;
                    SQLiteConnection AppDBCon = new SQLiteConnection(conString);
                    cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
                    cmd.CommandText = "select owners_unique_Id from PatientDetails where lut != \"\" AND owners_unique_Id !='-1' "; //For Foreign PC Records
                    SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
                    dt = new DataTable();
                    ad.Fill(dt); //fill the datasource
                                 //close connection
                    AppDBCon.Close();

                    foreach (DataRow dr in dt.Rows)
                    {
                        uniqueIDs.Add(dr["owners_unique_Id"].ToString());
                    }
                }
                */
            }
            //Edited by Sumit for GSP-1161   END
            return uniqueIDs;
        }
        public static List<string> GetOwnersUniqueIDsNeedToUpdate()
        {
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select owners_unique_id from PatientDetails where lut != \"\" ";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            List<string> Owners_uniqueIDs = new List<string>();

            foreach (DataRow dr in dt.Rows)
            {
                Owners_uniqueIDs.Add(dr["owners_unique_id"].ToString());
            }

            return Owners_uniqueIDs;
        }
        public static int GetRecordCountThisPC()
        {
            int retval = -1;
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select count(uniqueid) from PatientDetails where owners_unique_id='-1';";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();
            if(dt!=null && dt.Rows.Count>0)
            {
                retval = System.Convert.ToInt16(dt.Rows[0][0].ToString());
            }
            else
            {
                retval = 0;
            }
            return retval;
        }
        public static int GetRecordCount(string condition="")
        {
            int retval = -1;
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select count(uniqueid) from PatientDetails;";
            if(condition.Length>0)
            {
                cmd.CommandText = "select count(uniqueid) from PatientDetails"+condition;
            }
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            try
            {
                ad.Fill(dt); //fill the datasource
                             //close connection
            }
            catch(Exception exSQL)
            {
                throw exSQL;
            }
            AppDBCon.Close();
            if (dt != null && dt.Rows.Count > 0)
            {
                retval = System.Convert.ToInt16(dt.Rows[0][0].ToString());
            }
            else
            {
                retval = 0;
            }
            return retval;
        }


        /// <summary>
        /// Returns the Owners_Unique_Id which will be UniqueID at server for that record
        /// Implemented For GSP-1161
        /// </summary>
        /// <param name="localUniqueId">The unique ID for the record in local database</param>
        /// <returns>Owners_Unique_Id of the data base if the record is Foreign Record</returns>
        public static string GetOwnersUniqueID(string localUniqueId)
        {
            string Owners_Unique_Id = string.Empty;


            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select owners_unique_Id from PatientDetails where uniqueid=" + localUniqueId + " AND owners_unique_Id != '-1';"; //For Self/local PC Records
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Owners_Unique_Id = dr["Owners_Unique_Id"].ToString();
            }
            if(Owners_Unique_Id.Length<1)
            {
                Owners_Unique_Id = localUniqueId;
            }


            return Owners_Unique_Id;
        }

        //---Stall_id
        /// <summary>
        /// Returns the Stall_Id which will be UniqueID at server for that record
        /// Implemented For GSP-1161
        /// </summary>
        /// <param name="localUniqueId">The unique ID for the record in local database</param>
        /// <returns>Stall_Id of the data base if the record is Foreign Record</returns>
        public static string GetStallID(string localUniqueId)
        {
            string Stall_Id = string.Empty;


            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select Stall_Id from PatientDetails where uniqueid=" + localUniqueId + ";"; //For Self/local PC Records
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Stall_Id = dr["Stall_Id"].ToString();
            }
            if (Stall_Id.Length < 1)
            {
                Stall_Id = localUniqueId;
            }


            return Stall_Id;
        }
        //--

        //---reg_user_id----
        /// <summary>returns reg_user_id which will be UniqueID at server for that record
        /// Implemented For GSP-1161
        /// </summary>
        /// <param name="localUniqueId">The unique ID for the record in local database</param>
        /// <returns>reg_user_id of the data base if the record is Foreign Record</returns>
        public static string GetRegUserID(string localUniqueId)
        {
            string reg_user_id = string.Empty;
            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select reg_user_id from PatientDetails where uniqueid=" + localUniqueId + ";"; //For Self/local PC Records
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            foreach (DataRow dr in dt.Rows)
            {
                reg_user_id = dr["reg_user_id"].ToString();
            }
            if (reg_user_id.Length < 1)
            {
                reg_user_id = localUniqueId;
            }
            return reg_user_id;
        }
        //--

        //---Server_User_Id---
        /// <summary>
        /// Returns the Server_User_Id which will be UniqueID at server for that record
        /// Implemented For GSP-1161
        /// </summary>
        /// <param name="localUniqueId">The unique ID for the record in local database</param>
        /// <returns>Server_User_Id of the data base if the record is Foreign Record</returns>
        public static string GetServerUserID(string localUniqueId)
        {
            string Server_User_Id = string.Empty;

            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            //Edited by Sumit for GSP-1181   ---START
            //SQLiteConnection AppDBCon = new SQLiteConnection(conString);
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(conString);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();// ("select uniqueid,lut from PatientDetails");
            cmd.CommandText = "select Server_User_Id from PatientDetails where uniqueid=" + localUniqueId +";"; //For Self/local PC Records
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt); //fill the datasource
                         //close connection
            AppDBCon.Close();

            foreach (DataRow dr in dt.Rows)
            {
                Server_User_Id = dr["Server_User_Id"].ToString();
            }
            if (Server_User_Id.Length < 1)
            {
                throw new Exception("Cannot fetch Server_User_Id from local DB for uniqueid = " + localUniqueId);
            }


            return Server_User_Id;
        }
        //--


    }
}
