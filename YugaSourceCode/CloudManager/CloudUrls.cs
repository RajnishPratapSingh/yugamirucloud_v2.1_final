﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CloudManager
{
    public static class CloudUrls
    {
        //--Commented by Rajnish on 13-06-18-----//
        //public static string PatientDetailsSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadpatient_data";
        //public static string FrontBodyPositionKneeDownSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadkneedown_data";
        //public static string FrontBodyPositionStandingSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadstanding_data";
        //public static string SideBodyPositionSendToUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/uploadsidebody_data";

        //public static string PatientDetailsGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadpatient_data";
        //public static string FrontBodyPositionKneeDownGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadkneedown_data";
        //public static string FrontBodyPositionStandingGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadstanding_data";
        //public static string SideBodyPositionGetFromUrl = @"http://gs-demo.jma.website/apioauthdata/index.php/cloudData/downloadsidebody_data";

        //--Added by Rajnish on 13-06-18 as per new urls-----//
        //public static string PatientDetailsSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/uploadpatient_data";
        //public static string FrontBodyPositionKneeDownSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/uploadkneedown_data";
        //public static string FrontBodyPositionStandingSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/uploadstanding_data";
        //public static string SideBodyPositionSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/uploadsidebody_data";

        //public static string PatientDetailsGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/downloadpatient_data";
        //public static string FrontBodyPositionKneeDownGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/downloadkneedown_data";
        //public static string FrontBodyPositionStandingGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/downloadstanding_data";
        //public static string SideBodyPositionGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudsinglicenseData/downloadsidebody_data";

        
        //Added by sumit for Multilicense Cloud Sync
        public static string PatientDetailsSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/uploadpatient_data";
        public static string FrontBodyPositionKneeDownSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/uploadkneedown_data";
        public static string FrontBodyPositionStandingSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/uploadstanding_data";
        public static string SideBodyPositionSendToUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/uploadsidebody_data";

        public static string PatientDetailsGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadpatient_data";
        public static string FrontBodyPositionKneeDownGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadkneedown_data";
        public static string FrontBodyPositionStandingGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadstanding_data";
        public static string SideBodyPositionGetFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadsidebody_data";

        //Added by Rajnish  for GTL#359   ---START
        public static string PatientDetailsRecdinact = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadpatientrecdinact";
        public static string FrontBodyPositionKneeDownRecdinact = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadkneedownrecdinact";
        public static string FrontBodyPositionStandingRecdinact = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadstandingrecdinact";
        public static string SideBodyPositionRecdinact = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadsidebodyrecdinact";

        public static string PatientDetailsRecdinactbyserverid = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadpatientrecdinactbyserverid";
        public static string FrontBodyPositionKneeDownRecdinactbyserverid = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadkneedownrecdinactbyserverid";
        public static string FrontBodyPositionStandingRecdinactbyserverid = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadstandingrecdinactbyserverid";
        public static string SideBodyPositionRecdinactbyserverid = @"http://52.197.210.82/apioauthdata/index.php/cloudData/downloadsidebodyrecdinactbyserverid";
        //Added by Rajnish  for GTL#359   ---END

        public static string GetUpdateCounterFromUrl = @"http://52.197.210.82/apioauthdata/index.php/cloudData/getupdate_counter";

        //Added by Sumit for GTL#276   ---START
        public static string GetMaxUniqueIDUrl = @"http://52.197.210.82/apioauthdata/index.php/home/getrecordcount";
        //Added by Sumit for GTL#276   ---END
    }
}
