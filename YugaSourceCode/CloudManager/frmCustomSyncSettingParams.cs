using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace CloudManager
{
    public partial class frmCustomSyncSettingParams : Form
    {
        System.Text.RegularExpressions.Regex r;

        public frmCustomSyncSettingParams()
        {
            InitializeComponent();

            //added by rohini for GTL#294 -- START
            this.chkIdleFor.Text = CloudManager.Properties.Resources.IF_APPLICATION_IS_IDLE_FOR_MORE_THAN_MINUTES;
            this.label1.Text = CloudManager.Properties.Resources.MINUTES;
            this.chkBetweenTime.Text = CloudManager.Properties.Resources.BETWEEN;
            this.chkWhenClosingApp.Text = CloudManager.Properties.Resources.WHEN_CLOSING_THE_APP;
            this.chkWhenLaunchApp.Text = CloudManager.Properties.Resources.WHEN_STARTING_THE_APP;
            this.label2.Text = CloudManager.Properties.Resources.TO;
            btnSave.Text = CloudManager.Properties.Resources.SAVE;
            //added by rohini for GTL#294 -- END

            r = new System.Text.RegularExpressions.Regex(@"[~`!@#$%^&*()+=|\{}':;.,<>/?[\]""_]");

            //added by rohini for GTL#319 -- START
            if (chkIdleFor.Text.Length <= 25)
            {
                chkIdleFor.Width = 250;
                txtIdleMinutes.Location = new Point(265, 10);
                label1.Location = new Point(290, 13);
            }
            //added by rohini for GTL#319 -- END

        }

        public static string sCustomSetting { get; set; }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if(chkBetweenTime.Checked)
                {
                    string fromTimeSnap = cbFromHours.SelectedItem.ToString() + cbFromMinutes.SelectedItem.ToString();
                    string toTimeSnap = cbToHours.SelectedItem.ToString() + cbToMinutes.SelectedItem.ToString();

                    if(Convert.ToInt32(fromTimeSnap)>Convert.ToInt32(toTimeSnap))
                    {
                        //MessageBox.Show("Sync beetween 'from' time must be lesser than 'to' time");   //commented by rohini for GTL#294 
                        MessageBox.Show(CloudManager.Properties.Resources.SYNC_BTWN_FRMTIME_MUST_BE_LESS_THAN_TOTIME); //Added by rohini for GTL#294
                        return;
                    }
                }

                Dictionary<string, string> KeyConfig = new Dictionary<string, string>();

                KeyConfig.Add("If_Application_Is_Idle_For_More_Than_Minute", chkIdleFor.Checked ? "TRUE_" + txtIdleMinutes.Text : "FALSE_00");
                KeyConfig.Add("Between_From_To_Date", chkBetweenTime.Checked ? "TRUE_" + cbFromHours.SelectedItem.ToString()+cbFromMinutes.SelectedItem.ToString() + "_" + cbToHours.SelectedItem.ToString() + cbToMinutes.SelectedItem.ToString() : "FALSE_0000_0000");
                KeyConfig.Add("Closing_Application", chkWhenClosingApp.Checked ? "TRUE" : "FALSE");
                KeyConfig.Add("Starting_Application", chkWhenLaunchApp.Checked ? "TRUE" : "FALSE");

                string qry = "";
                foreach (KeyValuePair<string, string> column in KeyConfig)
                {
                    qry += "update KeyConfig set User1='" + column.Value + "' where KeyName='" + column.Key.ToString() + "';";
                }
                sCustomSetting = qry;
                this.Close();
            }
            catch (Exception ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
        }

        private void chkIdleFor_CheckedChanged(object sender, EventArgs e)
        {
            if(chkIdleFor.Checked)
            {
                txtIdleMinutes.Enabled = true;
            }
            else
            {
                txtIdleMinutes.Enabled = false;
            }
        }

        private void frmCustomSyncSettingParams_Load(object sender, EventArgs e)
        {
            try
            {
                Dictionary<string, string> DictActiveStatus = CloudUtility.SetCloudSetting();
                string[] sIdleFor = DictActiveStatus["If_Application_Is_Idle_For_More_Than_Minute"].ToString().Split('_');
                string[] sBetweenTime = DictActiveStatus["Between_From_To_Date"].ToString().Split('_');

                chkIdleFor.Checked = Convert.ToBoolean(sIdleFor[0]);
                txtIdleMinutes.Text = sIdleFor[1];
                chkBetweenTime.Checked = Convert.ToBoolean(sBetweenTime[0]);
                cbFromHours.SelectedItem = sBetweenTime[1].ToCharArray()[0].ToString() + sBetweenTime[1].ToCharArray()[1].ToString();
                cbFromMinutes.SelectedItem = sBetweenTime[1].ToCharArray()[2].ToString() + sBetweenTime[1].ToCharArray()[3].ToString();
                cbToHours.SelectedItem = sBetweenTime[2].ToCharArray()[0].ToString() + sBetweenTime[2].ToCharArray()[1].ToString();
                cbToMinutes.SelectedItem = sBetweenTime[2].ToCharArray()[2].ToString() + sBetweenTime[2].ToCharArray()[3].ToString();
                
                chkWhenClosingApp.Checked = Convert.ToBoolean(DictActiveStatus["Closing_Application"].ToString());
                chkWhenLaunchApp.Checked = Convert.ToBoolean(DictActiveStatus["Starting_Application"].ToString());
                if (cbFromHours.SelectedIndex < 0)
                {
                    cbFromHours.SelectedIndex = 0;
                    cbFromMinutes.SelectedIndex = 0;
                    cbToHours.SelectedIndex = 1;
                    cbToMinutes.SelectedIndex = 0;
                }
                if(txtIdleMinutes.Text=="00")
                {
                    txtIdleMinutes.Text = "01";
                }
            }
            catch { }
        }

        private void chkBetweenTime_CheckedChanged(object sender, EventArgs e)
        {
            if(chkBetweenTime.Checked)
            {
                cbFromHours.Enabled = true;
                cbFromMinutes.Enabled = true;
                cbToHours.Enabled = true;
                cbToMinutes.Enabled = true;
                if(cbFromHours.SelectedIndex<0)
                {
                    cbFromHours.SelectedIndex = 0;
                    cbFromMinutes.SelectedIndex = 0;
                    cbToHours.SelectedIndex = 1;
                    cbToMinutes.SelectedIndex = 0;
                }

            }
            else
            {
                cbFromHours.Enabled = false;
                cbFromMinutes.Enabled = false;
                cbToHours.Enabled = false;
                cbToMinutes.Enabled = false;
            }
        }

        private void txtIdleMinutes_TextChanged(object sender, EventArgs e)
        {
            if (r.IsMatch(((TextBox)(sender)).Text))
            {
                //  MessageBox.Show("Special characters are not allowed");  //commented by rohini for GTL#294
                MessageBox.Show(CloudManager.Properties.Resources.SPECIAL_CHARECTORS_NOT_ALLOWED);  //added by rohini for GTL#294
                ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).Text.Length - 1].ToString(), "");
            }
        }
    }
}
