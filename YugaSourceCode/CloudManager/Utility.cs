﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SQLite;

namespace CloudManager
{
    public static class Utility
    {

        /// <summary>
        /// Added by Sumit for GSP-1181
        /// Function returns the connection string as per solution build mode (DEBUG = nonProtected or  RELEASE = protected)
        /// </summary>
        /// <param name="dbpath"></param>
        /// <returns></returns>
        public static System.Data.SQLite.SQLiteConnection SQLiteGetSecureCon(string dbpath)
        {

            System.Data.SQLite.SQLiteConnection con;

#if DEBUG
            if (dbpath.ToUpper().Contains("Data Source".ToUpper()))
            {
                con = new System.Data.SQLite.SQLiteConnection(dbpath);
            }
            else
            {
                con = new System.Data.SQLite.SQLiteConnection("Data Source=" + dbpath);
            }

#else
            if(dbpath.ToUpper().Contains("Data Source".ToUpper()))
            {
            con = new System.Data.SQLite.SQLiteConnection(dbpath.Replace(";", "") + ";Version=3;password=" + ConvertToEncrypt("6G5s4Tdf45sT") + ";");
            }
            else
            {
            con = new System.Data.SQLite.SQLiteConnection("Data Source=" + dbpath.Replace(";", "") + ";Version=3;password=" + ConvertToEncrypt("6G5s4Tdf45sT") + ";");
            }
            
#endif
            return con;

        }
        public static string ConvertToEncrypt(string valueToEncrypt)
        {
            byte[] bt = UTF8Encoding.UTF8.GetBytes(valueToEncrypt);
            string s = Convert.ToBase64String(bt);
            return s;
        }

        /// <summary>
        /// Created By Sumit GTL#353.
        /// Checks all the four tables of SQLite DB for the data inconsistency and corrects the same if found any.
        /// </summary>
        /// <returns>bool: true DB was inconsistent otherwise if DB was correct returns false</returns>
        public static bool StabilizeSQLiteDatabase()
        {

            //-------------Find if DB is Inconsistent

            bool isDBInconsistent = false;

            string uniqueid = string.Empty;

            DataTable dt = new DataTable();
            SQLiteCommand cmd;
            SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(initCloudSystem.db_file);
            //Edited by Sumit for GSP-1181   ---END
            cmd = AppDBCon.CreateCommand();
            cmd.CommandText = "select Count(uniqueid) from PatientDetails UNION All " +
                "select Count(uniqueid) from FrontBodyPositionKneedown union ALL" +
                " select Count(uniqueid) from FrontBodyPositionStanding union ALL " +
                "select Count(uniqueid) from SideBodyPosition;";
            SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            dt = new DataTable();
            ad.Fill(dt);
            AppDBCon.Close();

            if (dt != null && dt.Rows.Count == 4)
            {
                if (dt.Rows[1][0].ToString().Trim() == dt.Rows[0][0].ToString().Trim() &&
                    dt.Rows[2][0].ToString().Trim() == dt.Rows[0][0].ToString().Trim() &&
                    dt.Rows[3][0].ToString().Trim() == dt.Rows[0][0].ToString().Trim())
                {
                    isDBInconsistent = false;
                }
                else
                {
                    isDBInconsistent = true;
                }
            }


            //--------------------------------------






            bool retVal = isDBInconsistent;
            //SQLiteCommand cmdRemove;
            SQLiteConnection AppDBConRemove = Utility.SQLiteGetSecureCon(initCloudSystem.db_file);
            using (SQLiteCommand cmdRemove = AppDBConRemove.CreateCommand())
            {
                //cmdRemove = AppDBConRemove.CreateCommand();
                cmdRemove.CommandText = @"delete from FrontBodyPositionKneedown Where UniqueId NOT IN (Select UniqueID from SideBodyPosition);
delete from FrontBodyPositionKneedown Where UniqueId NOT IN (Select UniqueID from FrontBodyPositionStanding);
delete from FrontBodyPositionKneedown Where UniqueId NOT IN (Select UniqueID from PatientDetails);

delete from FrontBodyPositionStanding Where UniqueId NOT IN (Select UniqueID from SideBodyPosition);
delete from FrontBodyPositionStanding Where UniqueId NOT IN (Select UniqueID from FrontBodyPositionKneedown);
delete from FrontBodyPositionStanding Where UniqueId NOT IN (Select UniqueID from PatientDetails);

delete from SideBodyPosition Where UniqueId NOT IN (Select UniqueID from FrontBodyPositionKneedown);
delete from SideBodyPosition Where UniqueId NOT IN (Select UniqueID from FrontBodyPositionStanding);
delete from SideBodyPosition Where UniqueId NOT IN (Select UniqueID from PatientDetails);

delete from PatientDetails  Where UniqueId NOT IN (Select UniqueID from FrontBodyPositionKneedown);
delete from PatientDetails  Where UniqueId NOT IN (Select UniqueID from FrontBodyPositionStanding);
delete from PatientDetails  Where UniqueId NOT IN (Select UniqueID from SideBodyPosition);";
                try
                {
                    AppDBConRemove.Open();
                    cmdRemove.ExecuteNonQuery();
                    AppDBConRemove.Close();

                }
                catch (Exception ex)
                {


#if debug
                System.Windows.Forms.MessageBox.Show(ex.StackTrace);
#endif
                }
            }

            return retVal;
            #region Commented By Sumit (Old Stabilization Logic)
            //bool isDBInconsistent = false;

            //string uniqueid = string.Empty;

            //DataTable dt = new DataTable();
            //SQLiteCommand cmd;            
            //SQLiteConnection AppDBCon = Utility.SQLiteGetSecureCon(initCloudSystem.db_file);
            ////Edited by Sumit for GSP-1181   ---END
            //cmd = AppDBCon.CreateCommand();
            //cmd.CommandText = "select Max(uniqueid) from PatientDetails UNION All " +
            //    "select Max(uniqueid) from FrontBodyPositionKneedown union ALL" +
            //    " select Max(uniqueid) from FrontBodyPositionStanding union ALL " +
            //    "select Max(uniqueid) from SideBodyPosition;";
            //SQLiteDataAdapter ad = new SQLiteDataAdapter(cmd);
            //dt = new DataTable();
            //ad.Fill(dt); 
            //AppDBCon.Close();

            //if(dt!=null && dt.Rows.Count==4)
            //{
            //    if (dt.Rows[1][0].ToString().Trim() == dt.Rows[0][0].ToString().Trim() &&
            //        dt.Rows[2][0].ToString().Trim() == dt.Rows[0][0].ToString().Trim() &&
            //        dt.Rows[3][0].ToString().Trim() == dt.Rows[0][0].ToString().Trim())
            //    {
            //        isDBInconsistent = false;
            //    }
            //    else
            //    {
            //        string uniqueidToRemove = dt.Rows[0][0].ToString().Trim();
            //        if(uniqueidToRemove.Length==0)
            //        {
            //            uniqueidToRemove = "0";
            //        }
            //        SQLiteCommand cmdRemove;
            //        SQLiteConnection AppDBConRemove = Utility.SQLiteGetSecureCon(initCloudSystem.db_file);                    
            //        cmdRemove = AppDBConRemove.CreateCommand();
            //        cmdRemove.CommandText = "delete from FrontBodyPositionKneedown where UniqueId = " + uniqueidToRemove + "; " +
            //            "delete from FrontBodyPositionStanding where UniqueId = " + uniqueidToRemove + "; " +
            //            "delete from SideBodyPosition where UniqueId = " + uniqueidToRemove + ";" +
            //            "delete from PatientDetails where UniqueId = " + uniqueidToRemove + "; ";
            //        AppDBConRemove.Open();
            //        cmdRemove.ExecuteNonQuery();
            //        AppDBConRemove.Close();
            //        isDBInconsistent = true;
            //    }
            //}
            //return isDBInconsistent;
            #endregion
        }
    }

   
}
