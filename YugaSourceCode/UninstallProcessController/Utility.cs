﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UninstallProcessController
{
    public static class Utility
    {

        /// <summary>
        /// Added by Sumit for GSP-1181
        /// Function returns the connection string as per solution build mode (DEBUG = nonProtected or  RELEASE = protected)
        /// </summary>
        /// <param name="dbpath"></param>
        /// <returns></returns>
        public static System.Data.SQLite.SQLiteConnection SQLiteGetSecureCon(string dbpath)
        {

            System.Data.SQLite.SQLiteConnection con;

#if DEBUG
            if (dbpath.ToUpper().Contains("Data Source".ToUpper()))
            {
                con = new System.Data.SQLite.SQLiteConnection(dbpath);
            }
            else
            {
                con = new System.Data.SQLite.SQLiteConnection("Data Source=" + dbpath);
            }

#else
            if(dbpath.ToUpper().Contains("Data Source".ToUpper()))
            {
            con = new System.Data.SQLite.SQLiteConnection(dbpath.Replace(";", "") + ";Version=3;password=" + ConvertToEncrypt("6G5s4Tdf45sT") + ";");
            }
            else
            {
            con = new System.Data.SQLite.SQLiteConnection("Data Source=" + dbpath.Replace(";", "") + ";Version=3;password=" + ConvertToEncrypt("6G5s4Tdf45sT") + ";");
            }
            
#endif
            return con;

        }
        public static string ConvertToEncrypt(string valueToEncrypt)
        {
            byte[] bt = UTF8Encoding.UTF8.GetBytes(valueToEncrypt);
            string s = Convert.ToBase64String(bt);
            return s;
        }
    }
}
