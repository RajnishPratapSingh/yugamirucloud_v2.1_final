﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace UninstallProcessController
{
    class ReloadWin10TaskBar
    {
        [Flags]
        private enum HChangeNotifyEventID
        {
            SHCNE_ASSOCCHANGED = 0x08000000,
        }

        [Flags]
        private enum HChangeNotifyFlags
        {
            SHCNF_IDLIST = 0x0000
        }

        [Flags]
        private enum SendMessageTimeoutFlags : uint
        {
            SMTO_ABORTIFHUNG = 0x2,
        }

        [DllImport("shell32.dll")]
        private static extern void SHChangeNotify(HChangeNotifyEventID wEventId, HChangeNotifyFlags uFlags, IntPtr dwItem1, IntPtr dwItem2);
    }
}
