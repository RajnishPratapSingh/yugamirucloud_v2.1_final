﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.Data.SQLite;
using System.Data;
using System.Diagnostics;
using System.IO;
using Shell32;
using System.Runtime.InteropServices;
using System.Text;


namespace UninstallProcessController
{
    static class Program
    {

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            {
                //Test
                //Process.Start(System.Windows.Forms.Application.StartupPath + "\\RemovePinnedIcon.bat");
                //Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe");
                //return;
                //Test
                ////Added by Sumit for GTL#406   ---START
                //try
                //{
                //    System.Diagnostics.Process process = new System.Diagnostics.Process();
                //    process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\RemovePinnedIcon.bat";
                //    process.Start();
                //    MessageBox.Show("Remove Icon file executed success.");
                //}
                //catch(Exception ex)
                //{
                //    MessageBox.Show("Remove Icon file execution failed: " + ex.StackTrace);
                //}

                // Check whether your app is currently pinned
                //IsCurrentAppPinnedAsync();

                //return;
                //bool isPinned = TaskbarManager.GetDefault().IsCurrentAppPinnedAsync();

                //if (isPinned)
                //{
                //    // The app is already pinned--no point in asking to pin it again!
                //}
                //else
                //{
                //    //The app is not pinned. 
                //}



                ////Added by Sumit for GTL#406   ---END

            }
            string languageConfig_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\LanguageConfig.txt";
            string languageSettingContent = System.IO.File.ReadAllText(languageConfig_file);
            if (languageSettingContent.Contains(@"""" + "en" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(@"en");
            }
            else if (languageSettingContent.Contains(@"""" + "ja-JP" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
            }
            //Added by Sumit GTL#122  ---START            
            try
            {
                Process[] ps = Process.GetProcesses();
                foreach (Process kp in ps)
                {
                    if (kp.ProcessName.ToUpper().Contains("YUGAMIRU") && !kp.ProcessName.ToUpper().Contains("CLOUD"))
                    {
                        //kp.CloseMainWindow();
                        kp.Kill();
                    }
                }
            }
            catch
            {
                
            }
            //Added by Sumit GTL#122  ---END

            //Condition added Sumit GTL#94 (NR Task)
            //Application.Run(new Form1());
            if (GetCount() > 0)
            {
                Application.Run(new Form1());
            }
            else
            {
                //Release the key and Delete from ProgramData now as going to uninstall
                ReleaseAndClean();
            }


            //Added by Sumit for GTL#406   ---START
            { 
            //try
            //{
                
            //    try
            //    {
            //        System.Diagnostics.Process process = new System.Diagnostics.Process();
            //        process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\RemovePinnedIcon.bat";
            //        process.Start();
            //        //MessageBox.Show("Remove Icon file executed success.");
            //    }
            //    catch(Exception ex)
            //    {
            //        MessageBox.Show("Remove Icon file execution failed: " + ex.StackTrace);
            //    }
            //    //try
            //    //{
            //    //    System.Diagnostics.Process process = new System.Diagnostics.Process();
            //    //    process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\Unload.bat";
            //    //    process.Start();
            //    //    //MessageBox.Show("Unload executed success.");
            //    //}
            //    //catch(Exception ex)
            //    //{
            //    //    MessageBox.Show("Unload execution failed: " + ex.StackTrace);
            //    //}
            //    try
            //    {
            //        System.Diagnostics.Process process = new System.Diagnostics.Process();
            //        //process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\Reload.bat";
            //        process.StartInfo.UseShellExecute = true;
            //        //process.StartInfo.FileName = Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe";
            //        process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\Reload.bat";
            //        process.StartInfo.WorkingDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Windows);                    
            //        process.StartInfo.Verb = "runas";
            //        process.Start();
            //        //System.Threading.Thread.Sleep(1);
            //        //Process.Start(Environment.GetFolderPath(Environment.SpecialFolder.Windows) + @"\explorer.exe");
            //        //Process.Start(Environment.SystemDirectory + @"C:\Windows\explorer.exe");
            //        //MessageBox.Show("Reload executed success.");
            //    }
            //    catch(Exception ex)
            //    {
            //        MessageBox.Show("Reload execution failed: " + ex.StackTrace);
            //    }

            //}
            //catch (Exception ex)
            //{
            //    MessageBox.Show("Remove Icon failed: " + ex.StackTrace);
            //}
            //Added by Sumit for GTL#406   ---END
            
            
                //Added by Sumit for GTL#406   ---START
                //System.Diagnostics.Process process = new System.Diagnostics.Process();
                //process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\YugaUIRefresh.bat";
                //process.Start();
                //Added by Sumit for GTL#406   ---END

                //Added by Sumit for GTL#406   ---START
                //SHChangeNotify(HChangeNotifyEventID.SHCNE_ASSOCCHANGED, HChangeNotifyFlags.SHCNF_IDLIST, IntPtr.Zero, IntPtr.Zero);
                //try
                //{
                //    SHChangeNotify(HChangeNotifyEventID.SHCNE_DELETE, HChangeNotifyFlags.SHCNF_IDLIST, IntPtr.Zero, IntPtr.Zero);
                //}
                //catch(Exception exNotify)
                //{
                //    MessageBox.Show(exNotify.StackTrace);
                //}
                //Added by Sumit for GTL#406   ---END
            }
        }

        /// <summary>
        /// Added by Sumit for GTL#406
        /// </summary>
        private static void UnpinTaskBar()
        {
            //string filePath = Application.StartupPath + @"\Yugamiru.exe";
            string filePath = @"C:\Program Files (x86)\gsport inc\yugamiru cloud\Yugamiru.exe";
            bool pin = false;
            if (!File.Exists(filePath)) throw new FileNotFoundException(filePath);

            // create the shell application object
            Shell shellApplication = new ShellClass();

            string path = Path.GetDirectoryName(filePath);
            string fileName = Path.GetFileName(filePath);

            Folder directory = shellApplication.NameSpace(path);
            FolderItem link = directory.ParseName(fileName);
            

            FolderItemVerbs verbs = link.Verbs();
            MessageBox.Show("Verb count = " + verbs.Count);
            string vrbs = "";
            for (int i = 0; i < verbs.Count; i++)
            {                
                FolderItemVerb verb = verbs.Item(i);
                
                string verbName = verb.Name.Replace(@"&", string.Empty).ToLower();
                vrbs += verbName;
                if ((pin && verbName.Equals("pin to taskbar")) || (!pin && verbName.Equals("unpin from taskbar")))
                {
                    MessageBox.Show(verbName);
                    
                    verb.DoIt();
                }
            }
            MessageBox.Show(vrbs);
            shellApplication = null;
        }

        /// <summary>
        /// Function added Sumit GTL#94   (NR Task)
        /// </summary>
        /// <returns></returns>
        public static int GetCount()
        {
            SQLiteDataAdapter ad;
            SQLiteConnection cn;
            int retval = 0;
            DataTable dtTotal = new DataTable();            
            string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
            "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
            try
            {                
                //Edited by Sumit for GSP-1181   ---START
                //cn = new SQLiteConnection("Data Source=" + db_file);
                cn = Utility.SQLiteGetSecureCon("Data Source=" + db_file);
                //Edited by Sumit for GSP-1181   ---END

                using (SQLiteCommand cmd = new SQLiteCommand(cn))
                {
                    //Total-----------------
                    cn.Open();
                    //cmd = cn.CreateCommand();
                    cmd.CommandText = "select count(*) from PatientDetails";  //set the passed query
                    using (ad = new SQLiteDataAdapter(cmd))
                    {
                        ad.Fill(dtTotal);
                    }
                    cn.Close();
                    if (dtTotal.Rows.Count > 0)
                    {
                        try
                        {
                            retval = Convert.ToInt16(dtTotal.Rows[0][0].ToString().Trim());
                        }
                        catch { }
                     
                    }
                    //retval = dtTotal.Rows.Count;
                }
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
            }
            return retval;
        }

        /// <summary>
        /// Function added Sumit GTL#94   (NR Task)
        /// </summary>
        static void ReleaseAndClean()
        {
            string db_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
            string deleteAddressDatabase = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                 "\\gsport\\database";
            string deleteAddressGsport = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                 "\\gsport";

            //Release the key now as going to uninstall
            CloudManager.GlobalItems.ReleaseKeyFromUnInstaller();
            //isCancelled = false;
            //System.Threading.Thread.Sleep(1000);
            try
            {
                if (System.IO.File.Exists(db_file))
                    System.IO.File.Delete(db_file);
            }
            catch (Exception ex)
            {

            }

            try
            {
                if (System.IO.Directory.Exists(deleteAddressDatabase))
                    System.IO.Directory.Delete(deleteAddressDatabase, true);
            }
            catch
            {

            }
            //System.Threading.Thread.Sleep(100);
            try
            {
                if (System.IO.Directory.Exists(deleteAddressDatabase))
                    System.IO.Directory.Delete(deleteAddressDatabase, true);
            }
            catch
            {

            }
            try
            {
                if (System.IO.Directory.Exists(deleteAddressGsport))
                    System.IO.Directory.Delete(deleteAddressGsport, true);
            }
            catch
            {

            }
            //System.Threading.Thread.Sleep(100);
            try
            {
                if (System.IO.Directory.Exists(deleteAddressGsport))
                    System.IO.Directory.Delete(deleteAddressGsport, true);
            }
            catch
            {

            }            
        }

        //Unpin Testing
        #region Commented_Only
            /*
        //PinUpin Application from Taskbar in Win7
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true, BestFitMapping = false, ThrowOnUnmappableChar = true)]
        internal static extern IntPtr LoadLibrary(string lpLibFileName);

        [DllImport("user32.dll", CharSet = CharSet.Auto, SetLastError = true, BestFitMapping = false, ThrowOnUnmappableChar = true)]
        internal static extern int LoadString(IntPtr hInstance, uint wID, StringBuilder lpBuffer, int nBufferMax);

        public static bool PinUnpinTaskbar(bool pin)
        {
            string filePath = Application.StartupPath + @"\Yugamiru.exe";
            //string filePath = @"C:\Program Files (x86)\gsport inc\yugamiru cloud\Yugamiru.exe";
            //bool pin = false;

            if (!File.Exists(filePath)) throw new FileNotFoundException(filePath);
            int MAX_PATH = 255;
            var actionIndex = pin ? 5386 : 5387; 
            StringBuilder szPinToStartLocalized = new StringBuilder(MAX_PATH);
            IntPtr hShell32 = LoadLibrary("Shell32.dll");
            LoadString(hShell32, (uint)actionIndex, szPinToStartLocalized, MAX_PATH);
            string localizedVerb = szPinToStartLocalized.ToString();

            string path = Path.GetDirectoryName(filePath);
            string fileName = Path.GetFileName(filePath);

            // create the shell application object
            dynamic shellApplication = Activator.CreateInstance(Type.GetTypeFromProgID("Shell.Application"));
            dynamic directory = shellApplication.NameSpace(path);
            dynamic link = directory.ParseName(fileName);

            dynamic verbs = link.Verbs();
            for (int i = 0; i < verbs.Count(); i++)
            {
                dynamic verb = verbs.Item(i);
                if (verb.Name.Equals(localizedVerb))
                {
                    verb.DoIt();
                    return true;
                }
                ////var verb = verbs.Item(i);
                //if (verb.Name.ToString().ToUpper().Contains("UNPIN"))
                //{
                //    verb.DoIt();
                //    //return true;
                //}
            }
            return false;
        }

        
        //end
        [Flags]
        private enum HChangeNotifyEventID
        {
            SHCNE_ASSOCCHANGED = 0x08000000,
            SHCNE_DELETE = 0x00000004
        }

        [Flags]
        private enum HChangeNotifyFlags
        {
            SHCNF_IDLIST = 0x0000
        }

        [Flags]
        private enum SendMessageTimeoutFlags : uint
        {
            SMTO_ABORTIFHUNG = 0x2,
        }

        [DllImport("shell32.dll")]
        private static extern void SHChangeNotify(HChangeNotifyEventID wEventId, HChangeNotifyFlags uFlags, IntPtr dwItem1, IntPtr dwItem2);
        //unpin
        //[DllImport(@"C:\Windows\System32\Windows.UI.Shell.dll")]
        //private static extern void IsCurrentAppPinnedAsync(); 
        */
#endregion
    }
}
