﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Data.SQLite;
using Newtonsoft.Json.Bson;
using System.Diagnostics;
using System.Text.RegularExpressions;

namespace WebComCation
{
    public partial class KeyValidator : Form
    {
        public static bool IsClose;
        Registry_Handler reg = new Registry_Handler();
        ServerCommunicator web = new ServerCommunicator();
        ServerOutInMsg regmsg = new ServerOutInMsg();
        ServerOutInMsg srvrmsg = new ServerOutInMsg();
        
        string jsonQuery = "";
        keyRequest req;
        public KeyValidator()
        {
            InitializeComponent();
            //

            // regmsg = reg.GetStoredRegistryMsg();
            // srvrmsg = web.SendAndGetResponse();
            jsonQuery = reg.RegToJsonRawMsgToSend(regmsg);

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string text = "";
            if (txtLicKey.Text.Trim().Length == 0)
            {
                //   MessageBox.Show("Please provide Activation key", "GSPORT");
                //Edited By Suhana For GSP-450
                MessageBox.Show(Properties.Resources.REQUEST_ACTIVATION_KEY, "Yugamiru");
                //Edited By Suhana For GSP-450
                return;
            }
            textBox1.Clear();

            if (!Utility.IsInternetConnected())
            {
                MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
                return;
            }
            else
            {
                try
                {
                    #region Validate_PC_Against_PC_Set_To_Future_Date
                    //Added by Sumit to cover PC set to future date scenario GTL#198   ---START
                    if (!DateTemperingHandler.IsPCDateValidWithNetForActivation())
                    {
                        Process[] allp = Process.GetProcesses();
                        foreach (Process p in allp)
                        {
                            if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                            {
                                p.CloseMainWindow();
                            }
                        }
                        //MessageBox.Show("Cannot continue application, PC date time is not correct");
                        foreach (Form frm in Application.OpenForms)
                        {

                            frm.Invoke((MethodInvoker)delegate
                            {
                            //Edited by Sumit GSP-1321  -------START                           
                            //MessageBox.Show(frm, Properties.Resources.CANNOT_ACTIVATE_INCORRECT_PC_DATE, "Yugamiru");
                            //Updated by Sumit For GTL#141   ---START
                            //MessageBox.Show(frm, Properties.Resources.CANNOT_CONTINUE_DATETIME_INCORRECT, "Yugamiru");                            
                            Cleaner.ScrosheetCleaner();
                                MessageBox.Show(frm, Properties.Resources.CANNOT_CONTINUE_DATETIME_INCORRECT, "Yugamiru");
                                Environment.Exit(0);
                            //Updated by Sumit For GTL#141   ---END
                            //Edited by Sumit GSP-1321  -------END
                        });
                            break;
                        }
                        Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                        Environment.Exit(0);
                    }
                    #endregion
                    //Check PC date first, if valid then only allow activation.
                    if (!DateTemperingHandler.IsPCDateValidWithNet())
                    {
                        MessageBox.Show(Properties.Resources.CANNOT_ACTIVATE_INCORRECT_PC_DATE, "Yugamiru");
                        return;
                    }
                }
                catch
                {
                    System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
                }
            }


            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //Updated by Sumit GTL#130 (NR Task)  ---START
                //string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                //                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                //                    "\",\"Activation_key\":\"" + txtLicKey.Text + "\",\"Language\":\"" + LicenseValidator.sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]"; //Added by Rajnish for GSP - 880 and GSP - 881
                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + txtLicKey.Text.Trim() + "\",\"Language\":\"" + LicenseValidator.sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]"; //Added by Rajnish for GSP - 880 and GSP - 881

                //Updated by Sumit GTL#130 (NR Task)  ---END
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                WebRequest webRequest = WebRequest.Create(StarterLicense.activateURL);


                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
                            //Added Sumit GSP-775 on 28-Aug-18 START
                            WebComCation.FaultManager.LogIt(ex);
                            //Added Sumit GSP-775 on 28-Aug-18 END

                            //Added Sumit GSP-775 on 28-Aug-18 START
                            WebComCation.FaultManager.LogIt(ex1);
                            //Added Sumit GSP-775 on 28-Aug-18 END
                            return;
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);

                //Try Catch added by sumit for GSP-1078-----------START
                //String responseFromServer = webResponseSReader.ReadToEnd();
                String responseFromServer = "";

                try
                {
                    responseFromServer = webResponseSReader.ReadToEnd();
                    //Added by Sumit GSP-1376   ------START

                    if (responseFromServer.ToUpper().Contains("TRIAL"))
                    {
                        LicenseValidator.sSubscriptionType = "TRIAL";
                    }
                    else
                    {
                        if (responseFromServer.ToUpper().Contains("SUBSCRIBED"))
                            LicenseValidator.sSubscriptionType = "SUBSCRIBED";
                    }
                    //Added by Sumit GSP-1376   ------END
                }
                catch
                {
                    if (!Utility.IsInternetConnected())
                        System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");

                    return;
                }
                //Try Catch added by sumit for GSP-1078-----------END

                #region QLM Down Activation Scenario Sumit GSP-1321
                //Added by Sumit GSP-1321 ----START
                #region QLM Down Activation Invalid Key case
                //Edited by Sumit for GTL#191   ---START
                /*
                 if((responseFromServer.Trim().ToUpper().Contains("Invalid".ToUpper()) ||
                    responseFromServer.Trim().ToUpper().Contains("Language Invalid".ToUpper()))
                    && (!responseFromServer.Contains("Trial Expired")))
                {
                    MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY, "Yugamiru");
                    textBox1.Text = Properties.Resources.INVALID_LICENSE_KEY;
                    return;
                }
                 */
                if ((responseFromServer.Trim().ToUpper().Contains("Invalid".ToUpper()) ||
                    responseFromServer.Trim().ToUpper().Contains("Language Invalid".ToUpper()))
                    && (!responseFromServer.Contains("Trial Expired") && !responseFromServer.ToUpper().Contains("has expired".ToUpper())))
                {
                    MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY, "Yugamiru");
                    textBox1.Text = Properties.Resources.INVALID_LICENSE_KEY;
                    return;
                }//has expired
                else if ((responseFromServer.Trim().ToUpper().Contains("Invalid".ToUpper())
                    && (responseFromServer.ToUpper().Contains("has expired".ToUpper()))))
                {
                    LicenseValidator.objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;
                    //LicenseValidator.objfrmCustomMessageBox.btnDetails.Text = Properties.Resources.SHOW_DETAILS;
                    LicenseValidator.objfrmCustomMessageBox.tbDetails.Text = Properties.Resources.ACTIVATION_KEY + ": " + this.txtLicKey.Text;// + Environment.NewLine + Properties.Resources.EXPIRY_DATE + ": " + WebComCation.LicenseValidator.sExpiryDate;
                    LicenseValidator.objfrmCustomMessageBox.btnOK.Focus();
                    LicenseValidator.objfrmCustomMessageBox.TopMost = true;
                    LicenseValidator.objfrmCustomMessageBox.ShowDialog();
                    //----------------------//
                    // MessageBox.Show("Activation Key Expired");//+ p1);//--Commented by Rajnish for GSP-768//
                    textBox1.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;//+ p1;
                    return;
                }
                //Edited by Sumit for GTL#191   ---END
                #endregion

                #region QLM Down Activation Valid Key success case

                if (responseFromServer.Trim().ToUpper().Contains("QlmStatus".ToUpper()) &&
                   responseFromServer.Trim().ToUpper().Contains("Activated".ToUpper()) &&
                   responseFromServer.Trim().ToUpper().Contains("Down".ToUpper()))
                {
                    //Handle QLM Down Successfull Activation (Temporary Activation for Three Days)
                    if (SaveAndTemporarelyActivateApplication(responseFromServer))
                    {
                        textBox1.Text = Properties.Resources.KEY_ACTIVATED;
                        System.Windows.Forms.MessageBox.Show(Properties.Resources.KEY_ACTIVATED, "Yugamiru");
                        DateTemperingHandler.SavePresentTimeStamp();
                        Utility.SavePresentTimeStamp(); //Added By Suhana
                        this.Close();
                        return;
                    }
                }

                #endregion

                //Added by Sumit GSP-1321 ----END
                #endregion



                #region License_Expired_Case
                if (responseFromServer.Contains("Trial Expired"))//Trial Expired
                {
                    MessageBox.Show(Properties.Resources.ALREADY_REGISTERED_TRIAL, "Yugamiru");
                    textBox1.Text = Properties.Resources.ALREADY_REGISTERED_TRIAL;
                    return;
                }
                else if (responseFromServer.Contains("invalid") && responseFromServer.Contains("expired"))
                {
                    int n1 = responseFromServer.IndexOf("Message");
                    string p1 = responseFromServer.Substring(n1 + 10);
                    p1 = p1.Replace("\0", "");
                    p1 = p1.Replace(@"\u", "");
                    p1 = p1.Replace(@"�", "");//                    
                    p1 = p1.Replace(@":", "");
                    p1 = p1.Substring(0, 112);

                    //--Added By Rajnish For GSP-768--//
                    LicenseValidator.objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;
                    LicenseValidator.objfrmCustomMessageBox.tbDetails.Text = Properties.Resources.ACTIVATION_KEY + ": " + WebComCation.LicenseValidator.sActivationKey + Environment.NewLine + Properties.Resources.EXPIRY_DATE + ": " + WebComCation.LicenseValidator.sExpiryDate;
                    LicenseValidator.objfrmCustomMessageBox.btnOK.Focus();
                    LicenseValidator.objfrmCustomMessageBox.TopMost = true;
                    LicenseValidator.objfrmCustomMessageBox.ShowDialog();
                    //----------------------//

                    // MessageBox.Show("Activation Key Expired");//+ p1);//--Commented by Rajnish for GSP-768//
                    textBox1.Text = Properties.Resources.ACTIVATION_KEY_EXPIRED;//+ p1;
                    return;
                    //Application.Exit();
                }
                #endregion


                #region Invalid_License
                else if (responseFromServer.Contains("invalid") || responseFromServer.Contains("Language Invalid"))
                {

                    string holdrenewMsg = LicenseValidator.GetRenewedComputerKey("", "");
                    //Edited By Suhana for GSP-450
                    MessageBox.Show(Properties.Resources.INVALID_LICENSE_KEY, "Yugamiru");
                    //Edited By Suhana For GSP-450
                    // MessageBox.Show("Invalid Activation Key");
                    //Application.Exit();
                    textBox1.Text = Properties.Resources.INVALID_LICENSE_KEY;
                    return;
                }
                #endregion


                #region Valid_License

                string sw = responseFromServer.Replace("?", "");
                sw = sw.Replace("\0", "");
                sw = sw.Replace(@"\u", "");
                sw = sw.Replace(@"�", "");//
                //sw = sw.Replace(sw[7].ToString(), "");//
                sw = sw.Replace("\u0002", "-");
                sw = sw.Replace("\u0016", "");
                sw = sw.Replace("\u0006", "");
                sw = sw.Replace("\u0010", "");

                string CPK = sw;
                string strrm = sw;
                //Edited by Sumit on 16-Nov-18 GSP-918----------START
                //CPK = CPK.Replace("Status-ComputerKey ", "");
                //CPK = CPK.Replace("-LicenseType-S-MessageLicense Key Activated-Keyvalid-Languagevalid", "");//Added by Rajnish for GSP - 880 and GSP - 881
                CPK = CPK.Replace("Status-ComputerKey ", "").Split('-')[0];
                //Edited by Sumit on 16-Nov-18 GSP-918----------START

                //Updating logic Sumit GSP-1378    -------START
                if (!sw.ToUpper().Contains("QLMSTATUS"))
                {
                    List<string> lstParts = new List<string>(sw.Split(new string[] { "-" }, StringSplitOptions.RemoveEmptyEntries));
                    string strHavingCK = "";
                    foreach (string str in lstParts)
                    {
                        if (str.ToUpper().Contains("COMPUTERKEY"))
                        {
                            strHavingCK = str;
                            break;
                        }
                    }
                    CPK = strHavingCK.ToUpper().Replace("COMPUTERKEY", "").Trim();
                }
                else if (sw.Trim().ToUpper().Contains("COMPUTERKEY") && sw.Trim().ToUpper().Contains("LANGUAGE"))
                {
                    string[] tmp1 = new string[] { "COMPUTERKEY" };
                    string[] tmp2 = new string[] { "LANGUAGE" };
                    CPK = sw.ToUpper().Split(tmp1, StringSplitOptions.None)[1];
                    CPK = CPK.Split(tmp2, StringSplitOptions.None)[0];
                    CPK = CPK.Replace("\0", "");
                    CPK = CPK.Replace("-", "");
                }
                else
                {
                    sw = (sw.Split('-').Length == 3) ? sw.Split('-')[1] : sw;
                    sw = sw.ToUpper().Replace("COMPUTERKEY", "").Replace(" ", "");
                    CPK = sw;
                }

                //Updating logic Sumit GSP-1378    -------END
                #region Store_Computer_Key_Locally

                //Keys.ReadKey();

                //QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //Keys.ActivationKey = txtLicKey.Text.Trim();
                //Keys.ComputerKey = CPK;
                //=========================
                LicenseStatus st = LicenseStatus.Unknown;
                //Fetch locally stored values first
                //Updated by Sumit for GTL#130 (NRT)  ---START
                //string LicenseKey = txtLicKey.Text;
                string LicenseKey = txtLicKey.Text.Trim();
                //Updated by Sumit for GTL#130   (NRT)---END
                string ComputerKey = CPK;//Registry_Handler.GetComputerKeyFromReg();
                string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                //store keys first.
                string ak = LicenseKey; //Validation Key
                string ck = ComputerKey; // Computer Key

                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---START
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                string IsKeyDeleted = string.Empty;
                ql.DeleteKeys(out IsKeyDeleted);
                //Added by Sumit GSP-775 on 04-Sep-18 1322 ---END

                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//sumit testing july
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;

                //Added by Sumit for GSP-821 on 24-Sep-2018-----------START
                //ql.ProxyUser = Environment.UserName;
                ql.ProxyUser = DateTime.Now.ToString("yyyy MM dd HH:mm:ss").Replace(" ", "").Replace(":", "");
                //Added by Sumit for GSP-821 on 24-Sep-2018-----------END

                ql.StoreKeys(ak, ck);
                //========FOR TESTING=========
                string lk = "";
                string cmk = "";
                ql.ReadKeys(ref lk, ref cmk);
                //===================TESTING END
                ///Added by Sumit GSP-705 ----START
                SecureAccessManager.AccessRightsHandler.SetLicenseReleaseableForAll();
                //Added by Sumit GSP-705 ----END
                #endregion


                #region Not_In_Use
                //if (strrm.Contains("invalid"))
                //{
                //    //keyStatus = LicenseStatus.Invalid;
                //    strrm = strrm.Replace("invalid", "");
                //    System.Windows.Forms.MessageBox.Show("Key is Invalid");
                //    textBox1.Text = "Key is Invalid";
                //    return;
                //}
                //else if (strrm.Contains("valid"))
                //{
                //keyStatus = LicenseStatus.Valid;
                //msg.Status = LicenseStatus.Valid.ToString();
                //msg.keyStatus = LicenseStatus.Valid.ToString();
                //msg.ServerMessage = "License Key is valid";
                //strrm = strrm.Replace("valid", "");
                //                }
                //                //System.Windows.Forms.MessageBox.Show(strrm);
                //                //ServerOutInMsg msg = new ServerOutInMsg();

                //                try
                //                {
                //                    //msg.ActivationDate = actdt;//dcValuePairs["ActivationDate"];
                //                    //msg.ComputerID = CPK;//dcValuePairs["ComputerID"];
                //                    //msg.ComputerName = cpname;//dcValuePairs["ComputerName"];
                //                    //msg.EndDate = eddt; //dcValuePairs["EndDate"];
                //                    //msg.LicenseKey = txtLicKey.Text;// dcValuePairs["LicenseKey"];
                //                    //msg.ServerMessage = strrm;//dcValuePairs["Message"];
                //                    //msg.StartDate = Stdt;//dcValuePairs["StartDate"];
                //                    //msg.Status = keyStatus.ToString();//dcValuePairs["Status"];
                //                    //msg.keyStatus = keyStatus.ToString();//dcValuePairs["keyStatus"];
                //                    //msg.LastRunDate = Utility.DateTimeToString(DateTime.Now); //dcValuePairs["LastRunDate"];
                //                    //Registry_Handler.SaveToRegistry(msg);
                //                }
                #endregion
                //                catch (Exception ex)
                //                {
                //                    throw new Exception("Message from server is not in correct Format" + Environment.NewLine + ex.Message);

                //                }
                #endregion

                textBox1.Text = Properties.Resources.KEY_ACTIVATED;//commented by Sumit Exception At Activation after this line change
                //Edited By Suhana For GSP-450
                System.Windows.Forms.MessageBox.Show(Properties.Resources.KEY_ACTIVATED, "Yugamiru");
                //Added by Sumit GSP-705 ----START
                SecureAccessManager.AccessRightsHandler.SetLicenseReleaseableForAll();
                //Added by Sumit GSP-705 ----END

                //Added by Sumit for GTL#192   ---START 
                //Clear old records if there is any
                if (Utility.GetReportsCount() > 0)
                {
                    Utility.DeleteReportsFromDB();
                }
                //Added by Sumit for GTL#192   ---END


                //System.Windows.Forms.MessageBox.Show("Application activated successfully", "GSPORT");
                //Edited By Suhana For GSP-450

                //Added by Sumit GSP-821 on 25-Sep-2018-----START
                DateTemperingHandler.SavePresentTimeStamp();
                #region sSubscriptionType
                //Added by Sumit for GSP-1345 ---------start
                if (responseFromServer.ToUpper().Contains("TRIAL"))
                {
                    //sSubscriptionType = "TRIAL";
                    DateTemperingHandler.SaveSubscriptionType("TRIAL");
                }
                else
                {
                    if (responseFromServer.ToUpper().Contains("SUBSCRIBED"))
                    {
                        //sSubscriptionType = "SUBSCRIBED";
                        DateTemperingHandler.SaveSubscriptionType("SUBSCRIBED");
                    }
                }
                //Added by Sumit for GSP-1345 ---------End
                #endregion
                //Added by Sumit GSP-821 on 25-Sep-2018-----END
                Utility.SavePresentTimeStamp(); //Added By Suhana

                //Update Install Info 
                //Added by sumit GSP-966------START
                UpdateInstallInfo();
                //Added by sumit GSP-966------END

                //Commenting progressbar GSP-1174-----START
                //if (LicenseValidator.IsKVFromLaunch == true)//--Added by Rajnish For GSP-782--//
                //{
                //    Process myp = new Process();
                //    myp.StartInfo.FileName = Application.StartupPath + "\\yugamiruPGB.exe";
                //    myp.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
                //    myp.Start();
                //}
                //Commenting progressbar GSP-1174-----END
                //// close everything
                webResponseSReader.Close();
                webResponse.Close();
                webDataStream.Close();
                this.Close();
                #endregion

            }
            catch (WebException we) //Any how key is invalid
            {
                #region Commented_Out
                //using (WebResponse response = we.Response)
                //{
                //    HttpWebResponse httpResponse = (HttpWebResponse)response;
                //    Console.WriteLine("Error code: {0}", httpResponse.StatusCode);
                //    string messageFromServer = string.Empty;
                //    using (Stream data = response.GetResponseStream())
                //    using (var reader = new StreamReader(data))
                //    {
                //        // text is the response body
                //        messageFromServer = reader.ReadToEnd();
                //    }

                //    // You now have the JSON text in the responseFromServer variable, use it :)
                //    string[] responseFromServer_1 = messageFromServer.Split(','); //.Replace(',', '\\');
                //    string sl = "";
                //    string servermsg = string.Empty;
                //    foreach (string s in responseFromServer_1)
                //    {
                //        sl += s + Environment.NewLine;

                //        if (s.Contains("\"errormessage\":"))
                //        {
                //            servermsg = s.Replace("\"errormessage\":", "");
                //        }

                //    }
                //    if (servermsg.Trim().Length > 0)
                //        MessageBox.Show(servermsg.Replace("\"", ""));
                //    sl = sl.Replace("\"", " ");
                //    sl = sl.Replace("{", " ");
                //    sl = sl.Replace("}", " ");
                //    sl = sl.Replace("[", " ");
                //    sl = sl.Replace("]", " ");

                //Message to show to user.
                //string msgResponse = responseFromServer_1[5];                   
                #endregion
                //Edited/Added by Sumit GSP-1351   --------START
                //textBox1.Text = textBox1.Text + Environment.NewLine + we.Message + Environment.NewLine + we.StackTrace;
                Cursor.Current = Cursors.Default;
                textBox1.Text = Properties.Resources.CANNOT_ACTIVATE_SERVER_ISSUE;
                MessageBox.Show(Properties.Resources.CANNOT_ACTIVATE_SERVER_ISSUE, "Server Error");
                //Edited/Added by Sumit GSP-1351   --------END
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(we);
                //Added Sumit GSP-775 on 28-Aug-18 END

            }
            //Added one more level Exception Handling by Sumit for GTL#477   ---START
            catch (Exception xe) //Any how key is invalid
            {

#if debug
                MessageBox.Show(xe.StackTrace);
#endif
                Cursor.Current = Cursors.Default;
                textBox1.Text = Properties.Resources.CANNOT_ACTIVATE_SERVER_ISSUE;
                MessageBox.Show(Properties.Resources.CANNOT_ACTIVATE_SERVER_ISSUE, "Server Error");
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(xe);
                //Added Sumit GSP-775 on 28-Aug-18 END

            }
            //Added one more level Exception Handling by Sumit for GTL#477   ---END
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void KeyValidator_Load(object sender, EventArgs e)
        {
            IsClose = false;
        }

        private void txtLicKey_TextChanged(object sender, EventArgs e)
        {
            textBox1.Clear();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            //IsClose = true;
            //this.Close();
            if (textBox1.Text.Contains(Properties.Resources.KEY_ACTIVATED))
            {
                //let the application launch
            }
            else
            {
                try
                {
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                }
                catch (FileLoadException elf)
                {
                    FaultManager.LogIt(elf);
                }

                catch (FieldAccessException efa)
                {
                    FaultManager.LogIt(efa);
                }
                catch (IOException eio)
                {
                    FaultManager.LogIt(eio);
                } // Sumit GSP-1299
                Environment.Exit(1);
            }
        }

        private void KeyValidator_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (textBox1.Text.Contains(Properties.Resources.KEY_ACTIVATED))
            {
                //let the application launch
            }
            else
            {
                try
                {
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                }
                catch (FileLoadException elf)
                {
                    FaultManager.LogIt(elf);
                }               
                
                catch (FieldAccessException efa)
                {
                    FaultManager.LogIt(efa);
                }
                catch (IOException eio)
                {
                    FaultManager.LogIt(eio);
                }
                
                Environment.Exit(1);
            }
        }		   

        /// <summary>
        /// Method Implpemented by Sumit GSP-1321 (QLM Down Temporary Activation)
        /// </summary>
        /// <param name="responseData">Response content string from server with Key 'License Key Activated' + 'QLM Down' status</param>
        /// <returns></returns>
        public bool SaveAndTemporarelyActivateApplication(string responseData)
        {
            bool success = false;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                //Extract Temp Computer Key (which should be a date in format YYYYMMDD e.g. 20190615 for 15-June-2019)
                List<string> lstCK_To_LType = new List<string>();
                lstCK_To_LType.AddRange(responseData.ToUpper().Trim().Split(new string[] { "COMPUTERKEY", "LICENSETYPE" }, StringSplitOptions.None));

                string dirtyDate = "";
                string dateStr = "";
                if (lstCK_To_LType.Count==3)
                {
                    dirtyDate = lstCK_To_LType[1];
                    List<string> lstT1 = new List<string>(dirtyDate.Split(new string[] { "\0" }, StringSplitOptions.None));
                    //select largest length string
                    int klen = 0;
                    int indx = 0;
                    int indR = -1;
                    foreach(string s in lstT1)
                    {
                        indR++;
                        if(klen<s.Length)
                        {
                            klen = s.Length;
                            indx = indR;
                        }
                    }
                    dateStr = (lstT1[indx].StartsWith("0")) ? lstT1[indx].TrimStart(new char[] { '0' }) : lstT1[indx];
                }

                //Updated by Sumit for GTL#130 (NR Task)   ---START
                //string LicenseKey = txtLicKey.Text;
                string LicenseKey = txtLicKey.Text.Trim();
                //Updated by Sumit for GTL#130   ---END
                string ComputerKeyTemp = "QLMDOWN_" + dateStr;                
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();   
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//sumit testing july
                ql.EvaluationPerUser = false;
                ql.FavorMachineLevelLicenseKey = true;
                ql.StoreKeys(LicenseKey, ComputerKeyTemp);
                string a = "", b = "";
                ql.ReadKeys(ref a, ref b);
                success = true;
            }
            catch(Exception ex)
            {
                success = false;
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return success;
        }


        //Method Implemented by sumit GSP-966
        /// <summary>
        /// For reading PCs self installation information and inserting the same in local DB
        /// </summary>
        void UpdateInstallInfo()
        {
            //Read the file that was placed at installation time
            //Data Format in file is JSON, it has one row to insert for system's self install info.
            //File Name: TempInstalInfoJson.txt

            //test
            string json = "";
            try
            {
                json = File.ReadAllText(@"C:\ProgramData\gsport\Yugamiru cloud\database\TempInstalInfoJson.txt");
            }
            catch (Exception ex)
            {
                return;
            }
            json = json.Replace(": ", ":");
            DataTable dt = new DataTable();
            List<string> lstColValPair = new List<string>(json.Replace("[{", "").Replace("}]", "").Replace("\"", "").Split(','));
            /*[{"Installation_name":"sdfsdfsfdsdffd","Date_of_install":"21-12-2018","Install_id":"7",
             * "reg_user_id":"855","Installed_by":"iolioliliolio","Comment":"xcvxcvxcvxvcxcv","Language":""}]*/

            string installation_name = lstColValPair[0].Split(':')[1];
            string Date_of_install = lstColValPair[1].Split(':')[1];
            string stall_id = lstColValPair[2].Split(':')[1];
            string reg_user_id = lstColValPair[3].Split(':')[1];
            string Installed_by = lstColValPair[4].Split(':')[1];
            string Comment = lstColValPair[5].Split(':')[1];
            string computer_name = Environment.MachineName;
            SQLiteConnection sqlite;

            //Added By Rajnish for GSP-487----------Start
            string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                               "\",\"Computer_name\":\"" + computer_name +
                               "\",\"Activation_key\":\"" + txtLicKey.Text.Trim() +
                               "\",\"Installation_name\":\"" + installation_name +
                               "\",\"Date_of_install\":\"" + Date_of_install +
                               "\",\"Install_id\":\"" + "" +
                               "\",\"reg_user_id\":\"" + "" +
                                "\",\"Installed_by\":\"" + Installed_by +
                                 "\",\"Comment\":\"" + Comment +
                                "\",\"Language\":\"" + LicenseValidator.sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]";
            String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);

            JsonSerializer jsonSerializer = new JsonSerializer();

            MemoryStream objBsonMemoryStream = new MemoryStream();

            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);

            jsonSerializer.Serialize(bsonWriterObject, studentObject);
            //  text = queryString;
            byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);
                                                               // WebRequest

            //Connect to our Yugamiru Web Server
            //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/validatelicense");
            WebRequest webRequest = WebRequest.Create("http://52.197.210.82/apioauthdata/index.php/home/installationdetails");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {
                if (!Utility.IsInternetConnected())
                    System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            webDataStream = webResponse.GetResponseStream();

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();

            #region Parsing_Response_Data
            string resData = responseFromServer;
            if (resData.Contains("Successfully"))
            {
                //Writedown this self installation 
                //information data in local txt file and update the same after application activation
                try
                {

                    string installId_RegId = ParseJsonInstallInfo(resData);
                    if (installId_RegId.Length > 1)
                    {
                        stall_id = Regex.Replace(installId_RegId.Split('_')[0], @"[^\w\.@+/=-]", "");
                        //installId_RegId.Split('_')[0];
                        reg_user_id = Regex.Replace(installId_RegId.Split('_')[1], @"[^\w\.@+/=-]", "");
                        //installId_RegId.Split('_')[1];
                        jsonQuery = "[{\"Installation_name\":\"" + installation_name +
                               "\",\"Date_of_install\":\"" + Date_of_install +
                               "\",\"Install_id\":\"" + stall_id +
                               "\",\"reg_user_id\":\"" + reg_user_id +
                                "\",\"Installed_by\":\"" + Installed_by +
                                 "\",\"Comment\":\"" + Comment +
                                "\",\"Language\":\"" + LicenseValidator.sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]";

                        string temp_Ins_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                                                "\\gsport\\Yugamiru cloud\\database\\TempInstalInfoJson.txt";

                        File.WriteAllText(temp_Ins_file, jsonQuery);
                    }

                }
                catch (Exception ex)
                { }
            }
            #endregion
            //Added By Rajnish for GSP-487----------END

            //Edited by Sumit for GSP-1181   ---START
            //sqlite = new SQLiteConnection("Data Source=" + Utility.db_file);
            sqlite = Utility.SQLiteGetSecureCon("Data Source=" + Utility.db_file);
            //Edited by Sumit for GSP-1181   ---END
            SQLiteCommand cmd;

#region GSP-1016
            cmd = sqlite.CreateCommand();
            cmd.CommandText = "select id from tblInstallInfo where stall_id='" + stall_id + "' AND reg_user_id='" + reg_user_id + "';";
            DataTable dtExist = new DataTable();
            SQLiteDataAdapter da = new SQLiteDataAdapter(cmd);
            da.Fill(dtExist);
            if (dtExist.Rows != null && dtExist.Rows.Count == 1)
            {
                sqlite.Open();
                cmd.CommandText = "	update tblInstallInfo set " +
                                                   " Computer_name = '" + computer_name + "'," +
                                                   " Installation_name = '" + installation_name + "'," +
                                                   " Date_of_install = '" + Date_of_install + "'," +
                                                   " Installed_by = '" + Installed_by + "'," +
                                                   " Comment = '" + Comment + "' " +
                                                   "where stall_id = '" + stall_id + "' AND reg_user_id = '" + reg_user_id + "'; ";

                cmd.ExecuteNonQuery();
                sqlite.Close();
                return;
            }
#endregion


            sqlite.Open();  //Initiate connection to the db
            //cmd = sqlite.CreateCommand();
            cmd.CommandText = "INSERT into tblInstallInfo(stall_id,	reg_user_id,	Computer_name,	Installation_name,	Date_of_install,	Installed_by,	Comment, sync)" +
                                                                      "values('" + stall_id + "','" + reg_user_id + "','" + computer_name + "'," +
                                                                       "'" + installation_name + "', '" + Date_of_install + "'," +
                                                                       "'" + Installed_by + "',  '" + Comment + "','THIS'); ";
            cmd.ExecuteNonQuery();
            sqlite.Close();

            //commented by Rajnish for  GTL#487--------Start
            //try
            //{
            //    File.Delete(@"C:\ProgramData\gsport\Yugamiru cloud\database\TempInstalInfoJson.txt");
            //}
            //catch { }
            //commented by Rajnish for GTL#487---------END
            //test
        }
        string ParseJsonInstallInfo(string bsonData)
        {
            string[] knownVals = { "Status", "Computer_id", "Install_id", "reg_user_id", "Message" };
            string[] parts1 = bsonData.Split(knownVals, StringSplitOptions.None);
            List<string> lstVals = new List<string>(parts1);

            for (int i = 1; i < lstVals.Count; i++)
            {
                lstVals[i] = lstVals[i].Replace("\09\0\0\0", "").Replace("\0", "")
                    .Replace("\u0002", "").Replace("\u0004", "");
                string q = lstVals[i].Replace("\09\0\0\0", "").Replace("\0", "")
                    .Replace("\u0002", "").Replace("\u0004", "");
            }
            if (lstVals.Count == 6)
            {
                //string retStr = "Install_id=" + lstVals[3]+", reg_user_id=" + lstVals[4];
                string retStr = lstVals[3] + "_" + lstVals[4];
                return retStr;
            }

            return "";
        }
    }
}
