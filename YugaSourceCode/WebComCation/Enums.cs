﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace WebComCation
{
	//Added three elements QlmDownValid, QlmDownInvalid, QlmDownExpired Sumit GSP-1321
    public enum LicenseStatus { Valid=1,Invalid=0,Unknown=-1,Expired=2,Error=3,ReleasedFromServer=4, QlmDownValid = 5, QlmDownInvalid = 6, QlmDownExpired = 7 };


    //Added by Sumit for GTL#221   ---START
    public enum KeyLinkedDataMigrationStatus
    {
        Migrated = 0, OneRecordError = 1, NoDataFreshKey = 2,
        Exception = 3, Unknown = 4, InvalidResponse = 5, ServerError = 6, ParsingError = 7
    }
    //Added by Sumit for GTL#221   ---START
}
