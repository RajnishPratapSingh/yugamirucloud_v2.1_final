﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;

namespace WebComCation
{
    public partial class frmViewLicense : Form
    {
        public static bool ViewLicenseActive = false;
        string licenseKey = string.Empty;
        public frmViewLicense()
        {
            InitializeComponent();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        //Making Public Sumit GSP-1174
        //private void frmViewLicense_Load(object sender, EventArgs e)
        public void frmViewLicense_Load(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            //string licenseKey = Registry_Handler.GetActivationKeyFromReg();
            //if(licenseKey.Length>0)
            try
            {

                //Fetch locally stored values first
                string LicenseKey = "";// Registry_Handler.GetActivationKeyFromReg();
                string ComputerKey = "";// Registry_Handler.GetComputerKeyFromReg();
                string computerID = keyRequest.GetComputerID();
                QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                
                //store keys first.
                //string ak = LicenseKey; //Validation Key
                //string ck = ComputerKey; // Computer Key
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                    , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;
                //ql.ProxyUser = Environment.UserName;//Sumit testing July
                
                //ql.StoreKeys(ak, ck);
                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                //Added by Sumit GSP-1321  -------START
                //Commenting below (even in QLMDown we can show the details) Sumit GSP-1378    START
                //if(ComputerKey.Contains("QLMDOWN_"))
                //{
                //    txtLicense.Text = LicenseKey;
                //    txtExpiryDate.Text = "";
                //    txtActivationDate.Text = "";
                //    return;
                //}
                //Commenting below Sumit GSP-1378    END
                //Added by Sumit GSP-1321  -------END

                if (LicenseKey.Trim() == "" || ComputerKey.Trim() == "")
                {      
                    if (LicenseKey.Trim().Length>0 && ComputerKey.Trim() == "")
                    {
                        string renewedkey= LicenseValidator.GetRenewedComputerKey(LicenseKey, "");
                        if (renewedkey.Trim().ToUpper() != licenseKey.Trim().ToUpper())
                        {
                            MessageBox.Show(Properties.Resources.ACTIVATION_KEY_UPDATED, "Yugamiru", MessageBoxButtons.OK);
                            //Edited by Sumit GSP-1378    -----------START
                            //ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());
                            ql.ValidateLicenseEx(LicenseKey, keyRequest.GetComputerID());
                            if (ComputerKey.Trim().Length > 0 && !ComputerKey.Contains("QLMDOWN_"))
                            {
                                ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());
                            }
                            //Edited by Sumit GSP-1378    -----------END
                            ql.GetStatus();
                            string EndDate = ql.ExpiryDate.ToString("dd/MMM/yyyy"); //Utility.DateTimeToString(ql.ExpiryDate);
                            txtLicense.Text = LicenseKey;
                            txtExpiryDate.Text = EndDate;
                            int DaysLeft = ql.DaysLeft;
                            var lctype = ql.LicenseType;
                            txtType.Text = lctype.ToString();
                            txtActivationDate.Text = ql.DaysLeft.ToString();
                            //Added by Sumit for GTL#64   ---START
                            if (txtActivationDate.Text.Contains("-") && ComputerKey.Contains("QLMDOWN_"))
                            {
                                txtExpiryDate.Text = "Server Down Issue";
                                txtActivationDate.Text = "Server Down Issue";
                            }
                            //Added by Sumit for GTL#64   ---END
                        }
                    }
                    else
                    {
                        //MessageBox.Show("No License found on the PC", "gsport", MessageBoxButtons.OK);
                    }

                                          
                    //return ;
                }
                else if (LicenseKey.Trim() != "" && ComputerKey.Trim() != "")
                {
                    //Check Offline                   
                    //Edited by Sumit GSP-1378   -------START
                    //ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());
                    ql.ValidateLicenseEx(LicenseKey, keyRequest.GetComputerID());
                    if (ComputerKey.Trim().Length > 0 && !ComputerKey.Contains("QLMDOWN_"))
                    {
                        ql.ValidateLicenseEx(ComputerKey, keyRequest.GetComputerID());
                    }
                    //Edited by Sumit GSP-1378   -------END
                    ql.GetStatus();
                    string EndDate = ql.ExpiryDate.ToString("dd/MMM/yyyy"); //Utility.DateTimeToString(ql.ExpiryDate);
                    txtLicense.Text = LicenseKey;
                    txtExpiryDate.Text = EndDate;
                    int DaysLeft = ql.DaysLeft;
                    var lctype = ql.LicenseType;
                    txtType.Text = lctype.ToString();
                    txtActivationDate.Text = ql.DaysLeft.ToString();
                    //Added by Sumit for GTL#64   ---START
                    if (txtActivationDate.Text.Contains("-") && ComputerKey.Contains("QLMDOWN_"))
                    {
                        txtExpiryDate.Text = "Server Down Issue";
                        txtActivationDate.Text = "Server Down Issue";
                    }
                    //Added by Sumit for GTL#64   ---END

                }
            }
            finally
            {
                Cursor = Cursors.Default;
            }
            
            //else
            //{
             //   MessageBox.Show("No License found on the system", "gsport", MessageBoxButtons.OK);
            //}
           
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
			//Added by Sumit for GTL#52   ---START
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                if (Utility.IsInternetConnected(4) == false)
                {
                    System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
                    return;
                }
            }
            catch
            {

            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            //Added by Sumit for GTL#52   ---END

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                ViewLicenseActive = true;
                string st = LicenseValidator.GetRenewedComputerKey("", "", "frmViewLicense");
                if(st.ToUpper()==LicenseStatus.ReleasedFromServer.ToString().ToUpper())
                {
                    frmCustomMessageBox objfrmCustomMessageBox = new frmCustomMessageBox();
                    objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.INVALID_LICENSE_KEY;//Sumit GSP-1082
                    objfrmCustomMessageBox.tbDetails.Text = txtLicense.Text;//Sumit GSP-1082
                    objfrmCustomMessageBox.btnOK.Focus();
                    objfrmCustomMessageBox.ShowDialog();
                    frmReleaseLicense.DeleteKey();//Sumit GSP-1082
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                    Environment.Exit(0);
                }
                frmViewLicense_Load(null, null);
            }
            catch(Exception ex)
            {
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
        }

        private void frmViewLicense_FormClosing(object sender, FormClosingEventArgs e)
        {
            ViewLicenseActive = false;
        }
    }
}
