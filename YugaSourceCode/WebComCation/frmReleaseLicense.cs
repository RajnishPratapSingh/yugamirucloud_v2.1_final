﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Diagnostics;
using Newtonsoft.Json.Bson;
using Microsoft.Win32;
using System.Security.Principal;

namespace WebComCation
{
    public partial class frmReleaseLicense : Form
    {
        ServerOutInMsg msgSrvr;
        Registry_Handler reg;
        QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
        public frmReleaseLicense()
        {
            InitializeComponent();
        }

        private void frmReleaseLicense_Load(object sender, EventArgs e)
        {
            try
            {
                //reg = new Registry_Handler();
                //msgSrvr = reg.GetStoredRegistryMsg();
                string LicenseKey = string.Empty;
                string ComputerKey = string.Empty;
                ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
              , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                ql.PublicKey = ProductInfo.ProductEncryptionKey;
                ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;
                ql.EvaluationPerUser = false;//Sumit testing July
                ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                ql.DefaultWebServiceUrl = Environment.UserName;
                //ql.ProxyUser = Environment.UserName;//Sumit testing July
                ql.ReadKeys(ref LicenseKey, ref ComputerKey);
                
                txtLicense.Text = LicenseKey;//msgSrvr.LicenseKey;
                if (LicenseKey.Trim() == "")
                    btnRelease.Enabled = false;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message,"GSPORT");
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
		//Updating for Multilanguage Sumit GSP-1357 ----START
        //public string msgReleaseLicense= "License released successfully From this PC.";
		public string msgReleaseLicense = Properties.Resources.LICENSE_RELEASED;
		//Updating for Multilanguage Sumit GSP-1357 ----END
        public string msgReleaseNetProblem = Properties.Resources.PC_NOT_CONNECTED + Environment.NewLine + Properties.Resources.KEY_NOT_RELEASED;
		//Following function changed from private to public SUMIT GSP-1357 
        //private string ReleaseLicense()
		public string ReleaseLicense()
        {
            // check if offline
            using (var client = new WebClient())
            {
                try
                {
                    using (client.OpenRead("http://clients3.google.com/generate_204"))
                    {
                        //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                    }
                }
                catch (Exception ex1)
                {
                    //Added Sumit GSP-775 on 28-Aug-18 START
                    WebComCation.FaultManager.LogIt(ex1);
                    //Added Sumit GSP-775 on 28-Aug-18 END
                    MessageBox.Show(msgReleaseNetProblem,"GSPORT");
                    //---------For Debug Only System.Windows.Forms.MessageBox.Show(msgReleaseNetProblem);//("You are not connected to internet." + Environment.NewLine + "For releasing a license internet connectivity is required.");
                    return "NOINTERNET";//"Connection Error";

                }
            }
            //connectivity test done





            string msgDel = string.Empty;
            string LK = "";
            string CK = "";
            //Added by Sumit GSP-1351    --------START
            bool isReleasedFromServer = false;
            //Added by Sumit GSP-1351    --------END
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
               , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;
            ql.EvaluationPerUser = false;//Sumit testing July
            ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
            ql.DefaultWebServiceUrl = Environment.UserName;
            //ql.ProxyUser = Environment.UserName;//Sumit testing July
            ql.ReadKeys(ref LK, ref CK);
            txtLicense.Text = LK;
            //Commented from here by Sumit GSP-1351    --------START
            //ql.DeleteKeys();
            //ql.DeleteKeys(out msgDel);
            //ql.Dispose();


            //Section Added by sumit for GSP-1059-----------START
            //if (LicenseReleased().Trim().Length > 0)
            //{
            //    return "action denied";
            //}
            //GSP-1059-----------END
			//Commented from here by Sumit GSP-1351    --------END

            if (msgDel.Length == 0)
                msgDel = msgReleaseLicense;
            //MessageBox.Show(msgDel);
            //return msgDel;
            string retMsg = msgReleaseLicense;// "License released successfully From this system.";
            try
            {
                Cursor.Current = Cursors.WaitCursor;

                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + txtLicense.Text + "\"}]";
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                // text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest


                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/releasedata");
                WebRequest webRequest = WebRequest.Create(StarterLicense.releaseURL);
                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);

                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);
                String responseFromServer = webResponseSReader.ReadToEnd();
                #endregion                    
                btnRelease.Enabled = false;
                //btnCancel.Text = "Close";
                //Added by Sumit GSP-1351    --------START
                ql.DeleteKeys();
                ql.DeleteKeys(out msgDel);
                ql.Dispose();
                isReleasedFromServer = true;
                if (LicenseReleased().Trim().Length > 0)
                {
                    return "action denied";
                }
                //Added by Sumit GSP-1351    --------END
            }
            catch(Exception ex)
            {
                if (ex.Message.ToUpper().Contains("CONNECTION"))
                {
                    System.Windows.Forms.MessageBox.Show(msgReleaseNetProblem);
                    return "NOINTERNET";
                }
                else
                {

                    //Added by Sumit GSP-1351    --------START
                    isReleasedFromServer = false;
                    msgReleaseLicense = "Server Issue Cannot Release License contact support";
                    retMsg = msgReleaseLicense;
                    //Added by Sumit GSP-1351    --------END

                    //retMsg = "Cannot release this key"+Environment.NewLine+ ex.Message; required only for debug
                    //btnRelease.Text = "Cancel";
                    //MessageBox.Show(retMsg);
                }
                //Added Sumit GSP-775 on 28-Aug-18 START
                WebComCation.FaultManager.LogIt(ex);
                //Added Sumit GSP-775 on 28-Aug-18 END

            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            return retMsg;
        }  

		//Following function changed from private to public SUMIT GSP-1357         
        //private void btnRelease_Click(object sender, EventArgs e)
        public void btnRelease_Click(object sender, EventArgs e)
        {
            //Added by Sumit : GSP-1059      Only Admin can release a key------START
            //bool isAdmin;
            //using (WindowsIdentity idt = WindowsIdentity.GetCurrent())
            //{
            //    WindowsPrincipal wp = new WindowsPrincipal(idt);
            //    isAdmin = wp.IsInRole(WindowsBuiltInRole.Administrator);
            //}
            //if(!isAdmin)
            //{
            //    MessageBox.Show(Properties.Resources.ADMIN_ONLY_KEY_RELEASE, "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    return;
            //}
            //GSP-1059------END

            try
            {
                //Added by Sumit for GSP-1357   -----------START
                if (sender == null && e == null)
                {
                    QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                    ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                        , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                    ql.PublicKey = ProductInfo.ProductEncryptionKey;
                    string AK1 = "", CK1 = "";
                    ql.ReadKeys(ref AK1, ref CK1);

                    if (!Utility.IsInternetConnected())
                    {
                        ql.DeleteKeys();
                        return;
                    }

                    if (AK1.Length == 0)
                        return;
                }
                string msgDlg = ReleaseLicense();


                //added by sumit GSP-1059------START
                if (msgDlg == "action denied")
                {
                    MessageBox.Show(Properties.Resources.KEY_NOT_RELEASED + Environment.NewLine + Properties.Resources.UNAUTHORISED_RELEASED, "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //added by sumit GSP-1351------START
                else if (msgDlg.ToUpper() == "Server Issue Cannot Release License contact support".ToUpper())
                {
                    MessageBox.Show(Properties.Resources.KEY_NOT_RELEASED_SERVER_ISSUE, "Server Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                //added by sumit GSP-1351------END
                //added by sumit GSP-1059------END



                if (msgDlg != "NOINTERNET")
                {
                    QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
                    ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                        , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
                    ql.PublicKey = ProductInfo.ProductEncryptionKey;
                    ql.StoreKeysOptions = QlmLicenseLib.EStoreKeysOptions.EStoreKeysPerMachine;//testing sumit july
                    ql.EvaluationPerUser = false;//Sumit testing July
                    ql.FavorMachineLevelLicenseKey = true;//Sumit testing July
                    ql.DefaultWebServiceUrl = Environment.UserName;
                    //ql.ProxyUser = Environment.UserName;//Sumit testing July
                    //ql.StoreKeys("X", "Y");
                    //bool one;bool two;string thr = string.Empty; //Sumit Testing

                    string recheck = ReleaseLicense();
                    //Added by Sumit GSP-1357   --------START
                    if (recheck.Trim().ToUpper() == Properties.Resources.LICENSE_RELEASED.Trim().ToUpper()//"License released successfully From this PC.".ToUpper()
                        && sender == null)
                    {
                        ql.DeleteKeys();
                        //Added by Sumit GTL#358   ---START
                        Utility.DeleteInstallInfoData();
                        //Added by Sumit GTL#358   ---END
                        MessageBox.Show(recheck, "Yugamiru");
                        return;
                    }
                    //Added by Sumit GSP-1357   --------END
                    string released = LicenseReleased();
                    if (released.Trim().Length == 0)
                    {
                        if (recheck.Trim().ToUpper() == "NOINTERNET")
                        {
                            MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED + Environment.NewLine + Properties.Resources.KEY_NOT_RELEASED, "Yugamiru");
                        }
                        else
                        {
                            MessageBox.Show(recheck, "Yugamiru");
                            //Added by Sumit GTL#358   ---START
                            Utility.DeleteInstallInfoData();
                            //Added by Sumit GTL#358   ---END
                        }
                    }
                    else
                    {

                        MessageBox.Show(Properties.Resources.KEY_NOT_RELEASED + Environment.NewLine + Properties.Resources.UNAUTHORISED_RELEASED, "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //    this.Close();
                        //Added by sumit for GSP-699 on 31-July-18----------START

                        //try
                        //{
                        //    //ProcessStartInfo ykr = new ProcessStartInfo();
                        //    //ykr.Verb = "runas";
                        //    //ykr.FileName = Application.StartupPath + "\\ActivationKeyReleaseHelper.exe";
                        //    //Process.Start(ykr);
                        //    //MessageBox.Show("Activation key released successfully from this PC");
                        //    //Environment.Exit(1);
                        //    //Application.Exit();
                        //}
                        //catch (Exception ex)

                        //{
                        //    MessageBox.Show("Key not released! " + Environment.NewLine + "Loggedin user is not authorised to release license key from this PC. Contact your system admin", "Action denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        //    this.Close();
                        //}
                        //Added by sumit for GSP-699 on 31-July-18----------END



                        return;
                    }

                    //commented by Sumit GSP-1351    --------START
                    //string a = string.Empty;
                    //string b = string.Empty;
                    //ql.ReadKeys(ref a, ref b);

                    //ql.ProxyUser = "RELEASED";

                    //ql.DeleteKeys();
                    //commented by Sumit GSP-1351    --------START

                }

                if (msgDlg.ToUpper() != "NOINTERNET" && msgDlg.ToUpper() != "Server Issue Cannot Release License contact support".ToUpper())
                {
                    //this.Close();
                    //Edited by sumit GSP-1194 GSP-1212------START
                    //Application.Exit();// 
                    Cleaner.ScrosheetCleaner(); // Sumit GSP-1299
                                                //Edited (Added try catch) by Sumit for GSP-1378 (no relevant task available)  ---START
                                                //Environment.Exit(1);
                    Environment.Exit(1);
                    try
                    {
                        Environment.Exit(1);
                    }
                    catch (Exception ex)
                    {
                        System.Threading.Thread.Sleep(100);
                        try
                        {
                            Environment.Exit(1);
                        }
                        catch
                        {
                            Application.Exit();
                        }
                    }
                    //Edited by Sumit for GSP-1378   ---END
                    //Edited by sumit GSP-1194 GSP-1212------END
                }
                //Environment.Exit(1);
            }
            catch(Exception Ex)//Try Catch Added by Rajnish For GSP-1492
            {
                if (!Utility.IsInternetConnected())
                    System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");
            }
        }
        public string LicenseReleased()
        {
            QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            string ak = string.Empty;
            string ck = string.Empty;

            ql.ReadKeys(ref ak, ref ck);
            //if(ak.Trim().Length>0)
            //{
            //    LicenseValidator.ActiavteLicense("", ak);
            //}
            return ak;
        }
        //Added Function by sumit for GSP-1082
        public static string DeleteKey()
        {
            QlmLicenseLib.QlmLicense ql = new QlmLicenseLib.QlmLicense();
            ql.DefineProduct(ProductInfo.ProductID, ProductInfo.ProductName, ProductInfo.ProductVersionMajor
                , ProductInfo.ProductVersionMinor, ProductInfo.ProductEncryptionKey, ProductInfo.ProductPersistencyKey);
            ql.PublicKey = ProductInfo.ProductEncryptionKey;
            string ak = string.Empty;
            string ck = string.Empty;

            //ql.ReadKeys(ref ak, ref ck);
            ql.DeleteKeys();


            //if(ak.Trim().Length>0)
            //{
            //    LicenseValidator.ActiavteLicense("", ak);
            //}

            return ak;
        }
    }
}
