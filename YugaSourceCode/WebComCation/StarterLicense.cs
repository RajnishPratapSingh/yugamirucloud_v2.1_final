﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
//using System.Threading.Tasks;
using System.Configuration;
using System.Windows.Forms;

namespace WebComCation
{
    public class StarterLicense
    {
        public static string activateURL;
        public static string validateURL;
        public static string releaseURL;


        static StarterLicense()
        {
            //commented by rohini for GSP-1367 --START
            //activateURL = ConfigurationManager.AppSettings["Activate"];
            //validateURL = ConfigurationManager.AppSettings["Validate"];
            //releaseURL = ConfigurationManager.AppSettings["Release"];

            //commented by rohini for GSP-1367-END

            #region Other_Commented_APIs
            /*
             <!-- <add key="Activate" value="https://yugamiru.net/apioauthdata/index.php/home/activatekeydata" />
    <add key="Validate" value="https://yugamiru.net/apioauthdata/index.php/home/validatelicense" />
    <add key="Release" value="https://yugamiru.net/apioauthdata/index.php/home/releasedata" />-->

    <!--My Local PHP (Dev Server code) START
    <add key="Activate" value="http://localhost:8989/index.php/home/activatekeydata" />
    <add key="Validate" value="http://localhost:8989/index.php/home/validatelicense" />
    <add key="Release" value="http://localhost:8989/index.php/home/releasedata" />
    My Local PHP (Dev Server code) END -->
    
    <!-- Dev Server 2.1 merged code START -->
    <add key="Activate" value="http://52.197.210.82/apioauthdata/index.php/home/activatekeydata" />
    <add key="Validate" value="http://52.197.210.82/apioauthdata/index.php/home/validatelicense" />
    <add key="Release" value="http://52.197.210.82/apioauthdata/index.php/home/releasedata" />
    <!-- Dev Server 2.1 merged code END-->
             */
#endregion

            //added by rohini for GSP-1367 START
            activateURL = "http://52.197.210.82/apioauthdata/index.php/home/activatekeydata";
            validateURL = "http://52.197.210.82/apioauthdata/index.php/home/validatelicense";
            releaseURL = "http://52.197.210.82/apioauthdata/index.php/home/releasedata";
            //added by rohini for GSP-1367 END
        }
    }
}
