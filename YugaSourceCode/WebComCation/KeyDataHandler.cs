﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Data.SQLite;
using Newtonsoft.Json.Bson;
using System.Diagnostics;

namespace WebComCation
{
    /// <summary>
    /// Class created by Sumit for Report Data Handling when it is related with specific license scenarios.
    /// </summary>
    public static class KeyDataHandler
    {

        /// <summary>
        /// Created by Sumit for #192
        /// </summary>
        /// <returns></returns>
        public static KeyLinkedDataMigrationStatus KeyDataCopier(string newLicenseKey, string computerID)
        {
            KeyLinkedDataMigrationStatus status = KeyLinkedDataMigrationStatus.Unknown;
            //steps.
            //Get the count and max uniqueId for the new activated 'newLicenseKey'
            string APIUrl = @"http://52.197.210.82/apioauthdata/index.php/home/getrecordcount";


            string JSONTemplate = @"";

            //-------------------------------------------------------------------------------------------------------------
            try
            {
                Cursor.Current = Cursors.WaitCursor;


                string jsonQuery = "[{\"Computer_id\":\"" + keyRequest.GetComputerID() +
                                    "\",\"Computer_name\":\"" + keyRequest.GetComputerName() +
                                    "\",\"Activation_key\":\"" + newLicenseKey.Trim() + "\",\"Language\":\"" + LicenseValidator.sCurrent_Language.ToLower().Replace("japanese", "japan") + "\"}]"; //Added by Rajnish for GSP - 880 and GSP - 881

                //Updated by Sumit GTL#130 (NR Task)  ---END
                String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
                var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);
                //3.
                JsonSerializer jsonSerializer = new JsonSerializer();
                //4.
                MemoryStream objBsonMemoryStream = new MemoryStream();
                //5.
                Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);
                //6.
                jsonSerializer.Serialize(bsonWriterObject, studentObject);
                //text = queryString;
                byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);



                #region WebRequest

                //Connect to our Yugamiru Web Server
                //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/activatekeydata");
                WebRequest webRequest = WebRequest.Create(APIUrl);


                webRequest.Method = "POST";
                webRequest.ContentType = "application/json";
                webRequest.ContentLength = requestByte.Length;
                Stream webDataStream = null;
                try
                {
                    webDataStream = webRequest.GetRequestStream();
                    webDataStream.Write(requestByte, 0, requestByte.Length);
                }
                catch (Exception ex)
                {
                    using (var client = new WebClient())
                    {
                        try
                        {
                            using (client.OpenRead("http://clients3.google.com/generate_204"))
                            {
                                //System.Windows.Forms.MessageBox.Show("You are not connected to internet");
                            }
                        }
                        catch (Exception ex1)
                        {
                            System.Windows.Forms.MessageBox.Show(Properties.Resources.PC_NOT_CONNECTED, "Yugamiru");                            
                            return KeyLinkedDataMigrationStatus.ServerError;
                        }
                    }
                }
                string ed = webDataStream.ToString();

                // get the response from our stream

                WebResponse webResponse = webRequest.GetResponse();
                webDataStream = webResponse.GetResponseStream();

                // convert the result into a String
                StreamReader webResponseSReader = new StreamReader(webDataStream);

                //Try Catch added by sumit for GSP-1078-----------START
                //String responseFromServer = webResponseSReader.ReadToEnd();
                String responseFromServer = "";

                try
                {
                    responseFromServer = webResponseSReader.ReadToEnd();

                    //format
                    {
                        /*
                        Response:
Success
{"Status":"200","Activation_key":"'.$value['Activation_key'].'","Recordcount":"'.$countStart.$counterval['rowcount'].$countEnd.'","Maxuniqueid":"'.$MaxUniqueIdStart.$counterval['UniqueIdvalmax'].$MaxUniqueIdEnd.'","Message":"'.$msg.'"}

in success there will be two messages one is Data Does not exist for this key and other is Recordcount and MaxUniqueId values  based on data
Failure:
{"Status":"401","Activationkey":"","Recordcount":"","Maxuniqueid":"","Message":"Key doesnot Exist"}

                         */
                    }

                    if(responseFromServer.ToUpper().Contains("Key doesnot Exist"))
                    {
                        return KeyLinkedDataMigrationStatus.NoDataFreshKey;
                    }
                    else if(responseFromServer.ToUpper().Contains("200") )
                    {
                        try
                        {
                            string recordCount = string.Empty;
                            string resData = responseFromServer.ToUpper();
                            //-----------Extract Count start
                            string[] spliterCount = { "COUNTSTART", "COUNTEND" };
                            List<string> lstCount = new List<string>();
                            lstCount.AddRange(resData.Split(spliterCount, StringSplitOptions.None));
                            if(lstCount.Count==3)
                            {
                                recordCount = lstCount[1];
                            }
                            //-----------Extract Count End


                            //-----------Extract UniqueID start
                            string maxUniqueID = string.Empty;
                            string[] spliterUniqueID = { "MaxUniqueIdStart".ToUpper(), "MaxUniqueIdStart".ToUpper() };
                            List<string> lstUniqueID = new List<string>();
                            lstUniqueID.AddRange(resData.Split(spliterUniqueID, StringSplitOptions.None));
                            if (lstUniqueID.Count == 3)
                            {
                                maxUniqueID = lstUniqueID[1];
                            }
                            //-----------Extract UniqueID End


                        }
                        catch
                        {
                            return KeyLinkedDataMigrationStatus.ParsingError;
                        }
                    }
                    else
                    {
                        return KeyLinkedDataMigrationStatus.InvalidResponse;
                    }



                }
                catch
                {

                }
            }
            catch (Exception exOut)
            {

            }
            finally
            {
                Cursor.Current = Cursors.WaitCursor;
            }
#endregion
            //-------------------------------------------------------------------------------------------------------------






            //Start Upload.
            //1. fetch one record form JSON 
            //2. replace UniqueId with max++ of above
            //3. Send this to server, do it for all tables
            //goto 1.
            //




            //Clear all the old reports from DB.




            //Call Restore data module.


            bool success = false;



            return status;
        }
    }
}
