﻿namespace LockUnLockSQLiteDB
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.gbActiveApplicationDB = new System.Windows.Forms.GroupBox();
            this.gbInstallerProjectDB = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAppDBPath = new System.Windows.Forms.TextBox();
            this.btnAppDBLock = new System.Windows.Forms.Button();
            this.btnAppDBUnlock = new System.Windows.Forms.Button();
            this.btnENDBUnLock = new System.Windows.Forms.Button();
            this.btnENDBLock = new System.Windows.Forms.Button();
            this.txtENInstallerProjDB = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnJPDBUnLock = new System.Windows.Forms.Button();
            this.btnJPDBLock = new System.Windows.Forms.Button();
            this.txtJPInstallerProjDB = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.gbActiveApplicationDB.SuspendLayout();
            this.gbInstallerProjectDB.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbActiveApplicationDB
            // 
            this.gbActiveApplicationDB.Controls.Add(this.btnAppDBUnlock);
            this.gbActiveApplicationDB.Controls.Add(this.btnAppDBLock);
            this.gbActiveApplicationDB.Controls.Add(this.txtAppDBPath);
            this.gbActiveApplicationDB.Controls.Add(this.label1);
            this.gbActiveApplicationDB.Location = new System.Drawing.Point(12, 12);
            this.gbActiveApplicationDB.Name = "gbActiveApplicationDB";
            this.gbActiveApplicationDB.Size = new System.Drawing.Size(790, 141);
            this.gbActiveApplicationDB.TabIndex = 0;
            this.gbActiveApplicationDB.TabStop = false;
            this.gbActiveApplicationDB.Text = "Active Application DB";
            // 
            // gbInstallerProjectDB
            // 
            this.gbInstallerProjectDB.Controls.Add(this.btnJPDBUnLock);
            this.gbInstallerProjectDB.Controls.Add(this.btnJPDBLock);
            this.gbInstallerProjectDB.Controls.Add(this.txtJPInstallerProjDB);
            this.gbInstallerProjectDB.Controls.Add(this.label3);
            this.gbInstallerProjectDB.Controls.Add(this.btnENDBUnLock);
            this.gbInstallerProjectDB.Controls.Add(this.btnENDBLock);
            this.gbInstallerProjectDB.Controls.Add(this.txtENInstallerProjDB);
            this.gbInstallerProjectDB.Controls.Add(this.label2);
            this.gbInstallerProjectDB.Location = new System.Drawing.Point(12, 159);
            this.gbInstallerProjectDB.Name = "gbInstallerProjectDB";
            this.gbInstallerProjectDB.Size = new System.Drawing.Size(790, 270);
            this.gbInstallerProjectDB.TabIndex = 1;
            this.gbInstallerProjectDB.TabStop = false;
            this.gbInstallerProjectDB.Text = "Installer Project DB";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 17);
            this.label1.TabIndex = 0;
            this.label1.Text = "DB Full Path";
            // 
            // txtAppDBPath
            // 
            this.txtAppDBPath.Location = new System.Drawing.Point(98, 35);
            this.txtAppDBPath.Name = "txtAppDBPath";
            this.txtAppDBPath.Size = new System.Drawing.Size(686, 22);
            this.txtAppDBPath.TabIndex = 2;
            this.txtAppDBPath.Text = "C:\\ProgramData\\gsport\\Yugamiru cloud\\database\\Yugamiru.sqlite";
            // 
            // btnAppDBLock
            // 
            this.btnAppDBLock.Location = new System.Drawing.Point(590, 75);
            this.btnAppDBLock.Name = "btnAppDBLock";
            this.btnAppDBLock.Size = new System.Drawing.Size(92, 32);
            this.btnAppDBLock.TabIndex = 3;
            this.btnAppDBLock.Text = "Lock";
            this.btnAppDBLock.UseVisualStyleBackColor = true;
            this.btnAppDBLock.Click += new System.EventHandler(this.btnAppDBLock_Click);
            // 
            // btnAppDBUnlock
            // 
            this.btnAppDBUnlock.Location = new System.Drawing.Point(692, 75);
            this.btnAppDBUnlock.Name = "btnAppDBUnlock";
            this.btnAppDBUnlock.Size = new System.Drawing.Size(92, 32);
            this.btnAppDBUnlock.TabIndex = 4;
            this.btnAppDBUnlock.Text = "UnLock";
            this.btnAppDBUnlock.UseVisualStyleBackColor = true;
            this.btnAppDBUnlock.Click += new System.EventHandler(this.btnAppDBUnlock_Click);
            // 
            // btnENDBUnLock
            // 
            this.btnENDBUnLock.Location = new System.Drawing.Point(692, 81);
            this.btnENDBUnLock.Name = "btnENDBUnLock";
            this.btnENDBUnLock.Size = new System.Drawing.Size(92, 32);
            this.btnENDBUnLock.TabIndex = 8;
            this.btnENDBUnLock.Text = "UnLock";
            this.btnENDBUnLock.UseVisualStyleBackColor = true;
            this.btnENDBUnLock.Click += new System.EventHandler(this.btnENDBUnLock_Click);
            // 
            // btnENDBLock
            // 
            this.btnENDBLock.Location = new System.Drawing.Point(590, 81);
            this.btnENDBLock.Name = "btnENDBLock";
            this.btnENDBLock.Size = new System.Drawing.Size(92, 32);
            this.btnENDBLock.TabIndex = 7;
            this.btnENDBLock.Text = "Lock";
            this.btnENDBLock.UseVisualStyleBackColor = true;
            this.btnENDBLock.Click += new System.EventHandler(this.btnENDBLock_Click);
            // 
            // txtENInstallerProjDB
            // 
            this.txtENInstallerProjDB.Location = new System.Drawing.Point(98, 41);
            this.txtENInstallerProjDB.Name = "txtENInstallerProjDB";
            this.txtENInstallerProjDB.Size = new System.Drawing.Size(686, 22);
            this.txtENInstallerProjDB.TabIndex = 6;
            this.txtENInstallerProjDB.Text = "C:\\Sumit\\GSPORT\\Daily_Setups\\V21_P3_EN\\Release\\database\\Yugamiru.sqlite";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 17);
            this.label2.TabIndex = 5;
            this.label2.Text = "EN DB Path";
            // 
            // btnJPDBUnLock
            // 
            this.btnJPDBUnLock.Location = new System.Drawing.Point(692, 190);
            this.btnJPDBUnLock.Name = "btnJPDBUnLock";
            this.btnJPDBUnLock.Size = new System.Drawing.Size(92, 32);
            this.btnJPDBUnLock.TabIndex = 12;
            this.btnJPDBUnLock.Text = "UnLock";
            this.btnJPDBUnLock.UseVisualStyleBackColor = true;
            this.btnJPDBUnLock.Click += new System.EventHandler(this.btnJPDBUnLock_Click);
            // 
            // btnJPDBLock
            // 
            this.btnJPDBLock.Location = new System.Drawing.Point(590, 190);
            this.btnJPDBLock.Name = "btnJPDBLock";
            this.btnJPDBLock.Size = new System.Drawing.Size(92, 32);
            this.btnJPDBLock.TabIndex = 11;
            this.btnJPDBLock.Text = "Lock";
            this.btnJPDBLock.UseVisualStyleBackColor = true;
            this.btnJPDBLock.Click += new System.EventHandler(this.btnJPDBLock_Click);
            // 
            // txtJPInstallerProjDB
            // 
            this.txtJPInstallerProjDB.Location = new System.Drawing.Point(98, 150);
            this.txtJPInstallerProjDB.Name = "txtJPInstallerProjDB";
            this.txtJPInstallerProjDB.Size = new System.Drawing.Size(686, 22);
            this.txtJPInstallerProjDB.TabIndex = 10;
            this.txtJPInstallerProjDB.Text = "C:\\Sumit\\GSPORT\\Daily_Setups\\V21_P3_JP\\Release\\database\\Yugamiru.sqlite";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 17);
            this.label3.TabIndex = 9;
            this.label3.Text = "JP DB Path";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(814, 444);
            this.Controls.Add(this.gbInstallerProjectDB);
            this.Controls.Add(this.gbActiveApplicationDB);
            this.Name = "Form1";
            this.Text = "Do Lock UnLock SQLIte";
            this.gbActiveApplicationDB.ResumeLayout(false);
            this.gbActiveApplicationDB.PerformLayout();
            this.gbInstallerProjectDB.ResumeLayout(false);
            this.gbInstallerProjectDB.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbActiveApplicationDB;
        private System.Windows.Forms.GroupBox gbInstallerProjectDB;
        private System.Windows.Forms.Button btnAppDBUnlock;
        private System.Windows.Forms.Button btnAppDBLock;
        private System.Windows.Forms.TextBox txtAppDBPath;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnJPDBUnLock;
        private System.Windows.Forms.Button btnJPDBLock;
        private System.Windows.Forms.TextBox txtJPInstallerProjDB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnENDBUnLock;
        private System.Windows.Forms.Button btnENDBLock;
        private System.Windows.Forms.TextBox txtENInstallerProjDB;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
    }
}

