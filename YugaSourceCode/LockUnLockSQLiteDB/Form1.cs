﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;

namespace LockUnLockSQLiteDB
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public bool LockDB(string dbpath)
        {
            bool dbLocked = false;

            SQLiteConnection conn = new SQLiteConnection(@"Data Source= " + dbpath.Replace(";","") + ";");
            var con = new SQLiteConnection(conn);
            try
            {
                con.Open();
                con.ChangePassword(ConvertToEncrypt("6G5s4Tdf45sT"));
                con.Close();
                dbLocked = true;
                MessageBox.Show("locked");
            }
            catch(Exception ex)
            {
                dbLocked = false;
                MessageBox.Show(ex.Message);
                throw ex;
            }
            return dbLocked;
        }

        public bool UnLockDB(string dbpath)
        {
            bool unlocked = false;
            SQLiteConnection conn = new SQLiteConnection(@"Data Source= " + dbpath + ";Version=3;New=True;password=" + ConvertToEncrypt("6G5s4Tdf45sT"));
            var con = new SQLiteConnection(conn);
            try
            {
                con.Open();
                con.ChangePassword("");
                con.Close();
                unlocked = true;
                MessageBox.Show("unlocked");                
            }
            catch(Exception ex)
            {
                unlocked = false;
                MessageBox.Show(ex.Message);
                throw ex;
            }
            return unlocked;
        }


        private void btnAppDBLock_Click(object sender, EventArgs e)
        {
            LockDB(txtAppDBPath.Text.Trim());
        }

        public static string ConvertToEncrypt(string valueToEncrypt)
        {
            byte[] bt = UTF8Encoding.UTF8.GetBytes(valueToEncrypt);
            string s = Convert.ToBase64String(bt);
            return s;
        }
        public static string ConvertToStringFromEncrypt(string EncryptedValue)
        {
            byte[] pt = Convert.FromBase64String(EncryptedValue);
            string op = UTF8Encoding.UTF8.GetString(pt);
            return op;
        }

        private void btnAppDBUnlock_Click(object sender, EventArgs e)
        {
            UnLockDB(txtAppDBPath.Text.Trim());
        }

        private void btnENDBLock_Click(object sender, EventArgs e)
        {
            LockDB(txtENInstallerProjDB.Text);
        }

        private void btnENDBUnLock_Click(object sender, EventArgs e)
        {
            UnLockDB(txtENInstallerProjDB.Text);
        }

        private void btnJPDBLock_Click(object sender, EventArgs e)
        {
            LockDB(txtJPInstallerProjDB.Text);
        }

        private void btnJPDBUnLock_Click(object sender, EventArgs e)
        {
            UnLockDB(txtJPInstallerProjDB.Text);
        }
    }
}
