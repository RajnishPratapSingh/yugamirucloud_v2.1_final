﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Globalization;
using System.IO;
using Shell32;
using System.Runtime.InteropServices;
using System.Text;

namespace LicenseInformation
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            string languageConfig_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
             "\\gsport\\Yugamiru cloud\\LanguageConfig.txt";
            string languageSettingContent = System.IO.File.ReadAllText(languageConfig_file);
            if (languageSettingContent.Contains(@"""" + "en" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo(@"en");
            }
            else if (languageSettingContent.Contains(@"""" + "ja-JP" + @""""))
            {
                CultureInfo.DefaultThreadCurrentUICulture = new CultureInfo("ja-JP");
            }

            Application.Run(new Form1());

            

            //Application.Run(new frmGetFamilyInstallInfo());
            //Application.Run(new frmCloudSycnSettings());
        }


    }
}
