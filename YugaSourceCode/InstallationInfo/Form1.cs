﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Data.SQLite;
using System.Windows.Forms;

namespace LicenseInformation
{
    public partial class Form1 : Form
    {
        bool closingFromOk;
        System.Text.RegularExpressions.Regex r;
        public Form1()
        {


            r = new System.Text.RegularExpressions.Regex(@"[~`!@#$%^&*()+=|\{}':;.,<>/?[\]""_]");
            closingFromOk = false;
            InitializeComponent();
            ToolTip tooltp = new ToolTip();
            tooltp.IsBalloon = true;
            tooltp.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            //tooltp.SetToolTip(LicenseKeyLblQue, "Enter the Activation Key which you would have received"+
            //    Environment.NewLine+"in your registered email after purchasing the yugamiru license."); //Commented by rohini for GTL#294
            tooltp.SetToolTip(LicenseKeyLblQue, Properties.Resources.WARNING4); //Added by rohini for GTL#294

            //Added by Rajnish for GSP-1462----------START
            if (Properties.Resources.CURRENT_LANGUAGE.Contains("Japanese"))
            {
                this.ImeMode = ImeMode.On;
                this.PCNameTxtBox.ImeMode = ImeMode.On;
                this.InstallNameTxtBox.ImeMode = ImeMode.On;
                this.InstallByTxtBox.ImeMode = ImeMode.On;
                this.CommentTxtBox.ImeMode = ImeMode.On;
            }
            else
            {
                this.ImeMode = ImeMode.NoControl;
            }
            //Added by Rajnish for GSP-1462----------END

            //Added by rohini for GTL#294 --- START
            label1.Text = Properties.Resources.WARNING;
            LicenseLbl.Text = Properties.Resources.LICENSE;
            PCNamelbl.Text = Properties.Resources.PC_NAME;
            Installlabl.Text = Properties.Resources.INSTALLATION_NAME;
            InstallDtLbl.Text = Properties.Resources.INSTALLATION_DATE;
            InstallByLbl.Text = Properties.Resources.INSTALLED_BY;
            CommentLbl.Text = Properties.Resources.COMMENT;
            btnCancel.Text = Properties.Resources.CANCEL;
            //Added by rohini for GTL#294 ---END
        }

        private void LicenseLbl_Click(object sender, EventArgs e)
        {

        }

        private void OkBtn_Click(object sender, EventArgs e)
        {
            //Added by Sumit for GTL#412   ---START
            if (!WebComCation.Utility.IsInternetConnected())
            {
                //  MessageBox.Show("No internet connection. Please connect to internet and retry.");
                MessageBox.Show(Properties.Resources.NO_NET_CONNECT_RETRY);
                return;
            }
            //Added by Sumit for GTL#412   ---END





            if (!String.IsNullOrEmpty(licenseTxtbox.Text))
            {

                if ((r.IsMatch(licenseTxtbox.Text)) || (r.IsMatch(InstallNameTxtBox.Text)) || (r.IsMatch(InstallByTxtBox.Text))
                    || (r.IsMatch(CommentTxtBox.Text)) || (r.IsMatch(InstallDateTxtBox.Text)))
                {
                    // MessageBox.Show("Special characters are not allowed");   //commented by rohini for GTL#294
                    MessageBox.Show(Properties.Resources.SPECIAL_CHARECTORS_NOT_ALLOWED); // added by rohini for GTL#294
                    //licenseTxtbox.Text = "";
                    //InstallNameTxtBox.Text = "";
                    //InstallByTxtBox.Text = "";
                    //CommentTxtBox.Text = "";
                    //DateTime currentDt = DateTime.Now;
                    //InstallDateTxtBox.Text = currentDt.ToString("dd-MM-yyyy");
                    return;
                }
                else
                {

                    string licenseText = licenseTxtbox.Text.Trim();
                    string installNameText = InstallNameTxtBox.Text.Trim();
                    string ComputerID = WebComCation.keyRequest.GetComputerID();
                    string InstallBy = InstallByTxtBox.Text.Trim();
                    string comment = CommentTxtBox.Text.Trim();
                    string InstallDateText = InstallDateTxtBox.Text.Trim();
                    string PC_Name = PCNameTxtBox.Text.Trim();

                    bool success = sendRecord(PC_Name, installNameText, ComputerID, InstallDateText, InstallBy, licenseText, comment);

                    if (!success)
                    {
                        //  MessageBox.Show("License key is invalid"); //commented by rohini for GTL#294
                        MessageBox.Show(Properties.Resources.INAVLID_KEY); // added by rohini for GTL#294
                        licenseTxtbox.Focus();
                        licenseTxtbox.SelectAll();
                        return;
                    }

                    closingFromOk = true;
                    this.Close();
                }
            }
            else
            {
                //  MessageBox.Show("Please provide License Key");//commented by rohini for GTL#294
                MessageBox.Show(Properties.Resources.PLEASE_PROVIDE_LICENSE_KEY); // added by rohini for GTL#294
            }
        }
        public bool sendRecord(string computer_name, string installation_name, string comp_id, string Install_date, string Install_by, string activation_key = null, string comm = null)
        {
            bool retVal = false;
            string language = "";
            string installId = "";
            string reg_user_id = "";
            string jsonQuery = "[{\"Computer_id\":\"" + comp_id +
                               "\",\"Computer_name\":\"" + computer_name +
                               "\",\"Activation_key\":\"" + activation_key +
                               "\",\"Installation_name\":\"" + installation_name +
                               "\",\"Date_of_install\":\"" + InstallDateTxtBox.Text +
                               "\",\"Install_id\":\"" + installId +
                               "\",\"reg_user_id\":\"" + reg_user_id +
                                "\",\"Installed_by\":\"" + Install_by +
                                 "\",\"Comment\":\"" + comm +
                                "\",\"Language\":\"" + language + "\"}]";
            String queryString = jsonQuery;//jsonRawTemplate;//jsonQuery;
            var studentObject = Newtonsoft.Json.JsonConvert.DeserializeObject(queryString);

            JsonSerializer jsonSerializer = new JsonSerializer();

            MemoryStream objBsonMemoryStream = new MemoryStream();

            Newtonsoft.Json.Bson.BsonWriter bsonWriterObject = new Newtonsoft.Json.Bson.BsonWriter(objBsonMemoryStream);

            jsonSerializer.Serialize(bsonWriterObject, studentObject);
            //  text = queryString;
            byte[] requestByte = objBsonMemoryStream.ToArray();//= Encoding.Default.GetBytes(queryString);
                                                               // WebRequest

            //Connect to our Yugamiru Web Server
            //WebRequest webRequest = WebRequest.Create("http://gs-demo.jma.website/apioauthdata/index.php/home/validatelicense");
            WebRequest webRequest = WebRequest.Create("http://52.197.210.82/apioauthdata/index.php/home/installationdetails");
            webRequest.Method = "POST";
            webRequest.ContentType = "application/json";
            webRequest.ContentLength = requestByte.Length;
            Stream webDataStream = null;
            try
            {
                webDataStream = webRequest.GetRequestStream();
                webDataStream.Write(requestByte, 0, requestByte.Length);
            }
            catch (Exception ex)
            {

            }
            string ed = webDataStream.ToString();

            // get the response from our stream

            WebResponse webResponse = webRequest.GetResponse();
            webDataStream = webResponse.GetResponseStream();

            // convert the result into a String
            StreamReader webResponseSReader = new StreamReader(webDataStream);
            String responseFromServer = webResponseSReader.ReadToEnd();

            #region Parsing_Response_Data
            string resData = responseFromServer;
            /*Sample:
             * "W\0\0\0\u0010Status\0�\u0001\0\0\u0002Computerid\0\u0001\0\0\0\0\u0002
             * Install_id\0\u0001\0\0\0\0\u0002Message\0\u0017\0\0\0Activation Key Invalid\0\0"             
             */
            if (resData.Contains("Invalid"))
            {
                retVal = false;
            }
            else if (resData.Contains("Successfully"))
            {
                retVal = true;
                //Writedown this self installation 
                //information data in local txt file and update the same after application activation
                try
                {

                    string installId_RegId = ParseJsonInstallInfo(resData);
                    if (installId_RegId.Length > 1)
                    {
                        installId = Regex.Replace(installId_RegId.Split('_')[0], @"[^\w\.@+/=-]", "");
                        //installId_RegId.Split('_')[0];
                        reg_user_id = Regex.Replace(installId_RegId.Split('_')[1], @"[^\w\.@+/=-]", "");
                        //installId_RegId.Split('_')[1];
                        jsonQuery = "[{\"Installation_name\":\"" + installation_name +
                               "\",\"Date_of_install\":\"" + InstallDateTxtBox.Text +
                               "\",\"Install_id\":\"" + installId +
                               "\",\"reg_user_id\":\"" + reg_user_id +
                                "\",\"Installed_by\":\"" + Install_by +
                                 "\",\"Comment\":\"" + comm +
                                "\",\"Language\":\"" + language + "\"}]";

                        string temp_Ins_file = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData) +
                                                                "\\gsport\\Yugamiru cloud\\database\\TempInstalInfoJson.txt";

                        File.WriteAllText(temp_Ins_file, jsonQuery);
                    }

                }
                catch (Exception ex)
                { }
            }
            #endregion
            return retVal;
        }

        string ParseJsonInstallInfo(string bsonData)
        {
            string[] knownVals = { "Status", "Computer_id", "Install_id", "reg_user_id", "Message" };
            string[] parts1 = bsonData.Split(knownVals, StringSplitOptions.None);
            List<string> lstVals = new List<string>(parts1);

            for (int i = 1; i < lstVals.Count; i++)
            {
                lstVals[i] = lstVals[i].Replace("\09\0\0\0", "").Replace("\0", "")
                    .Replace("\u0002", "").Replace("\u0004", "");
                string q = lstVals[i].Replace("\09\0\0\0", "").Replace("\0", "")
                    .Replace("\u0002", "").Replace("\u0004", "");
            }
            if (lstVals.Count == 6)
            {
                //string retStr = "Install_id=" + lstVals[3]+", reg_user_id=" + lstVals[4];
                string retStr = lstVals[3] + "_" + lstVals[4];
                return retStr;
            }

            return "";
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            if (Properties.Resources.CURRENT_LANGUAGE.Contains("Japanese"))
            {
                this.ImeMode = ImeMode.On;//Added by Rajnish for GSP-1462
                this.PCNameTxtBox.ImeMode = ImeMode.On;
                this.InstallNameTxtBox.ImeMode = ImeMode.On;
                this.InstallByTxtBox.ImeMode = ImeMode.On;
                this.CommentTxtBox.ImeMode = ImeMode.On;
            }
            else
            {
                this.ImeMode = ImeMode.NoControl;//Added by Rajnish for GSP-1462
            }

            PCNameTxtBox.Text = Environment.MachineName;
            DateTime currentDt = DateTime.Now;
            InstallDateTxtBox.Text = currentDt.ToString("dd-MM-yyyy");
        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            DateTime date = this.dateTimePicker1.Value;

            this.InstallDateTxtBox.Text = date.ToString("dd-MM-yyyy");
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            //Commented by rohini for GTL#294 --START
            //if(
            //    MessageBox.Show(
            //        "Cancelling this step will cancel the installation" +
            //        " of the Yugamiru Cloud Application."
            //        +Environment.NewLine +"Do you want to close and cancel the installation?","gsport",MessageBoxButtons.YesNo)==DialogResult.Yes)
            //{
            //    closingFromOk = true;
            //    Environment.Exit(1);
            //}
            //Commented by rohini for GTL#294 --END

            //Added by rohini for GTL#294 --START
            DialogResult result = Frmmessagebox.Show(Properties.Resources.WARNING1, Frmmessagebox.enumMessageIcon.Question, Frmmessagebox.enumMessageButton.YesNo);
            if (result == DialogResult.Yes && e != null)
            {
                closingFromOk = true;
                Environment.Exit(1);
            }

            // Added by rohini for GTL#294 --START
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!closingFromOk)
            {
                btnCancel.PerformClick();
                e.Cancel = true;
                closingFromOk = false;
            }
        }

        private void licenseTxtbox_KeyDown(object sender, KeyEventArgs e)
        {
            //Keys keyData = e.KeyData;
            //string s = keyData.ToString();          
            //Added by Sumit for GTL#468   ---START
            //--Commented by Rajnish as no need GSP-1457--start---//
            //if (((Control)(sender)).Name == licenseTxtbox.Name)
            //{
            //}
            //else
            //{
            //if (e.Modifiers == Keys.Control && e.KeyCode == Keys.V)
            //{
            //    // cancel the "paste" function
            //    e.SuppressKeyPress = true;
            //}
            //}
            //--Commented by Rajnish as no need GSP-1457 --END---//
            //Added by Sumit for GTL#468   ---END
        }

        private void licenseTxtbox_TextChanged(object sender, EventArgs e)
        {
            //--Added By Rajnish for GTL #1462---Start--// 
            var textboxSender = (TextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");//commented by Rajnish for GSP-1413
            //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #1462---END--//

            //int cursorPosition = licenseTxtbox.SelectionStart; //added by rohini for GTL#414
            //if (r.IsMatch(((TextBox)(sender)).Text))
            //{
            //    //MessageBox.Show("Special characters are not allowed"); //commented by rohini for GTL#294
            //    //  MessageBox.Show(Properties.Resources.SPECIAL_CHARECTORS_NOT_ALLOWED);  //added by rohini for GTL#294  //commented by rohini for GTL#414
            //    // ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).Text.Length - 1].ToString(), ""); //commented by rohini for GTL#414
            //    //((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).SelectionStart - 1].ToString(), "");
            //    ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).SelectionStart - 1].ToString(), "");
            //    licenseTxtbox.SelectionStart = cursorPosition - 1; //added by rohini for GTL#414
            //}
        }

        //added by rohini for GTL#414 -- START
        private void InstallNameTxtBox_TextChanged(object sender, EventArgs e)
        {
            //int cursorPosition = InstallNameTxtBox.SelectionStart;
            //if (r.IsMatch(((TextBox)(sender)).Text))
            //{

            //    ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).SelectionStart - 1].ToString(), "");
            //    InstallNameTxtBox.SelectionStart = cursorPosition - 1;
            //}

            //--Added By Rajnish for GTL #1462---Start--// 
            var textboxSender = (TextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");//commented by Rajnish for GSP-1413
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #1462---END--//
        }

        private void InstallByTxtBox_TextChanged(object sender, EventArgs e)
        {
            //int cursorPosition = InstallByTxtBox.SelectionStart;
            //if (r.IsMatch(((TextBox)(sender)).Text))
            //{

            //    ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).SelectionStart - 1].ToString(), "");
            //    InstallByTxtBox.SelectionStart = cursorPosition - 1;
            //}

            //--Added By Rajnish for GTL #1462---Start--// 
            var textboxSender = (TextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");//commented by Rajnish for GSP-1413
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #1462---END--//
        }

        private void CommentTxtBox_TextChanged(object sender, EventArgs e)
        {
            //int cursorPosition = CommentTxtBox.SelectionStart;
            //if (r.IsMatch(((TextBox)(sender)).Text))
            //{
            //    ((TextBox)(sender)).Text = ((TextBox)(sender)).Text.Replace(((TextBox)(sender)).Text[((TextBox)(sender)).SelectionStart - 1].ToString(), "");
            //    CommentTxtBox.SelectionStart = cursorPosition - 1;
            //}

            //--Added By Rajnish for GTL #1462---Start--// 
            var textboxSender = (TextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");//commented by Rajnish for GSP-1413
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #1462---END--//
        }
        //added by rohini for GTL#414 -- END
    }
}
