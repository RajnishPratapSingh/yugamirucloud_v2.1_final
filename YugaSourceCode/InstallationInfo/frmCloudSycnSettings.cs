﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Net;
using System.IO;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.Data.SQLite;

namespace LicenseInformation
{
    public partial class frmCloudSycnSettings : Form
    {
        SQLiteConnection sqlite;
        int selectedPCs = 0;
        int nonSelectedPCs = 0;
        int totalPCs = 0;
        public frmCloudSycnSettings()
        {
            InitializeComponent();

            //Added by rohini for GTL#294 --- START
            btnRefresh.Text = Properties.Resources.REFRESH;
            btnSaveSync.Text = Properties.Resources.SAVE_SYNC_SETTINGS;
            btnClose.Text = Properties.Resources.BUTTON_CLOSE;
            lblTotalPCCount.Text = Properties.Resources.TOTAL_PC_IN_NETWORK;
            lblTotalPCCount.Tag = Properties.Resources.TOTAL_PC_IN_NETWORK;
            lblPCsSyncedCount.Text = Properties.Resources.SELECTED;
            lblPCsSyncedCount.Tag = Properties.Resources.SELECTED;
            lblPCsAvlblToSync.Text = Properties.Resources.UNSELECTED;
            lblPCsAvlblToSync.Tag = Properties.Resources.UNSELECTED;
            
            //Added by rohini for GTL#294 --- END
        }

        private void frmCloudSycnSettings_Load(object sender, EventArgs e)
        {
            btnRefresh_Click(null, null);
            UpdateCounts();
            chkAll.CheckState = CheckState.Indeterminate;
            
        }
        public DataTable GetInstallInfoTable()
        {
            //Get Refresh data from Online
            if (!WebComCation.Utility.IsInternetConnected(5))
            {
                // MessageBox.Show("Working with old data" + Environment.NewLine + "No Internet.Cannot sync with latest data."); //commented by rohini for GTL#294
                MessageBox.Show(Properties.Resources.WARNING3); //Added by rohini for GTL#294
            }
            else
            {
                //try-catch added by Sumit for GTL#477   ---START
                //CloudManager.InstallInfoManager.SyncInstallInfo();
                try
                {
                    CloudManager.InstallInfoManager.SyncInstallInfo();
                }
                catch (Exception ex)
                {
                    // MessageBox.Show("Working with old data" + Environment.NewLine + "No Internet.Cannot sync with latest data."); 
                    MessageBox.Show(Properties.Resources.WARNING3);
#if debug
                    MessageBox.Show(ex.StackTrace);
#endif
                }
                //try-catch added by Sumit for GTL#477   ---END
            }
            //

            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            try
            {
                if (CloudManager.GlobalItems.NoOtherPCAvaialble)//Condition Added by rajnish for GSP-1486
                {
                    //--Added by rajnish for GSP - 1486----------Start
                    sqlite = CloudManager.Utility.SQLiteGetSecureCon("Data Source=" + WebComCation.Utility.db_file);
                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = " delete from tblInstallInfo where sync != 'THIS'";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "select * from tblInstallInfo where sync = 'THIS'";  //set the passed query
                    ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(dt); //fill the datasource
                    sqlite.Close();
                    //--Added by rajnish for GSP - 1486----------END
                }
                else
                {
                    //Edited by Sumit for GSP-1181   ---START
                    //sqlite = new SQLiteConnection("Data Source=" + WebComCation.Utility.db_file);
                    sqlite = CloudManager.Utility.SQLiteGetSecureCon("Data Source=" + WebComCation.Utility.db_file);
                    //Edited by Sumit for GSP-1181   ---END

                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = "select * from tblInstallInfo";  //set the passed query
                    ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(dt); //fill the datasource
                    sqlite.Close();
                }
            }
            catch (SQLiteException ex)
            {
                //Add your exception code here.
                MessageBox.Show(ex.Message);
            }
            //sqlite.Close();


            return dt;
        }

       
        private void btnRefresh_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                dt = GetInstallInfoTable();
                flpnlPCs.Controls.Clear();
                flpnlPCs.Tag = null;
                flpnlPCs.Tag = dt;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message + Environment.NewLine + ex.StackTrace);
            }
            finally
            {
                Cursor.Current = Cursors.Default;
            }
            

            

            foreach(DataRow dr in dt.Rows)
            {
                CustomControls.PC pC = new CustomControls.PC();
                //Extract Values
                string id= dr["id"].ToString();
                string stall_id = dr["stall_id"].ToString();
                string reg_user_id = dr["reg_user_id"].ToString();
                string computer_Name = dr["computer_Name"].ToString();
                string installation_name = dr["installation_name"].ToString();
                string date_of_install = dr["date_of_install"].ToString();
                string installed_by = dr["installed_by"].ToString();
                string comment = dr["comment"].ToString();
                string language = dr["language"].ToString();                
                string Sync = dr["sync"].ToString();

                //Set PC info

               
                pC.SetCommentTip(comment);
               
                pC.txtPCName.Text = computer_Name;
                pC.txtInsName.Text = installation_name;
                pC.txtInstallDate.Text = date_of_install;
              
                pC.strComments = comment;
                pC.strComputerName = computer_Name;
                pC.strDateOfInstall = date_of_install;
                pC.strInstallationName = installation_name;
                pC.strInstalledBy = installed_by;
                pC.strLanguage = language;
                pC.strRegUserId = reg_user_id;
                pC.strRemarks = comment;
                pC.strStallId = stall_id;
                pC.chkChangedEvent += new Action(UpdateCounts);


                if (Sync.ToUpper()=="TRUE")
                {
                    pC.chkSelected.Checked = true;
                }
                else if (Sync.ToUpper() == "FALSE")
                {
                    pC.chkSelected.Checked = false;
                }
                else if (Sync.ToUpper() == "THIS")
                {
                    pC.chkSelected.Checked = true;
                    pC.chkSelected.ThreeState = true;
                    pC.Enabled = false;                    
                }              

                this.flpnlPCs.Controls.Add(pC);
                this.flpnlPCs.Controls[flpnlPCs.Controls.Count - 1].Focus();
            }
        }

        private void btnSaveSync_Click(object sender, EventArgs e)
        {
            DataTable dt;
            if (flpnlPCs.Tag != null)
            {
                dt = ((DataTable)(flpnlPCs.Tag));
                foreach(Control ct in flpnlPCs.Controls)
                {
                    CustomControls.PC pc = ((CustomControls.PC)(ct));
                    if (!pc.Enabled)
                        continue;

                    for(int k=0;k<dt.Rows.Count;k++)
                    {
                        if(pc.strStallId==dt.Rows[k]["stall_id"].ToString())
                        {
                            dt.Rows[k]["sync"] = pc.chkSelected.Checked ? "TRUE" : "FALSE";
                        }
                    }
                }
                //Update database
                SQLiteCommand cmd;
                sqlite.Open();  //Initiate connection to the db
                cmd = sqlite.CreateCommand();
                string qry = "";
                foreach(DataRow dr in dt.Rows)
                {
                    qry += "update tblInstallInfo set sync='" + dr["sync"] + "' where stall_id='" + dr["stall_id"] + "';";
                }
                cmd.CommandText = qry;
                cmd.ExecuteNonQuery();
                sqlite.Close();
                // MessageBox.Show("Settings saved successfully."); //commented by rohini for GTL#294
                MessageBox.Show(Properties.Resources.SETTING_SAVED_SUCCESSFULLY); //Added by rohini for GTL#294

                this.Close();
            }
            else
                return;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void UpdateCounts()
        {
            totalPCs = 0;
            selectedPCs = 0;
            nonSelectedPCs = 0;
            foreach (Control c in flpnlPCs.Controls)
            {
                totalPCs++;
                if(((CustomControls.PC)(c)).chkSelected.Checked)
                {
                    selectedPCs += 1;
                }
                else
                {
                    nonSelectedPCs += 1;
                }
            }
            lblPCsAvlblToSync.Text = lblPCsAvlblToSync.Tag.ToString() + nonSelectedPCs.ToString();
            lblPCsSyncedCount.Text = lblPCsSyncedCount.Tag.ToString() + selectedPCs;
            lblTotalPCCount.Text = lblTotalPCCount.Tag.ToString() + (totalPCs).ToString();

            if (nonSelectedPCs == 1)
            {
                chkAll.CheckState = CheckState.Unchecked;
            }
            else if (selectedPCs==totalPCs)
            {
                chkAll.CheckState = CheckState.Checked;
            }
            else
            {
                chkAll.CheckState = CheckState.Indeterminate;
            }
        }

        private void chkAll_CheckedChanged(object sender, EventArgs e)
        {
           // if(chkAll.CheckState==CheckState.Checked)
           // {
           //     foreach (Control c in flpnlPCs.Controls)
           //     {
           //         ((CustomControls.PC)(c)).chkSelected.Checked = true;
           //     }
           // }
           //else if (chkAll.CheckState == CheckState.Unchecked)
           // {
           //     foreach (Control c in flpnlPCs.Controls)
           //     {
           //         if (!((CustomControls.PC)(c)).Enabled) continue;
           //         ((CustomControls.PC)(c)).chkSelected.Checked = false;
           //     }
           // }
        }
    }
}
