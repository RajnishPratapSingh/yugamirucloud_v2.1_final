﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CustomControls
{
    public partial class CheckedListSearchControl : UserControl
    {
        DataTable dt;
        CheckBox cb;
        List<string> lstNames;
        List<string> lstHints;
        List<CheckBox> lstCB = new List<CheckBox>();
        public bool SortOnName { get; set; }

        public CheckedListSearchControl()
        {
            InitializeComponent();
            lstHints = new List<string>();

            lstNames = new List<string>();
            SortOnName = true;
            this.Refresh();
            //added by rohini for GTL#294 -- START
            this.btnAll.Text = CustomControls.Properties.Resources.All;
            this.btnSelected.Text = CustomControls.Properties.Resources.Selected;
            this.btnNotSelected.Text = CustomControls.Properties.Resources.UnSelected;
            lblPCCount.Text = "";
            //added by rohini for GTL#294 -- END
        }
        /// <summary>
        /// Call this function for filling Test Data for testing the control. For Actual Data use [FillData(DataTable dt)] function.
        /// </summary>
        public void FillTestData()
        {
            dt = new DataTable();
            dt.Columns.Add("ID");
            dt.Columns.Add("Name");

            for (int i = 0, j = 1; i < 12; i++, j++)
            {
                if (j > 12)
                    j = 1;
                string d = DateTime.Now.AddDays(i).DayOfWeek.ToString();
                dt.Rows.Add(i, d);
                lstNames.Add(d);

                string m = DateTime.Now.AddMonths(j).ToString("MMMM");
                dt.Rows.Add(i + 60, m);
                lstNames.Add(m);

                pnl.AutoScroll = true;
            }
            if (SortOnName)
            {
                //var lstSrted=lstNames.
                var orderedList = lstNames.OrderBy(item => item).Select(t => t);//.Split('.')[0]);
                var a = orderedList.Select(x => x).Distinct();
                lstNames = a.ToList<string>();

            }
            SetPanelCheckBoxes();
        }

        /// <summary>
        /// Call this function to supply the content for the control to display. DataTable must contain at least two columns "ID" & "Name". 
        /// Names will be displayed on the control
        /// </summary>
        /// <param name="dtIDName">DataTable with minimum two columns ID & Name</param>
        public void FillData(DataTable dtIDName)
        {
            if (dtIDName == null || dtIDName.Rows.Count == 0)
            {
                return;
                //throw new Exception("Table does not contains any data");
            }
            if (dtIDName.Columns.Count < 2)// 
            {
                return;
                //throw new Exception("Table does not contains sufficient number of columns");
            }

            dt = dtIDName;

            //Expected column dt.Columns.Add("ID");
            //Expected column dt.Columns.Add("Name");
            lstNames.Clear();
            foreach (DataRow dr in dt.Rows)
            {
                lstNames.Add(dr["Name"].ToString());
            }
            if (SortOnName)
            {
                var orderedList = lstNames.OrderBy(item => item).Select(t => t);//.Split('.')[0]);
                var a = orderedList.Select(x => x).Distinct();
                lstNames = a.ToList<string>();
            }

            //Added by Sumit for GTL#229   ---START
            if(lstCB.Count< lstNames.Count)
            {
                lstCB.Clear();                
            }

            //Added by Sumit for GTL#229   ---END

            SetPanelCheckBoxes();
            textBox1.Text = "";
        }

        void SetPanelCheckBoxes()
        {
            pnl.Controls.Clear();
            if (lstCB.Count == 0)
            {
                //int k = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    cb = new CheckBox();
                    cb.Text = dr["Name"].ToString();
                    cb.Name = dr["ID"].ToString();
                    pnl.Controls.Add(cb);
                    cb.Dock = DockStyle.Top;
                    cb.BringToFront();
                    //if (k % 2 == 0)
                    //{
                    //    //cb.BackColor = Color.GhostWhite;//.LightBlue;
                    //    //cb.Tag = Color.GhostWhite.Name;//LightBlue.Name;
                    //}
                    //else
                    //{
                    //    //cb.BackColor = Color.WhiteSmoke;
                    //    //cb.Tag = Color.WhiteSmoke.Name;
                    //}
                    //k++;
                    cb.MouseHover += Cb_MouseHover;
                    cb.MouseLeave += Cb_MouseLeave;
                    cb.CheckedChanged += Cb_CheckedChanged;
                    lstCB.Add(cb);
                }
                pnl.Refresh();
                // btnAll.Text = "All(" + lstCB.Count + ")"; //commented by rohini for GTL#294
                btnAll.Text = Properties.Resources.All +"(" + lstCB.Count + ")"; //Added by rohini for GTL#294
                // btnNotSelected.Text = lstCB.Count + " Unselected"; //commented by rohini for GTL#294
                btnNotSelected.Text = lstCB.Count + Properties.Resources.UnSelected; //Added by rohini for GTL#294
                // btnSelected.Text = "0 Selected"; //commented by rohini for GTL#294
                btnSelected.Text = "0" + Properties.Resources.Selected; //Added by rohini for GTL#294
                btnNotSelected.Tag = "OFF";
                btnSelected.Tag = "OFF";
            }
            else
            {
                foreach (CheckBox cb in lstCB)
                {
                    pnl.Controls.Add(cb);
                    cb.Dock = DockStyle.Top;
                    cb.BringToFront();
                }
            } 
            if(lstCB.Count>0)
            {
                lblPCCount.Text = lstCB.Count + " PCs";
            }
            else
            {
                lblPCCount.Text = "";
            }
        }
        
        public event EventHandler InnerCb_CheckedChanged;//Added By Rajnish For GSP-1488
        private void Cb_CheckedChanged(object sender, EventArgs e)
        {
            if (InnerCb_CheckedChanged != null)
            {
                InnerCb_CheckedChanged(sender, e);
            }            
            // btnNotSelected.Text = GetNonSelectedCount() + " Unselected"; //commented by rohini for GTL#294
            btnNotSelected.Text = GetNonSelectedCount() + Properties.Resources.UnSelected; //Added by rohini for GTL#294
            // btnSelected.Text = GetSelectedCount() + "  Selected"; //commented by rohini for GTL#294
            btnSelected.Text = GetSelectedCount() + Properties.Resources.Selected; //Added by rohini for GTL#294
        }

       //Made public by Sumit GTL#229
        public void Form1_Load(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox1.Text = "";
            textBox1.Refresh();
            //FillTestData();
            if (dt != null)
                FillData(dt);
        }

        private void Cb_MouseLeave(object sender, EventArgs e)
        {
            
        }

        private void Cb_MouseHover(object sender, EventArgs e)
        {
        }

       

        /// <summary>
        /// Returns Table containing ID & Name columns with selected(checked) records
        /// </summary>
        /// <returns>DataTable with two columns ID & Name</returns>
        public DataTable GetSelectedData()
        {
            DataTable dtSelected = new DataTable();
            dtSelected.Columns.Add("ID");
            dtSelected.Columns.Add("Name");
            pnl.Refresh();
            foreach (Control ct in pnl.Controls)
            {
                //((CheckBox)(ct)).
                if (((CheckBox)(ct)).Checked)
                {
                    dtSelected.Rows.Add(new string[] { ct.Name, ct.Text });
                }
            }
            //if(dtSelected.Rows.Count==0)
            //{
            //    foreach(CheckBox cb in lstCB)
            //    {
            //        if(cb.Checked)
            //        {
            //            dtSelected.Rows.Add(new string[] { cb.Name, cb.Text });
            //        }
            //    }
            //}
            return dtSelected;
        }

        List<string> lstHasTemp1 = new List<string>();
        List<string> lstHasTemp2 = new List<string>();
        List<CheckBox> lstChkb = new List<CheckBox>();
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

            //if text box is empty
            if (textBox1.Text.Trim() == "")
            {                
                SetPanelCheckBoxes();
                return;
            }



            int longestLenght = lstNames.Max(r => r.Length);
            string mt = textBox1.Text;

            int intLoct1 = 0;
            int intLoct2 = 0;

            lstHasTemp1.Clear();
            lstHasTemp2.Clear();

            foreach (string st in lstNames)
            {
                if (st.ToUpper().Contains(mt.ToUpper()))
                {
                    lstHasTemp1.Add(st);
                }
            }
            if (lstHasTemp1.Count == 1)
            {
                if (lstHasTemp1.Count == 0)
                    return;
            }

            if (lstHasTemp1.Count == 1)
            {
                lstHasTemp2.Add(lstHasTemp1[0]);
                //return;
            }
            else
            {
                string strcr = "";


                for (int i = 0; i < lstHasTemp1.Count; i++)
                {


                    strcr = lstHasTemp1[i];
                    intLoct2 = strcr.IndexOf(textBox1.Text);
                    string strtmp1 = strcr;
                    foreach (string s in lstHasTemp1)
                    {
                        if (strcr == s)
                            continue;
                        intLoct1 = s.ToUpper().IndexOf(textBox1.Text.ToUpper());

                        if (intLoct1 > -1 && intLoct1 < intLoct2)
                        {
                            intLoct2 = intLoct1;
                            strtmp1 = s;
                        }
                    }
                    if (!lstHasTemp2.Contains(strtmp1))
                    {
                        lstHasTemp2.Add(strtmp1);
                        lstHasTemp1.Remove(strtmp1);
                        i = 0;
                    }

                }
                if (lstHasTemp1.Count == 1)
                {
                    lstHasTemp2.Add(lstHasTemp1[0]);
                }
            }
                       
            {
                foreach (string s in lstHasTemp2)
                {
                    foreach (CheckBox chk in lstCB)
                    {
                        if (chk.Text.Trim().ToUpper() == s.Trim().ToUpper())
                        {
                            lstChkb.Add(chk);
                        }
                    }
                }
            }
            pnl.Controls.Clear();
            pnl.Refresh();
            int k = 1;
            foreach (CheckBox cb in lstChkb)
            {
                pnl.Controls.Add(cb);
                cb.Dock = DockStyle.Top;
                cb.BringToFront();                
            }
            lstChkb.Clear();
        }

        private void btnSelected_Click(object sender, EventArgs e)
        {
            
            if (btnSelected.Tag.ToString() == "OFF")
            {
                btnSelected.Tag = "ON";
                btnNotSelected.Tag = "OFF";
                btnNotSelected.FlatStyle = FlatStyle.Standard;
                btnSelected.FlatStyle = FlatStyle.Flat;
                foreach (CheckBox c in lstCB)
                {
                    if (c.Checked)
                    {
                        c.Visible = true;
                    }
                    else
                    {
                        c.Visible = false;
                    }
                }
                pnl.Refresh();
            }
            else if (btnSelected.Tag.ToString() == "ON")
            {
                btnSelected.Tag = "OFF";
                btnSelected.FlatStyle = FlatStyle.Standard;
                foreach (CheckBox c in lstCB)
                {
                    c.Visible = true;
                }
                pnl.Refresh();
            }

        }

        private void btnAll_Click(object sender, EventArgs e)
        {
           
            if (GetSelectedCount() == lstCB.Count)
            {
                foreach (CheckBox c in pnl.Controls)
                {
                    if (c.Visible)
                    {
                        c.Checked = false;
                    }
                }
            }
            else
            {
                foreach (CheckBox c in pnl.Controls)
                {
                    if (c.Visible)
                    {
                        c.Checked = true;
                    }
                }
            }
        }
        private int GetSelectedCount()
        {
            int ret = 0;
            foreach (CheckBox cb in lstCB)
            {
                if (cb.Checked)
                {
                    ret++;
                }
            }
            return ret;
        }
        private int GetNonSelectedCount()
        {
            int ret = 0;
            foreach (CheckBox cb in lstCB)
            {
                if (!cb.Checked)
                {
                    ret++;
                }
            }
            return ret;
        }

        private void btnNotSelected_Click(object sender, EventArgs e)
        {
            if (btnNotSelected.Tag.ToString() == "OFF")
            {
                btnNotSelected.Tag = "ON";
                btnSelected.Tag = "OFF";
                btnSelected.FlatStyle = FlatStyle.Standard;
                btnNotSelected.FlatStyle = FlatStyle.Flat;
                foreach (CheckBox c in lstCB)
                {
                    if (c.Checked)
                    {
                        c.Visible = false;
                    }
                    else
                    {
                        c.Visible = true;
                    }
                }
                pnl.Refresh();
            }
            else if (btnNotSelected.Tag.ToString() == "ON")
            {
                btnNotSelected.Tag = "OFF";
                btnNotSelected.FlatStyle = FlatStyle.Standard;
                foreach (CheckBox c in lstCB)
                {
                    c.Visible = true;
                }
                pnl.Refresh();
            }
        }
    }
}
