﻿namespace CustomControls
{
    partial class CheckedListSearchControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnl = new System.Windows.Forms.Panel();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnAll = new System.Windows.Forms.Button();
            this.btnSelected = new System.Windows.Forms.Button();
            this.btnNotSelected = new System.Windows.Forms.Button();
            this.lblPCCount = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // pnl
            // 
            this.pnl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnl.AutoScroll = true;
            this.pnl.BackColor = System.Drawing.Color.LightSteelBlue;
            this.pnl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnl.Location = new System.Drawing.Point(1, 32);
            this.pnl.Margin = new System.Windows.Forms.Padding(4);
            this.pnl.Name = "pnl";
            this.pnl.Padding = new System.Windows.Forms.Padding(13, 0, 0, 0);
            this.pnl.Size = new System.Drawing.Size(392, 203);
            this.pnl.TabIndex = 1;
            // 
            // textBox1
            // 
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox1.Location = new System.Drawing.Point(0, 0);
            this.textBox1.Margin = new System.Windows.Forms.Padding(4);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(400, 22);
            this.textBox1.TabIndex = 0;
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // btnAll
            // 
            this.btnAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAll.Location = new System.Drawing.Point(24, 185);
            this.btnAll.Margin = new System.Windows.Forms.Padding(4);
            this.btnAll.Name = "btnAll";
            this.btnAll.Size = new System.Drawing.Size(65, 29);
            this.btnAll.TabIndex = 2;
            this.btnAll.Text = "All";
            this.btnAll.UseVisualStyleBackColor = true;
            this.btnAll.Visible = false;
            this.btnAll.Click += new System.EventHandler(this.btnAll_Click);
            // 
            // btnSelected
            // 
            this.btnSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelected.Location = new System.Drawing.Point(95, 185);
            this.btnSelected.Margin = new System.Windows.Forms.Padding(4);
            this.btnSelected.Name = "btnSelected";
            this.btnSelected.Size = new System.Drawing.Size(133, 29);
            this.btnSelected.TabIndex = 3;
            this.btnSelected.Text = "Selected";
            this.btnSelected.UseVisualStyleBackColor = true;
            this.btnSelected.Visible = false;
            this.btnSelected.Click += new System.EventHandler(this.btnSelected_Click);
            // 
            // btnNotSelected
            // 
            this.btnNotSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnNotSelected.Location = new System.Drawing.Point(233, 185);
            this.btnNotSelected.Margin = new System.Windows.Forms.Padding(4);
            this.btnNotSelected.Name = "btnNotSelected";
            this.btnNotSelected.Size = new System.Drawing.Size(133, 29);
            this.btnNotSelected.TabIndex = 4;
            this.btnNotSelected.Text = "Unselected";
            this.btnNotSelected.UseVisualStyleBackColor = true;
            this.btnNotSelected.Visible = false;
            this.btnNotSelected.Click += new System.EventHandler(this.btnNotSelected_Click);
            // 
            // lblPCCount
            // 
            this.lblPCCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblPCCount.AutoSize = true;
            this.lblPCCount.Location = new System.Drawing.Point(347, 218);
            this.lblPCCount.Name = "lblPCCount";
            this.lblPCCount.Size = new System.Drawing.Size(46, 17);
            this.lblPCCount.TabIndex = 0;
            this.lblPCCount.Text = "label1";
            this.lblPCCount.Visible = false;
            // 
            // CheckedListSearchControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnl);
            this.Controls.Add(this.lblPCCount);
            this.Controls.Add(this.btnSelected);
            this.Controls.Add(this.btnNotSelected);
            this.Controls.Add(this.btnAll);
            this.Controls.Add(this.textBox1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "CheckedListSearchControl";
            this.Size = new System.Drawing.Size(400, 240);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Panel pnl;
        public System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnAll;
        private System.Windows.Forms.Button btnSelected;
        private System.Windows.Forms.Button btnNotSelected;
        private System.Windows.Forms.Label lblPCCount;
    }
}
