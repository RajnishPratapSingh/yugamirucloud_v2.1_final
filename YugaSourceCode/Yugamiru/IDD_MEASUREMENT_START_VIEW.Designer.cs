﻿namespace Yugamiru
{
    partial class IDD_MEASUREMENT_START_VIEW
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.IDC_SearchBtn = new System.Windows.Forms.PictureBox();
            this.IDC_RotateBtn = new System.Windows.Forms.PictureBox();
            this.IDC_ShootBtn = new System.Windows.Forms.PictureBox();
            this.IDC_NextBtn = new System.Windows.Forms.PictureBox();
            this.IDC_BackBtn = new System.Windows.Forms.PictureBox();
            this.IDC_ID = new System.Windows.Forms.Label();
            this.IDC_Name = new System.Windows.Forms.Label();
            this.IDC_Gender = new System.Windows.Forms.Label();
            this.IDC_DoB = new System.Windows.Forms.Label();
            this.IDC_Height = new System.Windows.Forms.Label();
            this.ImageClipWnd = new System.Windows.Forms.PictureBox();
            this.ImagePrevWnd = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SearchBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RotateBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ShootBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_NextBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BackBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageClipWnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePrevWnd)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // IDC_SearchBtn
            // 
            this.IDC_SearchBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_SearchBtn.Location = new System.Drawing.Point(265, 38);
            this.IDC_SearchBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_SearchBtn.Name = "IDC_SearchBtn";
            this.IDC_SearchBtn.Size = new System.Drawing.Size(75, 30);
            this.IDC_SearchBtn.TabIndex = 0;
            this.IDC_SearchBtn.TabStop = false;
            this.IDC_SearchBtn.Click += new System.EventHandler(this.IDC_SearchBtn_Click);
            // 
            // IDC_RotateBtn
            // 
            this.IDC_RotateBtn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.IDC_RotateBtn.Location = new System.Drawing.Point(265, 98);
            this.IDC_RotateBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_RotateBtn.Name = "IDC_RotateBtn";
            this.IDC_RotateBtn.Size = new System.Drawing.Size(75, 30);
            this.IDC_RotateBtn.TabIndex = 1;
            this.IDC_RotateBtn.TabStop = false;
            this.IDC_RotateBtn.Click += new System.EventHandler(this.IDC_RotateBtn_Click);
            // 
            // IDC_ShootBtn
            // 
            this.IDC_ShootBtn.Location = new System.Drawing.Point(265, 155);
            this.IDC_ShootBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_ShootBtn.Name = "IDC_ShootBtn";
            this.IDC_ShootBtn.Size = new System.Drawing.Size(75, 30);
            this.IDC_ShootBtn.TabIndex = 2;
            this.IDC_ShootBtn.TabStop = false;
            this.IDC_ShootBtn.Click += new System.EventHandler(this.IDC_ShootBtn_Click);
            // 
            // IDC_NextBtn
            // 
            this.IDC_NextBtn.Location = new System.Drawing.Point(265, 226);
            this.IDC_NextBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_NextBtn.Name = "IDC_NextBtn";
            this.IDC_NextBtn.Size = new System.Drawing.Size(75, 30);
            this.IDC_NextBtn.TabIndex = 3;
            this.IDC_NextBtn.TabStop = false;
            this.IDC_NextBtn.Click += new System.EventHandler(this.IDC_NextBtn_Click);
            // 
            // IDC_BackBtn
            // 
            this.IDC_BackBtn.Location = new System.Drawing.Point(265, 292);
            this.IDC_BackBtn.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.IDC_BackBtn.Name = "IDC_BackBtn";
            this.IDC_BackBtn.Size = new System.Drawing.Size(75, 30);
            this.IDC_BackBtn.TabIndex = 4;
            this.IDC_BackBtn.TabStop = false;
            this.IDC_BackBtn.Click += new System.EventHandler(this.IDC_BackBtn_Click);
            // 
            // IDC_ID
            // 
            this.IDC_ID.AutoSize = true;
            this.IDC_ID.Location = new System.Drawing.Point(480, 38);
            this.IDC_ID.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IDC_ID.Name = "IDC_ID";
            this.IDC_ID.Size = new System.Drawing.Size(46, 17);
            this.IDC_ID.TabIndex = 5;
            this.IDC_ID.Text = "label1";
            this.IDC_ID.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // IDC_Name
            // 
            this.IDC_Name.AutoSize = true;
            this.IDC_Name.Location = new System.Drawing.Point(480, 98);
            this.IDC_Name.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IDC_Name.Name = "IDC_Name";
            this.IDC_Name.Size = new System.Drawing.Size(46, 17);
            this.IDC_Name.TabIndex = 6;
            this.IDC_Name.Text = "label2";
            this.IDC_Name.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // IDC_Gender
            // 
            this.IDC_Gender.AutoSize = true;
            this.IDC_Gender.Location = new System.Drawing.Point(480, 155);
            this.IDC_Gender.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IDC_Gender.Name = "IDC_Gender";
            this.IDC_Gender.Size = new System.Drawing.Size(46, 17);
            this.IDC_Gender.TabIndex = 7;
            this.IDC_Gender.Text = "label3";
            // 
            // IDC_DoB
            // 
            this.IDC_DoB.AutoSize = true;
            this.IDC_DoB.Location = new System.Drawing.Point(480, 226);
            this.IDC_DoB.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IDC_DoB.Name = "IDC_DoB";
            this.IDC_DoB.Size = new System.Drawing.Size(46, 17);
            this.IDC_DoB.TabIndex = 8;
            this.IDC_DoB.Text = "label4";
            // 
            // IDC_Height
            // 
            this.IDC_Height.AutoSize = true;
            this.IDC_Height.Location = new System.Drawing.Point(480, 292);
            this.IDC_Height.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.IDC_Height.Name = "IDC_Height";
            this.IDC_Height.Size = new System.Drawing.Size(46, 17);
            this.IDC_Height.TabIndex = 9;
            this.IDC_Height.Text = "label5";
            // 
            // ImageClipWnd
            // 
            this.ImageClipWnd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ImageClipWnd.BackColor = System.Drawing.Color.Black;
            this.ImageClipWnd.Location = new System.Drawing.Point(41, 64);
            this.ImageClipWnd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ImageClipWnd.Name = "ImageClipWnd";
            this.ImageClipWnd.Size = new System.Drawing.Size(133, 62);
            this.ImageClipWnd.TabIndex = 10;
            this.ImageClipWnd.TabStop = false;
            this.ImageClipWnd.SizeChanged += new System.EventHandler(this.ImageClipWnd_SizeChanged);
            this.ImageClipWnd.Paint += new System.Windows.Forms.PaintEventHandler(this.ImageClipWnd_Paint);
            this.ImageClipWnd.MouseDown += new System.Windows.Forms.MouseEventHandler(this.ImageClipWnd_MouseDown);
            this.ImageClipWnd.MouseMove += new System.Windows.Forms.MouseEventHandler(this.ImageClipWnd_MouseMove);
            this.ImageClipWnd.MouseUp += new System.Windows.Forms.MouseEventHandler(this.ImageClipWnd_MouseUp);
            // 
            // ImagePrevWnd
            // 
            this.ImagePrevWnd.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ImagePrevWnd.BackColor = System.Drawing.Color.Black;
            this.ImagePrevWnd.Location = new System.Drawing.Point(41, 226);
            this.ImagePrevWnd.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.ImagePrevWnd.Name = "ImagePrevWnd";
            this.ImagePrevWnd.Size = new System.Drawing.Size(133, 62);
            this.ImagePrevWnd.TabIndex = 11;
            this.ImagePrevWnd.TabStop = false;
            this.ImagePrevWnd.Paint += new System.Windows.Forms.PaintEventHandler(this.ImagePrevWnd_Paint);
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(300, 385);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(88, 44);
            this.pictureBox2.TabIndex = 12;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Click += new System.EventHandler(this.pictureBox2_Click);
            // 
            // IDD_MEASUREMENT_START_VIEW
            // 
            this.AllowDrop = true;
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(748, 530);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.ImagePrevWnd);
            this.Controls.Add(this.ImageClipWnd);
            this.Controls.Add(this.IDC_Height);
            this.Controls.Add(this.IDC_DoB);
            this.Controls.Add(this.IDC_Gender);
            this.Controls.Add(this.IDC_Name);
            this.Controls.Add(this.IDC_ID);
            this.Controls.Add(this.IDC_BackBtn);
            this.Controls.Add(this.IDC_NextBtn);
            this.Controls.Add(this.IDC_ShootBtn);
            this.Controls.Add(this.IDC_RotateBtn);
            this.Controls.Add(this.IDC_SearchBtn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.Name = "IDD_MEASUREMENT_START_VIEW";
            this.Text = "IDD_MEASUREMENT_START_VIEW";
            this.SizeChanged += new System.EventHandler(this.IDD_MEASUREMENT_START_VIEW_SizeChanged);
            this.DragDrop += new System.Windows.Forms.DragEventHandler(this.IDD_MEASUREMENT_START_VIEW_DragDrop);
            this.DragEnter += new System.Windows.Forms.DragEventHandler(this.IDD_MEASUREMENT_START_VIEW_DragEnter);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.IDD_MEASUREMENT_START_VIEW_Paint);
            this.MouseUp += new System.Windows.Forms.MouseEventHandler(this.IDD_MEASUREMENT_START_VIEW_MouseUp);
            this.Resize += new System.EventHandler(this.IDD_MEASUREMENT_START_VIEW_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.IDC_SearchBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_RotateBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_ShootBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_NextBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.IDC_BackBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageClipWnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImagePrevWnd)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox IDC_SearchBtn;
        private System.Windows.Forms.PictureBox IDC_RotateBtn;
        private System.Windows.Forms.PictureBox IDC_ShootBtn;
        private System.Windows.Forms.PictureBox IDC_NextBtn;
        private System.Windows.Forms.PictureBox IDC_BackBtn;
        private System.Windows.Forms.Label IDC_ID;
        private System.Windows.Forms.Label IDC_Name;
        private System.Windows.Forms.Label IDC_Gender;
        private System.Windows.Forms.Label IDC_DoB;
        private System.Windows.Forms.Label IDC_Height;
        private System.Windows.Forms.PictureBox ImageClipWnd;
        private System.Windows.Forms.PictureBox ImagePrevWnd;
        private System.Windows.Forms.PictureBox pictureBox2;
    }
}