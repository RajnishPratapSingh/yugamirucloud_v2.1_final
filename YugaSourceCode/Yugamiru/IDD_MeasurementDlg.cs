﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Yugamiru
{
    public partial class IDD_MeasurementDlg : Form
    {
        PictureBox picturebox1 = new PictureBox();
        Bitmap bmBack1;
        //public Panel Panel_reference;
        JointEditDoc m_GetDocument;
        //Addded by Sumit GSP-1188-----START
        public bool comingIromInitialScreen = false;
        //Addded by Sumit GSP-1188-----END
        bool finishbuttonclick = false;  //Addded by Rohini GTL#79
        public IDD_MeasurementDlg(JointEditDoc GetDocument)
        {
            InitializeComponent();
            txtHeight.Parent = IDC_Height.Parent;
            //Added By suhana for GSP 867
            cross_btn_ID.Parent = IDC_ID;
            cross_btn_ID.Visible = false;
            cross_btn_ID.TabStop = false;
            cross_btn_ID.FlatStyle = FlatStyle.Flat;
            cross_btn_ID.FlatAppearance.BorderSize = 0;
            cross_btn_ID.Dock = DockStyle.Right;

            cross_btn_Name.Parent = IDC_Name;
            cross_btn_Name.Visible = false;
            cross_btn_Name.TabStop = false;
            cross_btn_Name.FlatStyle = FlatStyle.Flat;
            cross_btn_Name.FlatAppearance.BorderSize = 0;
            cross_btn_Name.Dock = DockStyle.Right;

            //Added By suhana for GSP 867


            //--Added by Rajnish For Background License check--//
            objBackgroundWorker.WorkerSupportsCancellation = true;
            objBackgroundWorker.DoWork += ObjBackgroundWorker_DoWork;
            objBackgroundWorker.RunWorkerCompleted += ObjBackgroundWorker_RunWorkerCompleted;
            //------------------------------------------------//
            bmBack1 = Yugamiru.Properties.Resources.Mainpic2;
            this.Controls.Add(picturebox1);


            picturebox1.Size = new Size(bmBack1.Size.Width, bmBack1.Size.Height);
            picturebox1.Image = bmBack1;

            switch (GetDocument.GetInputMode())
            {
                case Constants.INPUTMODE_NEW:
                    {
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                        IDC_BackBtn.Visible = true;
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                        IDC_NextBtn.Visible = true;

                    }
                    break;
                case Constants.INPUTMODE_MODIFY:
                    {
                        //IDC_BackBtn.Image.Dispose();
                        IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                        IDC_BackBtn.Visible = false;

                        //IDC_NextBtn.Image.Dispose();
                        IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_down;
                        IDC_NextBtn.Visible = true;
                    }
                    break;
                default:
                    break;
            }

            m_GetDocument = GetDocument;
            IDC_ID.Focus();

            //commented by sumit GSP-1065-------START
            //if(Yugamiru.Properties.Resources.CURRENT_LANGUAGE == "Japanese")
            //{
            //    IDC_ID.MaxLength = 7;
            //    IDC_Name.MaxLength = 12;
            //}
            //else
            //commented by sumit GSP-1065-------END
            {
                IDC_ID.MaxLength = 10;
                IDC_Name.MaxLength = 20;
            }

            DateTime tm = new DateTime();
            DateTime.Now.ToString();

            int y = tm.Year;
            IDC_COMBO_GENDER.Items.Add("-");
            //IDC_COMBO_GENDER.
            if (GetDocument.GetLanguage() == "Japanese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if (GetDocument.GetLanguage() == "Chinese")
            {
                IDC_COMBO_GENDER.Items.Add("男");
                IDC_COMBO_GENDER.Items.Add("女");
            }
            if (GetDocument.GetLanguage() == "Korean")
            {
                IDC_COMBO_GENDER.Items.Add("남성");
                IDC_COMBO_GENDER.Items.Add("여성");
            }
            if (GetDocument.GetLanguage() == "English" || GetDocument.GetLanguage() == "Thai")
            {
                IDC_COMBO_GENDER.Items.Add("Male");
                IDC_COMBO_GENDER.Items.Add("Female");
            }


            // •\Ž¦
            switch (GetDocument.GetMeasurementViewMode())
            {
                case Constants.MEASUREMENTVIEWMODE_INITIALIZE:
                    {
                        IDC_COMBO_DAY.SelectedIndex = 0;
                        IDC_COMBO_GENDER.SelectedIndex = 0;
                        IDC_COMBO_MONTH.SelectedIndex = 0;
                        IDC_dtp_DOB.Value = DateTime.ParseExact("1/1/1980", "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                    }
                    break;
                case Constants.MEASUREMENTVIEWMODE_RETAIN:
                    {
                        IDC_ID.Text = GetDocument.GetDataID();
                        IDC_Name.Text = GetDocument.GetDataName();
                        string str = "-";
                        if (GetDocument.GetDataGender() == 1) str = "Male";
                        if (GetDocument.GetDataGender() == 2) str = "Female";

                        IDC_COMBO_GENDER.SelectedIndex = IDC_COMBO_GENDER.Items.IndexOf(str);

                        string year = string.Empty, month = string.Empty, day = string.Empty;
                        GetDocument.GetDataDoB(ref year, ref month, ref day);
                        IDC_BirthYear.Value = Convert.ToInt32(year);

                        IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(month);
                        IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_DAY.Items.IndexOf(day);
                        IDC_dtp_DOB.Value = DateTime.ParseExact(day + "/" + month + "/" + year, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

                        if (GetDocument.GetDataHeight() == 0)
                        {
                            str = "-";  // g’·
                        }
                        else
                        {
                            str = GetDocument.GetDataHeight().ToString();
                        }
                        IDC_Height.Value = Convert.ToInt32(str);
                    }
                    break;
                case Constants.MEASUREMENTVIEWMODE_NONE:
                default:
                    break;
            }

            CheckInputData();

        }
        public void Reload()
        {
            //Addded by Sumit GSP-1188-----START
            if (comingIromInitialScreen)
            {
                IDC_ID.Clear();
                IDC_Name.Clear();
                IDC_Height.Value = 100;
                txtHeight.Text = "100";
                IDC_COMBO_GENDER.SelectedIndex = 0;
                IDC_dtp_DOB.Value = DateTime.ParseExact("1/1/1980", "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);//--Added by Rajnish for GSP-1224
                //IDC_dtp_DOB.Value = IDC_dtp_DOB.MinDate; //--Commented by Rajnish for GSP-1224

                return;
            }
            //Addded by Sumit GSP-1188-----END
            IDC_ID.Focus();
            IDC_ID.Text = m_GetDocument.GetDataID();//Commented by Rajnish for GSP-1479
            IDC_Name.Text = m_GetDocument.GetDataName();//Commented by Rajnish for GSP-1479
            //Edited by Sumit GSP-1081---------START
            if (IDC_ID.Text.Contains("...") && !UtilityGlobal.ID_NAME_Handler.GetFull_ID.Contains("..."))
            {
                IDC_ID.Text = UtilityGlobal.ID_NAME_Handler.GetFull_ID;
            }
            if (IDC_Name.Text.Contains("...") && !UtilityGlobal.ID_NAME_Handler.GetFull_Name.Contains("..."))
            {
                IDC_Name.Text = UtilityGlobal.ID_NAME_Handler.GetFull_Name;
            }
            //Edited by Sumit GSP-1081---------END
            string str = "-";
            // if (m_GetDocument.GetDataGender() == 1) str = "Male";
            // if (m_GetDocument.GetDataGender() == 2) str = "Female";

            // IDC_COMBO_GENDER.SelectedIndex = IDC_COMBO_GENDER.Items.IndexOf(str);

            string year = string.Empty, month = string.Empty, day = string.Empty;
            m_GetDocument.GetDataDoB(ref year, ref month, ref day);
            IDC_BirthYear.Value = Convert.ToInt32(year);

            IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(month);
            IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_DAY.Items.IndexOf(day);
            IDC_dtp_DOB.Value = DateTime.ParseExact(day + "/" + month + "/" + year, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);

            if (m_GetDocument.GetDataHeight() == 0)
            {
                str = "100";  // g’·
            }
            else
            {
                str = m_GetDocument.GetDataHeight().ToString();
            }
            IDC_Height.Value = Convert.ToInt32(str);
            //added by Sumit for GTL#155   ---START    (Set Focus To ID)
            IDC_ID.Focus();
            //added by Sumit for GTL#155   ---END

        }
        public void DisposeControls()
        {
            this.picturebox1.Image.Dispose();

            this.IDC_BackBtn.Image.Dispose();
            this.IDC_NextBtn.Image.Dispose();

            Yugamiru.Properties.Resources.gobackgreen_up.Dispose();
            Yugamiru.Properties.Resources.gobackgreen_down.Dispose();
            Yugamiru.Properties.Resources.completegreen_down.Dispose();
            Yugamiru.Properties.Resources.Mainpic2.Dispose();


            this.Dispose();
            this.Close();
        }
        public bool CheckInputData()
        {
            if (IDC_BirthYear.Value.ToString() != null)
            {
                IDC_NextBtn.Visible = true;
            }
            else
            {
                IDC_NextBtn.Visible = false;

            }
            //Invalidate();

            return true;
        }

        //--Added by Rajnish For Background License check--//
        bool IsLicExpired { get; set; }
        string sLicStatus { get; set; }
        //bool IsFirstTimeOnly { get; set; }
        bool IsFirstTimeOnly = true;//--Added by Rajnish To Handle GSP-771--//
        public void ExpLicenceVerify(WebComCation.LicenseStatus s)
        {
            //Added by Sumit GSP-1321  -------START
            if (s == WebComCation.LicenseStatus.QlmDownValid)
            {
                return;
            }
            //Added by Sumit GSP-1321  -------END
            IsLicExpired = false;
            //ReCheck://--Commented by Rajnish for GSP-798 --//
            //IDC_NextBtn.Enabled = true;
            //WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            sLicStatus = s.ToString();
            //if (s != WebComCation.LicenseStatus.Unknown)//--Added by Rajnish For GSP-798--//
            {
                if (s != WebComCation.LicenseStatus.Valid)
                {

                    //Thread.CurrentThread.Abort();
                    IsLicExpired = true;

                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (Control c in this.Controls)
                            {
                                c.Enabled = false;
                            }
                        }));
                    }
                    else
                    {
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = false;
                        }
                    }
                    if (IsFirstTimeOnly)//--Added by Rajnish To Handle GSP-771--//
                    {
                        IsFirstTimeOnly = false; //--Added by Rajnish To Handle GSP-771--//
                                                 //--Added By Rajnish For GSP-768--//
                                                 //edited by sumit GSP-1109-----------START
                                                 //WebComCation.LicenseValidator.objfrmCustomMessageBox.lblMessage.Text = "Activation key  Expired";
                                                 //WebComCation.LicenseValidator.objfrmCustomMessageBox.tbDetails.Text = "Activation key : " + WebComCation.LicenseValidator.sActivationKey + Environment.NewLine + "Activation Date : " + WebComCation.LicenseValidator.sActivationDate + Environment.NewLine + "Expiry Date : " + WebComCation.LicenseValidator.sExpiryDate;
                        WebComCation.LicenseValidator.objfrmCustomMessageBox.lblMessage.Text = Properties.Resources.LICENSE_EXPIRED;
                        WebComCation.LicenseValidator.objfrmCustomMessageBox.tbDetails.Text = Properties.Resources.ACTIVATION_KEY + ": " + WebComCation.LicenseValidator.sActivationKey + Environment.NewLine + Properties.Resources.EXPIRY_DATE + ": " + WebComCation.LicenseValidator.sExpiryDate;
                        //edited by sumit GSP-1109-----------END
                        WebComCation.LicenseValidator.objfrmCustomMessageBox.btnOK.Focus();
                        WebComCation.LicenseValidator.objfrmCustomMessageBox.TopMost = true;

                        //Commented by Sumit GSP-1108-------START
                        //Commenting due to twice expiry msg, Sumit GSP-1351-------START
                        //WebComCation.LicenseValidator.objfrmCustomMessageBox.ShowDialog();
                        //Commenting due to twice expiry msg, Sumit GSP-1351-------END
                        //Commented by Sumit GSP-1108-------END
                        //----------------------//
                        //MessageBox.Show("Activation key  Expired", "gsport");


                        //Commented by rohini for GTL#68 -- START
                        //DialogResult deci = MessageBox.Show(Yugamiru.Properties.Resources.DO_YOU_WANT_TO_ENTER_LIC, "Yugamiru", MessageBoxButtons.YesNo);
                        //if (deci == DialogResult.Yes)
                        //Commented by rohini for GTL#68 -- END
                        //Added by rohini for GTL#68 -------START
                        DialogResult result = Frmmessagebox.Show(Yugamiru.Properties.Resources.DO_YOU_WANT_TO_ENTER_LIC, Frmmessagebox.enumMessageIcon.Question, Frmmessagebox.enumMessageButton.YesNo);
                        if (result == DialogResult.Yes)
                        //Added by rohini for GTL#68 -------END
                        {

                            try
                            {
                                WebComCation.LicenseValidator.IsKVFromLaunch = false; //--Added by Rajnish For GSP-782--//
                                WebComCation.KeyValidator kv = new WebComCation.KeyValidator();
                                kv.ShowDialog();
                                //goto ReCheck;//--Commented by Rajnish for GSP-798 --//
                            }
                            finally
                            {

                                //Added by Sumit GSP-782 For closing the animation progress bar------------START
                                //if (IsCallFromBC)
                                //commented by rohini for GSP-1379 ---START
                                //{
                                //    System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                                //    foreach (System.Diagnostics.Process p in allp)
                                //    {
                                //        if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                                //        {
                                //            p.CloseMainWindow();
                                //        }
                                //    }
                                //}
                                //commented by rohini for GSP-1379 ---END
                                UtilityGlobal.end_process1(); //added by rohini for GSP-1379
                                //Added by Sumit GSP-782 For closing the animation progress bar------------END
                            }
                        }
                        else
                        {
                            //Added by Sumit GSP-782 For closing the animation progress bar------------START
                            //if (IsCallFromBC)
                            {
                                //commented by rohini for GSP-1379 --START
                                //System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                                //foreach (System.Diagnostics.Process p in allp)
                                //{
                                //    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                                //    {
                                //        p.CloseMainWindow();
                                //    }
                                //}
                                //commented by rohini for GSP-1379 ---END
                                UtilityGlobal.end_process1(); //added by rohini for GSP-1379
                            }
                            //Added by Sumit GSP-782 For closing the animation progress bar------------END
                            //Application.Exit();
                            Environment.Exit(1);
                            //if (InvokeRequired)
                            //{
                            //    this.Invoke(new MethodInvoker(delegate
                            //    {
                            //        IDC_NextBtn.Enabled = false;
                            //    }));
                            //}
                            //else
                            //{
                            //    IDC_NextBtn.Enabled = false;
                            //}

                        }
                    }
                }
                else  //--Added by Rajnish On 06 Sep 18 For GSP-782--start--//
                {
                    IsFirstTimeOnly = true;//--Added by Rajnish To Handle GSP-782--//
                    IsLicExpired = false;

                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (Control c in this.Controls)
                            {
                                c.Enabled = true;
                            }
                        }));
                    }
                    else
                    {
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = true;
                        }
                    }
                }//----------------------------------------------------end--//
                 //Added by Sumit GSP-782 For closing the animation progress bar------------START
                 //if (IsCallFromBC)
                {
                    //commented by rohini for GSP-1379 ---START
                    //System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
                    //foreach (System.Diagnostics.Process p in allp)
                    //{
                    //    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                    //    {
                    //        p.CloseMainWindow();
                    //    }
                    //}
                    //commented by rohini for GSP-1379 ---END
                    UtilityGlobal.end_process1(); //added by rohini for GSP-1379
                }
                //Added by Sumit GSP-782 For closing the animation progress bar------------END
                //--Added by Rajnish For GSP-782--//
                if (WebComCation.LicenseValidator.IsKVFromLaunch == false)
                {
                    IsFirstTimeOnly = true;//--Added by Rajnish To Handle GSP-782--//
                    if (InvokeRequired)
                    {
                        this.Invoke(new MethodInvoker(delegate
                        {
                            foreach (Control c in this.Controls)
                            {
                                c.Enabled = true;
                            }
                        }));
                    }
                    else
                    {
                        foreach (Control c in this.Controls)
                        {
                            c.Enabled = true;
                        }
                    }
                }
                //------------------------------//
            }
            //--Added by Rajnish For GSP-798--//
        }
        //-------------------------------------------------//

        private void IDD_MeasurementDlg_Load(object sender, EventArgs e)
        {

            //Added by Sumit GSP-1146-----START
            //if (CloudManager.SyncSchedulekeyConfigValues.Custom.ToBool() &&
            //    CloudManager.SyncSchedulekeyConfigValues.Starting_Application.ToBool())
            //{
            //    try
            //    {
            //        Cursor.Current = Cursors.WaitCursor;
            //        CloudManager.CloudSyncManager.SynchronizeAll();
            //    }
            //    catch (Exception eex1)
            //    {
            //        //Suppress error coz it is a background activity. It will retry on next attempt
            //    }
            //    finally
            //    {
            //        Cursor.Current = Cursors.Default;
            //    }
            //}
            //Added by Sumit GSP-1146-----START

            // Added by sumit GSP-357, 346: --------START
            #region Commented by Sumit on 22_Mar_18, change validation Startegy
            //ReCheck:
            //WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            //if (s != WebComCation.LicenseStatus.Valid)
            //{
            //    Application.Exit();
            //    ////DialogResult deci= MessageBox.Show("Do you want to provide a valid license?", "gsport", MessageBoxButtons.YesNo);

            //    ////if (deci == DialogResult.Yes)
            //    //{
            //    //    try
            //    //    {

            //    //        WebComCation.KeyValidator kv = new WebComCation.KeyValidator();
            //    //        kv.ShowDialog();
            //    //        goto ReCheck;
            //    //    }
            //    //    finally
            //    //    {

            //    //    }
            //    //}
            //    ////else
            //    ////  Environment.Exit(0);
            //}

            #endregion
            //End  GSP-357, 346 Added By Sumit---------END
            IDC_dtp_DOB.MaxDate = DateTime.Now;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        public void IDD_MeasurementDlg_SizeChanged(object sender, EventArgs e)
        {
            //--to centre the picture box while resizing the form
            picturebox1.Left = (this.ClientSize.Width - picturebox1.Width) / 2;
            picturebox1.Top = (this.ClientSize.Height - picturebox1.Height) / 2 - 12;

            //--end

            this.IDC_BackBtn.Size = new Size(112, 42);
            this.IDC_BackBtn.Location = new Point(350, 600 + picturebox1.Top);

            this.IDC_NextBtn.Size = new Size(112, 42);
            this.IDC_NextBtn.Location = new Point(800, 600 + picturebox1.Top);

            //Edited/Added by Sumit for GTL#94   ---START
            //this.IDC_ID.Size = new Size(182, 47);
            this.IDC_ID.Size = new Size(182, 40);
            //Edited/Added by Sumit for GTL#94   ---END
            this.IDC_ID.Location = new Point(455, 106 + picturebox1.Top);

            //Edited/Added by Sumit for GTL#94   ---START
            //this.IDC_Name.Size = new Size(305, 47);
            this.IDC_Name.Size = new Size(305, 40);
            //Edited/Added by Sumit for GTL#94   ---END
            this.IDC_Name.Location = new Point(455, 176 + picturebox1.Top);

            //this.IDC_COMBO_GENDER.Size = new Size(102, 35);//Commented by Rohini for GSP-1236
            //Added by Sumit for GTL#51   ---START
            //this.IDC_COMBO_GENDER.Size = new Size(112, 35); //Added by rohini for GSP-1236
            //Edited/Added by Sumit for GTL#94   ---START
            //this.IDC_COMBO_GENDER.Size = new Size(182, 35);
            this.IDC_COMBO_GENDER.Size = new Size(182, 32);
            //Edited/Added by Sumit for GTL#94   ---END
            //Added by Sumit for GTL#51   ---END            
            this.IDC_COMBO_GENDER.Location = new Point(455, 236 + picturebox1.Top);

            //this.IDC_BirthYear.Size = new Size(80, 35);//(77, 41);
            //this.IDC_BirthYear.Location = new Point(455, 300 + picturebox1.Top);

            //this.IDC_COMBO_MONTH.Size = new Size(70, 35);
            //this.IDC_COMBO_MONTH.Location = new Point(582, 300 + picturebox1.Top);

            //this.IDC_COMBO_DAY.Size = new Size(70, 35);
            //this.IDC_COMBO_DAY.Location = new Point(702, 300 + picturebox1.Top);
            //Edited by Sumit--- Placing problem No relevant JIRA Task-------START
            //Edited/Added by Sumit for GTL#94   ---START
            //this.IDC_dtp_DOB.Size = new Size(305, 35);
            this.IDC_dtp_DOB.Size = new Size(305, 40);
            //Edited/Added by Sumit for GTL#94   ---END
            this.IDC_dtp_DOB.Location = new Point(455, 300 + picturebox1.Top);
            //Edited by Sumit--- Placing problem No relevant JIRA Task-------END

            this.IDC_Height.Size = new Size(80, 35);
            this.IDC_Height.Location = new Point(455, 367 + picturebox1.Top); //new Point(455, 364 + picturebox1.Top);
            //Added by Sumit YV0R-3----START
            //this.txtHeight.Size = new Size(67, 35);
            //this.txtHeight.Location = new Point(455, 367 + picturebox1.Top);
            txtHeight.BringToFront();
            //Edited/Added by Sumit for GTL#94   ---START
            //this.txtHeight.Size = new Size(67, 35);
            this.txtHeight.Size = new Size(67, IDC_Height.Height + 1);
            //Edited/Added by Sumit for GTL#94   ---END 
            this.txtHeight.Location = IDC_Height.Location;
            txtHeight.BringToFront();
            //Added by Sumit YV0R-3----END



            //IDC_ID.Top = picturebox1.Top + 110;


            IDC_ID.Left = (this.Width / 2 - IDC_ID.Left) + IDC_ID.Width + 50;
            IDC_Name.Left = (this.Width / 2 - IDC_Name.Left) + IDC_Name.Width - 74;
            //IDC_COMBO_GENDER.Left = (this.Width / 2 - IDC_COMBO_GENDER.Left) + IDC_COMBO_GENDER.Width + 130;//line commented by rohini for GSP-1236
            //Added by Sumit for GTL#51   ---START
            //IDC_COMBO_GENDER.Left = (this.Width / 2 - IDC_COMBO_GENDER.Left) + IDC_COMBO_GENDER.Width + 120; //line added by rohini for GSP-1236
            IDC_COMBO_GENDER.Left = (this.Width / 2 - IDC_COMBO_GENDER.Left) + IDC_COMBO_GENDER.Width + 50;
            //Added by Sumit for GTL#51   ---END
            IDC_dtp_DOB.Left = (this.Width / 2 - IDC_dtp_DOB.Left) + IDC_dtp_DOB.Width - 74;
            //IDC_dtp_DOB.Left = (this.Width / 2 - IDC_BirthYear.Left) + IDC_BirthYear.Width + 150;
            //IDC_BirthYear.Left = (this.Width / 2 - IDC_BirthYear.Left) + IDC_BirthYear.Width + 150;
            //--
            //IDC_BirthYear.Left = (this.Width / 2 - IDC_BirthYear.Left) + IDC_BirthYear.Width + 150;
            //IDC_COMBO_MONTH.Left = IDC_BirthYear.Left + IDC_COMBO_MONTH.Width + 60;
            //IDC_COMBO_DAY.Left = IDC_COMBO_MONTH.Left + IDC_COMBO_DAY.Width + 54;
            IDC_Height.Left = this.Width / 2 - IDC_Height.Left + 230;
            //Added by Sumit YV0R-3----START
            txtHeight.Left = this.Width / 2 - IDC_Height.Left + 230;
            txtHeight.BringToFront();
            this.txtHeight.Location = IDC_Height.Location;
            txtHeight.BringToFront();
            //Added by Sumit YV0R-3-END


            IDC_BackBtn.Left = this.Width / 2 - IDC_BackBtn.Left + 20;
            IDC_NextBtn.Left = IDC_BackBtn.Left + IDC_NextBtn.Width + 400 - 20;
            //updated Using by Sumit GTL#118----START
            UtilityGlobal.ReleaseMemoryNow();
            //updated Using by Sumit GTL#118----END
        }

        private void IDC_Name_TextChanged(object sender, EventArgs e)
        {

        }

        // edited by rohini for for GTL#79   -----START
        Timer t = new Timer();
        //System.Windows.Forms.Timer t = new System.Windows.Forms.Timer();
        // edited by rohini for for GTL#79   -----END
        //System.Threading.Thread
        static string m_ClickMode = "None";
        void timer_click(object sender, EventArgs e)
        {
            switch (m_ClickMode)
            {
                case "RETURN":
                    IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                    break;
                case "NEXT":
                    switch (m_GetDocument.GetInputMode())
                    {
                        case Constants.INPUTMODE_NEW:
                            m_GetDocument.SetMeasurementStartViewMode(Constants.MEASUREMENTSTARTVIEWMODE_SIDE_STANDING);
                            this.Visible = false;
                            m_GetDocument.GetMeasurementStart().Visible = true;
                            m_GetDocument.GetMeasurementStart().RefreshForm();
                            m_GetDocument.GetMeasurementStart().reload();
                            break;
                        case Constants.INPUTMODE_MODIFY:
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_on;
                            break;
                        default:
                            break;
                    }

                    break;
            }
            t.Stop();
        }
        private void IDC_BackBtn_Click(object sender, EventArgs e)
        {
            //added by Rajnish for GTL #457------Start
            if (txtHeight.Text == "")
            {
                txtHeight.Text = "100";
            }
            //added by Rajnish for GTL #457-------END

            m_ClickMode = "RETURN";
            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
            t.Interval = 100;
            t.Start();
            t.Tick += new EventHandler(timer_click);

            //Edited By Suhana For GSP 870
            if ((!string.IsNullOrEmpty(IDC_Name.Text)) || (!string.IsNullOrEmpty(IDC_ID.Text)) || (IDC_BirthYear.Value != 1980)
                || (IDC_COMBO_MONTH.SelectedIndex != 0) || (IDC_COMBO_DAY.SelectedIndex != 0) || (IDC_Height.Value != 100)
    //Added by Sumit GSP-1188-----START
    || txtHeight.Text != "100"
                //Added by Sumit GSP-1188-----END
                || (IDC_COMBO_GENDER.SelectedIndex != 0) || (IDC_dtp_DOB.Value != DateTime.ParseExact("1/1/1980", "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture)))
            {
                //Commented by rohini for GTL#68 -- START
                //DialogResult dialogResult = MessageBox.Show(
                ///*"if return to title view, current data will be lost. return to title view OK?"*/
                //Yugamiru.Properties.Resources.WARNING1, "Yugamiru", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                //if (dialogResult == DialogResult.Yes)
                //Commented by rohini for GTL#68 -- END
                //Added by rohini for GTL#68 -------START
                DialogResult result = Frmmessagebox.Show(Yugamiru.Properties.Resources.WARNING1, Frmmessagebox.enumMessageIcon.Question, Frmmessagebox.enumMessageButton.YesNo);
                if (result == DialogResult.Yes)
                //Added by rohini for GTL#68 -------end
                {
                    // CloseForm(EventArgs.Empty); // triggerring the event, to send it to form1 which is base form - step3
                    //this.Close();
                    m_GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
                    this.Visible = false;
                    m_GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
                    m_GetDocument.GetInitialScreen().Visible = true;
                    m_GetDocument.GetInitialScreen().RefreshForms();
                    m_GetDocument.m_SideImageBytes = null;
                    m_GetDocument.m_FrontKneedownImageBytes = null;
                    m_GetDocument.m_FrontStandingImageBytes = null;
                    IDC_ID.Clear();
                    IDC_Name.Clear();
                    IDC_COMBO_GENDER.SelectedIndex = 0;
                    IDC_COMBO_MONTH.SelectedIndex = 0;

                    IDC_COMBO_DAY.SelectedIndex = 0;
                    IDC_dtp_DOB.Value = DateTime.ParseExact("1/1/1980", "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                    //added by Sumit GSP-1188------START
                    txtHeight.Text = "100";
                    IDC_Height.Value = 100;
                    //RefreshForm();
                    //Addded by Sumit GSP-1188-----START
                    IDC_ID.Clear();
                    IDC_Name.Clear();
                    IDC_Height.Value = 100;
                    txtHeight.Text = "100";
                    IDC_COMBO_GENDER.SelectedIndex = 0;
                    comingIromInitialScreen = true;
                    //Addded by Sumit GSP-1188-----END

                    //Added by Sumit GSP-1261 -----START                            
                    m_GetDocument.GetMainScreen().isSavePending = false;
                    //Added by Sumit GSP-1261 -----END

                    //this.DisposeControls();

                }
                //else if (dialogResult == DialogResult.No)  //Commented by rohini for GTL#68 
                if (result == DialogResult.No)  //added by rohini for GTL#68
                {
                    IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_on;
                }
            }
            else
            {
                m_GetDocument.SetInputMode(Constants.INPUTMODE_NEW);
                this.Visible = false;
                m_GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
                m_GetDocument.GetInitialScreen().Visible = true;
                m_GetDocument.GetInitialScreen().RefreshForms();
                m_GetDocument.m_SideImageBytes = null;
                m_GetDocument.m_FrontKneedownImageBytes = null;
                m_GetDocument.m_FrontStandingImageBytes = null;
                IDC_ID.Clear();
                IDC_Name.Clear();
                IDC_COMBO_GENDER.SelectedIndex = 0;
                IDC_COMBO_MONTH.SelectedIndex = 0;
                IDC_COMBO_DAY.SelectedIndex = 0;
                IDC_dtp_DOB.Value = DateTime.ParseExact("1/1/1980", "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);


            }
            //Edited By Suhana For GSP 870
            //--Added by Rajnish For Background License check--//--Added for GSP-798--//
            //if (!objBackgroundWorker.IsBusy)
            //    objBackgroundWorker.RunWorkerAsync(2000);
            //{
            //    //--Added by Rajnish For Background License check--//
            //   if (sLicStatus == Convert.ToString(WebComCation.LicenseStatus.Valid))
            //   {
            //       if (!IsLicExpired)
            //       {
            //           //----------------------------//
            //       }
            //   }
            //}

        }
        public event EventHandler closeForm; // creating event handler - step1
        public void CloseForm(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = closeForm;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDC_NextBtn_Click(object sender, EventArgs e)
        {
            // --checking validation before licence for GSP - 1234------start--//
            //Added By Sumit for GSP-1065-----------START
            bool nonInteger = false;
            try
            {
                int k = Convert.ToInt16(txtHeight.Text);
            }
            catch
            {
                nonInteger = true;
            }
            if (nonInteger || txtHeight.Text.Length < 3 || Convert.ToInt16(txtHeight.Text) < 100)
            {
                //MessageBox.Show("Please enter a valid height");
                MessageBox.Show(Properties.Resources.PLEASE_ENTER_VALID_HEIGHT, "Yugamiru");
                txtHeight.Focus();
                return;
            }
            IDC_Height.Value = Convert.ToInt16(txtHeight.Text);
            //Added By Sumit for GSP-1065-----------END  

            //Added By Rohini for GSP-1180-----------START  
            if (IDC_ID.Text.Trim() == "" || IDC_Name.Text.Trim() == "" || IDC_COMBO_GENDER.Text == "-")
            {
                //MessageBox.Show("Please fill all the details");
                MessageBox.Show(Properties.Resources.PLEASE_FILL_ALL_DETAILS, "Yugamiru");
                return;
            }
            //Added By Rohini for GSP-1180-----------END
            //--checking validation before licence for GSP-1234------END--//

            //Added by Sumit YV0R-3-START
            //IDC_Height.los

            //if (!allowMoveNext)
            //    return;
            //Added by Sumit YV0R-3-END

            //Added by Sumit for GSP-1000 START
            //ResultView.isAnForeignRecord = false;
            //Added by Sumit for GSP-1000 END
            //--Added by Rajnish For Background License check--//
            if (!objBackgroundWorker.IsBusy)
            {
                //Edited by Sumit GSP-1176  -----START
                //objBackgroundWorker.RunWorkerAsync(2000);
                if (!m_GetDocument.GetMainScreen().tdLicense.IsAlive)
                    objBackgroundWorker.RunWorkerAsync(2000);
                //Edited by Sumit GSP-1176  -----END
            }

            {
                //Condition Edited by Sumit GSP-1176
                //if (sLicStatus == Convert.ToString(WebComCation.LicenseStatus.Valid))
                //Condition Edited by Sumit for GSP-1321  -------START
                //if (sLicStatus == Convert.ToString(WebComCation.LicenseStatus.Valid) || m_GetDocument.GetMainScreen().tdLicense.IsAlive)
                if (sLicStatus == Convert.ToString(WebComCation.LicenseStatus.Valid)
                    || sLicStatus == Convert.ToString(WebComCation.LicenseStatus.QlmDownValid)
                    || m_GetDocument.GetMainScreen().tdLicense.IsAlive)
                //Condition Edited by Sumit for GSP-1321  -------END
                {
                    if (!IsLicExpired)
                    {
                        //----------------------------//
                        m_ClickMode = "NEXT";
                        t.Interval = 100;
                        t.Start();
                        t.Tick += new EventHandler(timer_click);
                        m_GetDocument.SetMeasurementViewMode(Constants.MEASUREMENTVIEWMODE_NONE);
                        string y = string.Empty, m = string.Empty, d = string.Empty;

                        //--Commented by Rajnish For GSP-1175--//
                        // y = IDC_BirthYear.Value.ToString();
                        // m = IDC_COMBO_MONTH.GetItemText(this.IDC_COMBO_MONTH.SelectedItem).ToString();
                        // d = IDC_COMBO_DAY.GetItemText(IDC_COMBO_DAY.SelectedItem).ToString();
                        y = IDC_dtp_DOB.Value.ToString("yyyy");
                        m = IDC_dtp_DOB.Value.ToString("MM");
                        d = IDC_dtp_DOB.Value.ToString("dd");
                        if (!CheckDate(Convert.ToInt32(y), Convert.ToInt32(m), Convert.ToInt32(d)) || IDC_dtp_DOB.Value > DateTime.Today)
                        {
                            t.Stop();
                            MessageBox.Show(Yugamiru.Properties.Resources.INCORRECT_BIRTHDAY, "Yugamiru");
                            return;
                        }

                        //Edited By Sumit for GSP-1081-----------START
                        //m_GetDocument.SetDataID(IDC_ID.Text);
                        //m_GetDocument.SetDataName(IDC_Name.Text);
                        //clsIDNameHandler idname = new clsIDNameHandler(IDC_ID.Text, IDC_Name.Text, Yugamiru.Properties.Resources.CURRENT_LANGUAGE);
                        UtilityGlobal.Set_ID_NAME_Handler(IDC_ID.Text, IDC_Name.Text, Yugamiru.Properties.Resources.CURRENT_LANGUAGE);
                        // m_GetDocument.SetDataID(UtilityGlobal.ID_NAME_Handler.GetIDForApp());//Commented by Rajnish for GSP-1479
                        m_GetDocument.SetDataID(UtilityGlobal.ID_NAME_Handler.GetFull_ID);//Added by Rajnish for GSP-1479
                        //m_GetDocument.SetDataName(UtilityGlobal.ID_NAME_Handler.GetNameForApp());//Commented by Rajnish for GSP-1479
                        m_GetDocument.SetDataName(UtilityGlobal.ID_NAME_Handler.GetFull_Name);//Added by Rajnish for GSP-1479
                        IDC_ID.Text = UtilityGlobal.ID_NAME_Handler.GetFull_ID;
                        IDC_Name.Text = UtilityGlobal.ID_NAME_Handler.GetFull_Name;
                        //Edited By Sumit for GSP-1081-----------END

                        string gender = IDC_COMBO_GENDER.GetItemText(this.IDC_COMBO_GENDER.SelectedItem).ToString();


                        if (gender == "-")
                            m_GetDocument.SetDataGender(0);
                        if (gender == "Male" || gender == "男" || gender == "남성")
                            m_GetDocument.SetDataGender(1);
                        if (gender == "Female" || gender == "女" || gender == "여성")
                            m_GetDocument.SetDataGender(2);

                        m_GetDocument.SetDataDoB(y, m, d);    // ¶”NŒŽ“ú
                        //--Added By Rohini--//


                        //int height = Convert.ToInt32(Math.Round(IDC_Height.Value, 0));
                        //if (string.IsNullOrEmpty(IDC_Height.Text) || (height < 100))
                        //{
                        //    t.Stop();
                        //    MessageBox.Show("Enter valid Height");
                        //    return;
                        //}
                        //------------------//
                        float m_Height = (float)IDC_Height.Value;
                        m_GetDocument.SetDataHeight(m_Height);  // g’·	*/
                                                                //Added By Suhana For GSP 868 and GSP 869
                        m_GetDocument.showStatus();
                        //Added By Suhana For GSP 868 and GSP 869
                        if (m_GetDocument.GetInputMode() == Constants.INPUTMODE_NEW)
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_down;
                        else
                        {
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_up;
                            this.Visible = false;
                            FuctiontoStartfromMeasurementViewtoResultView(EventArgs.Empty);
                        }
                        //Added by Sumit GSP-1188-----START
                        comingIromInitialScreen = false;
                        //Added by Sumit GSP-1188-----END
                    }
                }
                //Added by Rohini for GTL#79 --- START

                else if (sLicStatus == null && objBackgroundWorker.IsBusy)
                {
                    this.Cursor = Cursors.WaitCursor;
                    finishbuttonclick = true;
                }

                //Added by Rohini for GTL#79 --- END
            }

        }
        bool CheckDate(int y, int m, int d)
        {

            int ActualDays = DateTime.DaysInMonth(y, m);
            if (y < 1900)
                return false;
            if (d > ActualDays)
                return false;


            return true;
            /*
            bool bLeap = false;
            if (y % 400 == 0 ||
                (y % 4 == 0 && y % 100 != 0))
                bLeap = true;

            if (m < 1 || 12 < m) return false;

            if (d < 1) return false;
            switch (m)
            {
                case 1:
                case 3:
                case 5:
                case 7:
                case 8:
                case 10:
                case 12:
                    if (31 < d) return false;
                    break;
                case 4:
                case 6:
                case 9:
                case 11:
                    if (30 < d) return false;
                    break;
                case 2:
                    if (bLeap && 29 < d) return false;
                    if (!bLeap && 28 < d) return false;
                    break;
            }

            return true;*/
        }
        public event EventHandler EventToStartNextScreen; // creating event handler - step1
        public void CloseFormToStartNextScreen(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToStartNextScreen;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        public event EventHandler EventfromMeasurementViewtoResultView; // creating event handler - step1
        public void FuctiontoStartfromMeasurementViewtoResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventfromMeasurementViewtoResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }

        private void IDD_MeasurementDlg_Paint(object sender, PaintEventArgs e)
        {
            //e.Graphics.DrawImage(bmBack1, (this.Width - bmBack1.Width) / 2, 0, bmBack1.Width, bmBack1.Height );
        }

        private void IDC_COMBO_MONTH_KeyDown(object sender, KeyEventArgs e)
        {
            // comboBox is readonly
            e.SuppressKeyPress = true;
        }

        private void IDC_COMBO_DAY_KeyDown(object sender, KeyEventArgs e)
        {
            // comboBox is readonly
            e.SuppressKeyPress = true;

        }
        public void RefreshForm()
        {
            IDC_ID.Focus();
            try
            {
                this.Cursor = Cursors.Default;  //Added by Rohini for GTL#79
                int Combo_Count = IDC_COMBO_GENDER.Items.Count;
                for (int i = 0; i < Combo_Count; i++)
                {
                    IDC_COMBO_GENDER.Items.RemoveAt(0);
                }

                IDC_COMBO_GENDER.Items.Add("-");

                IDC_COMBO_GENDER.Items.Add(Yugamiru.Properties.Resources.MALE);
                IDC_COMBO_GENDER.Items.Add(Yugamiru.Properties.Resources.FEMALE);

                //string gender = IDC_COMBO_GENDER.GetItemText(this.IDC_COMBO_GENDER.SelectedItem).ToString();

                if (m_GetDocument.GetDataGender() == 0)
                {
                    IDC_COMBO_GENDER.SelectedIndex = 0;
                }
                else if (m_GetDocument.GetDataGender() == 1)
                {
                    IDC_COMBO_GENDER.SelectedIndex = 1;
                }
                else if (m_GetDocument.GetDataGender() == 2)
                {
                    IDC_COMBO_GENDER.SelectedIndex = 2;
                }
                picturebox1.Image = Yugamiru.Properties.Resources.Mainpic2;
                m_GetDocument.GetMainScreen().RefreshMenuStrip(false);
                switch (m_GetDocument.GetInputMode())
                {
                    case Constants.INPUTMODE_NEW:
                        {
                            m_GetDocument.SetDataDoB("1980", "1", "1");
                            IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                            IDC_BackBtn.Visible = true;
                            IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                            IDC_NextBtn.Visible = true;

                        }
                        break;
                    case Constants.INPUTMODE_MODIFY:
                        {
                            if (m_GetDocument.GetFinalScreenMode() == Constants.FINAL_SCREEN_MODE_NONE)
                            {
                                IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_up;
                                IDC_BackBtn.Visible = true;
                                IDC_NextBtn.Image = Yugamiru.Properties.Resources.gonextgreen_up;
                                IDC_NextBtn.Visible = true;
                            }
                            else
                            {
                                IDC_BackBtn.Image = Yugamiru.Properties.Resources.gobackgreen_down;
                                IDC_BackBtn.Visible = false;
                                IDC_NextBtn.Image = Yugamiru.Properties.Resources.completegreen_down;
                                IDC_NextBtn.Visible = true;

                            }


                        }
                        break;
                    default:
                        break;
                }
            }
            finally
            {

            }

        }

        private void IDC_ID_KeyDown(object sender, KeyEventArgs e)
        {

        }
        //Added By suhana for GSP 867
        bool execTextChanged = true;//Added by Sumit GSP-1065
        System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"[~`!@#$%^&*()+=|\{}':;.,<>/?[\]""_-]"); //added by rohini for GTL#221
        private void IDC_ID_TextChanged(object sender, EventArgs e)
        {
            //--Added By Rajnish for GTL #475---Start--// 
            var textboxSender = (RichTextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #475---END--//
            //int cursorPosition = IDC_ID.SelectionStart; //added by rohini for GTL#222
            //added by rohini for GTL#221 --START
            if (r.IsMatch(((RichTextBox)(sender)).Text))
            {
                try
                {
                    ((RichTextBox)(sender)).Text = ((RichTextBox)(sender)).Text.Replace(((RichTextBox)(sender)).Text[((RichTextBox)(sender)).SelectionStart - 1].ToString(), "");
                }
                catch (Exception ex)
                {

#if debug
                    MessageBox.Show(ex.StackTrace);
#endif

                    return;
                }
            }
            //added by rohini for GTL#221 --END
            //Added by Sumit GSP-1065-------------Allow English Only--------START
            //if (Properties.Resources.CURRENT_LANGUAGE.ToUpper() == "ENGLISH")
            {
                if (!execTextChanged) return;
                string s = IDC_ID.Text;

                if (s.Length > 0)
                {
                    string correctVal = "";
                    foreach (char c in s)
                    {
                        if (System.Text.Encoding.UTF8.GetByteCount(new char[] { c }) > 1)
                        {
                            //IDC_ID.Text = "";                        
                        }
                        else
                        {
                            correctVal += c;
                        }
                    }
                    execTextChanged = false;
                    IDC_ID.Text = "";
                    IDC_ID.AppendText(correctVal);
                    IDC_ID.SelectionStart = cursorPosition; //added by rohini for GTL#222
                    IDC_ID.Focus();
                    execTextChanged = true;
                }
            }
            //GSP-1065--------END
            if (IDC_ID.Focus() && !string.IsNullOrEmpty(IDC_ID.Text))
            {
                cross_btn_ID.Visible = true;
            }
            else
                cross_btn_ID.Visible = false;

        }
        //Added By suhana for GSP 867
        private void IDD_MeasurementDlg_KeyDown(object sender, KeyEventArgs e)
        {

        }
        //--Added by Rajnish For Background License Check--//
        BackgroundWorker objBackgroundWorker = new BackgroundWorker();


        private void ObjBackgroundWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            ExpLicenceVerify((WebComCation.LicenseStatus)Enum.Parse(typeof(WebComCation.LicenseStatus), sLicStatus));
            //Added by Sumit GSP-821 on 25-Sep-2018-----START
            WebComCation.DateTemperingHandler.SavePresentTimeStamp();
            //Added by Sumit GSP-821 on 25-Sep-2018-----END
            //Added by Rohini for GTL#79 --- START
            if (finishbuttonclick == true)
            {
                this.Cursor = Cursors.Default;
                IDC_NextBtn_Click(null, new EventArgs());
                finishbuttonclick = false;
            }
            //Added by Rohini for GTL#79 --- END
        }

        private void ObjBackgroundWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            WebComCation.LicenseValidator.IsCallFromBC = true;
            WebComCation.LicenseStatus s = WebComCation.LicenseValidator.VerifyPreActivated();
            WebComCation.LicenseValidator.IsCallFromBC = false;
            sLicStatus = s.ToString();
        }

        private void IDD_MeasurementDlg_VisibleChanged(object sender, EventArgs e)
        {
            //Added by Sumit GSP-782 For closing the animation progress bar------------START
            //if (IsCallFromBC)
            //commented by rohini for GSP - 1379 -- START
            //{ 
            //System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
            //foreach (System.Diagnostics.Process p in allp)
            //{
            //    if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
            //    {
            //        p.CloseMainWindow();
            //    }
            //}
            //}
            //commented by rohini for GSP - 1379 -- END

            UtilityGlobal.end_process1(); //added by rohini for GSP-1379
            //Added by Sumit GSP-782 For closing the animation progress bar------------END


            if (objBackgroundWorker.IsBusy)
            {
                objBackgroundWorker.CancelAsync();
                objBackgroundWorker = new BackgroundWorker();
                objBackgroundWorker.DoWork += ObjBackgroundWorker_DoWork;
                objBackgroundWorker.RunWorkerCompleted += ObjBackgroundWorker_RunWorkerCompleted;
                objBackgroundWorker.WorkerSupportsCancellation = true;
            }
            else if (!objBackgroundWorker.IsBusy)
            {
                //Added by Sumit GSP-1176  -----START
                //objBackgroundWorker.RunWorkerAsync(2000);
                if (!m_GetDocument.GetMainScreen().tdLicense.IsAlive)
                    objBackgroundWorker.RunWorkerAsync(2000);
                //Added by Sumit GSP-1176  -----END
            }
            //Addded by Sumit GSP-1188-----START
            if (this.Visible && comingIromInitialScreen)
            {
                IDC_ID.Clear();
                IDC_Name.Clear();
                IDC_Height.Value = 100;
                txtHeight.Text = "100";
                IDC_COMBO_GENDER.SelectedIndex = 0;
                IDC_dtp_DOB.Value = DateTime.ParseExact("1/1/1980", "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);//--Added by Rajnish for GSP-1224
                //IDC_dtp_DOB.Value = IDC_dtp_DOB.MinDate;//--Commented by Rajnish for GSP-1224

            }
            //Addded by Sumit GSP-1188-----END



        }
        //Added By suhana for GSP 867
        private void cross_btn_Name_Click(object sender, EventArgs e)
        {
            IDC_Name.Text = "";
            this.cross_btn_Name.Visible = false;
        }
        //Added By suhana for GSP 867
        //Added By suhana for GSP 867
        private void IDC_Name_TextChanged_1(object sender, EventArgs e)
        {
            //--Added By Rajnish for GTL #475---Start--// 
            var textboxSender = (RichTextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");//commented by Rajnish for GSP-1413
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #475---END--//
            if (IDC_Name.Focus() && !string.IsNullOrEmpty(IDC_Name.Text))
            {
                cross_btn_Name.Visible = true;
            }
            else

                cross_btn_Name.Visible = false;
        }
        //Added By suhana for GSP 867
        //Added By suhana for GSP 867
        private void cross_btn_ID_Click(object sender, EventArgs e)
        {
            IDC_ID.Text = "";
            this.cross_btn_ID.Visible = false;
        }

        //Event Added by Sumit GSP-1065
        private void IDC_COMBO_GENDER_KeyDown(object sender, KeyEventArgs e)
        {
            //e.Handled = true; //Commented by Rajnish For GSP-1166
        }

        //Event Added by Sumit GSP-1065
        private void IDC_COMBO_GENDER_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        //Event Added by Sumit GSP-1065

        private void IDC_COMBO_GENDER_KeyUp(object sender, KeyEventArgs e)
        {
            // e.Handled = true; //Commented by Rajnish For GSP-1166
        }

        //Event Added by Sumit GSP-1065
        private void IDC_BirthYear_KeyDown(object sender, KeyEventArgs e)
        {
            //e.Handled = true; //Commented by Rajnish For GSP-1166
        }

        //Event Added by Sumit GSP-1065
        private void IDC_BirthYear_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = true; //Commented by Rajnish For GSP-1166
        }

        //Event Added by Sumit GSP-1065
        private void IDC_BirthYear_KeyUp(object sender, KeyEventArgs e)
        {
            //e.Handled = true; //Commented by Rajnish For GSP-1166
        }
        //--Added by Rajnish For GSP-1166--//
        private void IDC_Height_KeyPress(object sender, KeyPressEventArgs e)
        {
            //commented by Sumit on 18-Apr-19
            //((TextBox)IDC_Height.Controls[1]).MaxLength = 3;
        }

        private void IDC_COMBO_MONTH_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }
        private void IDC_COMBO_DAY_KeyPress(object sender, KeyPressEventArgs e)
        {
            //e.Handled = true;
        }
        //--------------------------------//
        private void IDD_MeasurementDlg_MouseUp(object sender, MouseEventArgs e)
        {
            //Event Added by Sumit GSP-1146
            //Added by Sumit GSP-1146-----START
            CloudManager.SynchronizationScheduler.lastActivityTime = DateTime.Now;
            //Added by Sumit GSP-1146-----START
        }

        private void IDC_ID_KeyPress(object sender, KeyPressEventArgs e)
        {
            ////Added bby Sumit GSP-1065--START
            //if (System.Text.Encoding.UTF8.GetByteCount(new char[] { e.KeyChar }) > 1)
            //{
            //    e.Handled = true;
            //}
            ////GSP-1065 ------END

            //Added by Sumit GSP-1261 -----START
            m_GetDocument.GetMainScreen().isSavePending = true;
            //Added by Sumit GSP-1261 -----END
        }


        private void IDC_Height_Leave(object sender, EventArgs e)
        {
            //Added by Sumit YV0R-3-START
            //if (IDC_Height.Value < 100)
            //{
            //    //IDC_Height.Value = 100;
            //    MessageBox.Show("Please Enter a valid height");
            //    IDC_Height.Focus();
            //    allowMoveNext = false;
            //}
            //else
            //{
            //    allowMoveNext = true;
            //}
            //Added by Sumit YV0R-3-END
        }

        private void IDC_Height_ValueChanged(object sender, EventArgs e)
        {
            //if(IDC_Height.Value<100)
            //{
            //    IDC_Height.Value = 100;
            //}
            txtHeight.Text = IDC_Height.Value.ToString();
        }

        private void IDC_Height_KeyUp(object sender, KeyEventArgs e)
        {
            //if (e.KeyData == Keys.Back)
            //    return;
            //if (IDC_Height.Value < 100)
            //{
            //    IDC_Height.Value = 100;
            //}
        }
        private void IDC_Height_LostFocus(object sender, System.EventArgs e)
        {
            //Added by Sumit YV0R-3-START
            //if (IDC_Height.Value < 100)
            //{
            //    //IDC_Height.Value = 100;
            //    MessageBox.Show("Please Enter a valid height");
            //    IDC_Height.Focus();
            //    allowMoveNext = false;
            //}
            //else
            //{
            //    allowMoveNext = true;
            //}
            //Added by Sumit YV0R-3-END
        }

        private void txtHeight_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == '0' || e.KeyChar == '1' || e.KeyChar == '2'
                || e.KeyChar == '3' || e.KeyChar == '4' || e.KeyChar == '5'
                || e.KeyChar == '6' || e.KeyChar == '7' || e.KeyChar == '8' || e.KeyChar == '9')
            {
                //Added by Sumit GSP-1261 -----START
                m_GetDocument.GetMainScreen().isSavePending = true;
                //Added by Sumit GSP-1261 -----END
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtHeight_MouseClick(object sender, MouseEventArgs e)
        {

        }

        private void txtHeight_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Modifiers == Keys.Control && e.KeyCode == Keys.V)
            {
                // cancel the "paste" function
                e.SuppressKeyPress = true;
            }
            ///added by rohini for GSP-1369 START
            try
            {
                int height = Convert.ToInt32(txtHeight.Text);

                if (e.KeyCode == Keys.Up)
                {
                    if (height < 999)
                        txtHeight.Text = (height + 1).ToString();
                    IDC_Height.Value = Convert.ToDecimal(txtHeight.Text); //added by rohini for GTL#223
                }
                else if (e.KeyCode == Keys.Down)
                {
                    if (height > 100)
                        txtHeight.Text = (height - 1).ToString();
                    IDC_Height.Value = Convert.ToDecimal(txtHeight.Text); //added by rohini for GTL#223
                }
            }
            catch
            {

            }
            ///added by rohini for GSP-1369 END
        }

        private void IDC_Name_KeyPress(object sender, KeyPressEventArgs e)
        {
            //Added by Sumit GSP-1261 -----START
            m_GetDocument.GetMainScreen().isSavePending = true;
            //Added by Sumit GSP-1261 -----END
        }

        private void IDC_COMBO_GENDER_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Added by Sumit GSP-1261 -----START
            //m_GetDocument.GetMainScreen().isSavePending = true;
            //Added by Sumit GSP-1261 -----END
        }

        private void IDC_dtp_DOB_ValueChanged(object sender, EventArgs e)
        {
            //Added by Sumit GSP-1261 -----START
            //m_GetDocument.GetMainScreen().isSavePending = true;
            //Added by Sumit GSP-1261 -----END
            //Added by Sumit GTL#130 -----START
            IDC_BirthYear.Value = IDC_dtp_DOB.Value.Year;
            int indexM = IDC_COMBO_MONTH.FindString(IDC_dtp_DOB.Value.Month.ToString());
            IDC_COMBO_MONTH.SelectedIndex = indexM;
            IDC_COMBO_MONTH.SelectedItem = IDC_COMBO_MONTH.Items[indexM];

            int indexD = IDC_COMBO_DAY.FindString(IDC_dtp_DOB.Value.Day.ToString());
            IDC_COMBO_DAY.SelectedIndex = indexD;
            IDC_COMBO_DAY.SelectedItem = IDC_COMBO_DAY.Items[indexD];

            //IDC_COMBO_MONTH.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(IDC_dtp_DOB.Value.Month);
            //IDC_COMBO_DAY.SelectedIndex = IDC_COMBO_MONTH.Items.IndexOf(IDC_dtp_DOB.Value.Day);
            //Added by Sumit GTL#130 -----END
        }

        private void IDC_COMBO_GENDER_SelectedValueChanged(object sender, EventArgs e)
        {
            //Added by Sumit GSP-1261 -----START
            //m_GetDocument.GetMainScreen().isSavePending = true;
            //Added by Sumit GSP-1261 -----END
        }

        //added by rohini for GTL#223 --START
        private void txtHeight_TextChanged(object sender, EventArgs e)
        {
            try
            {
                IDC_Height.Value = Convert.ToDecimal(txtHeight.Text);
            }
            catch
            {

            }
        }

        //Event added by Rajnish for GTL #457 ----START
        private void txtHeight_Leave(object sender, EventArgs e)
        {
            if (txtHeight.Text == "")
            {
                txtHeight.Text = "100";

            }
        }
        //Event added by Rajnish for GTL #457  ---END 
        //Added By suhana for GSP 867
        //-----------------------------------------------------//
    }
}
