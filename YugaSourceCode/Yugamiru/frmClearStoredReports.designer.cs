﻿namespace Yugamiru
{
    partial class frmClearStoredReports
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClearStoredReports));
            this.btnContinueRelease = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btnCancelUninstall = new System.Windows.Forms.Button();
            this.lblTotalReportsCount = new System.Windows.Forms.Label();
            this.lblCloudReportsCount = new System.Windows.Forms.Label();
            this.lblNotSavedReportsCount = new System.Windows.Forms.Label();
            this.btnSaveToCloud = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.label10 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnContinueRelease
            // 
            this.btnContinueRelease.Location = new System.Drawing.Point(725, 194);
            this.btnContinueRelease.Name = "btnContinueRelease";
            this.btnContinueRelease.Size = new System.Drawing.Size(231, 40);
            this.btnContinueRelease.TabIndex = 1;
            this.btnContinueRelease.Text = "Continue Key Release";
            this.btnContinueRelease.UseVisualStyleBackColor = true;
            this.btnContinueRelease.Click += new System.EventHandler(this.btnContinueRelease_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(12, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(699, 18);
            this.label2.TabIndex = 2;
            this.label2.Text = "Releasing License Activation Key will remove all saved patient reports from this " +
    "PC. See the details below.";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Enabled = false;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.Black;
            this.label3.Location = new System.Drawing.Point(12, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(151, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "See the details below.";
            this.label3.Visible = false;
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.Black;
            this.label4.Location = new System.Drawing.Point(12, 68);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(312, 17);
            this.label4.TabIndex = 4;
            this.label4.Text = "Total number of reports will be removed from PC";
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.Black;
            this.label5.Location = new System.Drawing.Point(12, 134);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(247, 17);
            this.label5.TabIndex = 5;
            this.label5.Text = "Number of reports not saved on cloud";
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.ForeColor = System.Drawing.Color.Black;
            this.label6.Location = new System.Drawing.Point(12, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(223, 17);
            this.label6.TabIndex = 6;
            this.label6.Text = "Number of reports saved on cloud";
            // 
            // btnCancelUninstall
            // 
            this.btnCancelUninstall.Location = new System.Drawing.Point(15, 187);
            this.btnCancelUninstall.Name = "btnCancelUninstall";
            this.btnCancelUninstall.Size = new System.Drawing.Size(188, 49);
            this.btnCancelUninstall.TabIndex = 7;
            this.btnCancelUninstall.Text = "Cancel Uninstall";
            this.btnCancelUninstall.UseVisualStyleBackColor = true;
            this.btnCancelUninstall.Visible = false;
            // 
            // lblTotalReportsCount
            // 
            this.lblTotalReportsCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblTotalReportsCount.AutoSize = true;
            this.lblTotalReportsCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalReportsCount.ForeColor = System.Drawing.Color.Black;
            this.lblTotalReportsCount.Location = new System.Drawing.Point(386, 66);
            this.lblTotalReportsCount.Name = "lblTotalReportsCount";
            this.lblTotalReportsCount.Size = new System.Drawing.Size(58, 20);
            this.lblTotalReportsCount.TabIndex = 8;
            this.lblTotalReportsCount.Text = "Count";
            // 
            // lblCloudReportsCount
            // 
            this.lblCloudReportsCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblCloudReportsCount.AutoSize = true;
            this.lblCloudReportsCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCloudReportsCount.ForeColor = System.Drawing.Color.Black;
            this.lblCloudReportsCount.Location = new System.Drawing.Point(386, 101);
            this.lblCloudReportsCount.Name = "lblCloudReportsCount";
            this.lblCloudReportsCount.Size = new System.Drawing.Size(58, 20);
            this.lblCloudReportsCount.TabIndex = 9;
            this.lblCloudReportsCount.Text = "Count";
            // 
            // lblNotSavedReportsCount
            // 
            this.lblNotSavedReportsCount.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lblNotSavedReportsCount.AutoSize = true;
            this.lblNotSavedReportsCount.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNotSavedReportsCount.ForeColor = System.Drawing.Color.Red;
            this.lblNotSavedReportsCount.Location = new System.Drawing.Point(386, 132);
            this.lblNotSavedReportsCount.Name = "lblNotSavedReportsCount";
            this.lblNotSavedReportsCount.Size = new System.Drawing.Size(58, 20);
            this.lblNotSavedReportsCount.TabIndex = 10;
            this.lblNotSavedReportsCount.Text = "Count";
            // 
            // btnSaveToCloud
            // 
            this.btnSaveToCloud.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnSaveToCloud.Location = new System.Drawing.Point(455, 123);
            this.btnSaveToCloud.Name = "btnSaveToCloud";
            this.btnSaveToCloud.Size = new System.Drawing.Size(249, 40);
            this.btnSaveToCloud.TabIndex = 11;
            this.btnSaveToCloud.Text = "Save to cloud now.";
            this.btnSaveToCloud.UseVisualStyleBackColor = true;
            this.btnSaveToCloud.Click += new System.EventHandler(this.btnSaveToCloud_Click);
            // 
            // label7
            // 
            this.label7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label7.Location = new System.Drawing.Point(350, 71);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(24, 25);
            this.label7.TabIndex = 17;
            this.label7.Text = "?";
            this.toolTip1.SetToolTip(this.label7, resources.GetString("label7.ToolTip"));
            this.label7.Visible = false;
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label8.Location = new System.Drawing.Point(263, 104);
            this.label8.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(24, 25);
            this.label8.TabIndex = 18;
            this.label8.Text = "?";
            this.toolTip1.SetToolTip(this.label8, "This represents the number of reports saved on Yugamiru Cloud as well and can be " +
        "\r\ndownloaded in future using Active License with Yugamiru Cloud application.");
            this.label8.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label9.Location = new System.Drawing.Point(288, 137);
            this.label9.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(24, 25);
            this.label9.TabIndex = 19;
            this.label9.Text = "?";
            this.toolTip1.SetToolTip(this.label9, "This represents the number of reports which are not saved on Yugamiru Cloud\r\n due" +
        " to offline working and will be lost permanently.");
            this.label9.Visible = false;
            // 
            // toolTip1
            // 
            this.toolTip1.IsBalloon = true;
            this.toolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info;
            // 
            // label10
            // 
            this.label10.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label10.Location = new System.Drawing.Point(720, 132);
            this.label10.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(24, 25);
            this.label10.TabIndex = 20;
            this.label10.Text = "?";
            this.toolTip1.SetToolTip(this.label10, "Click to save patient reports to Yugamiru Cloud now.");
            this.label10.Visible = false;
            // 
            // frmClearStoredReports
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(968, 248);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.btnSaveToCloud);
            this.Controls.Add(this.lblNotSavedReportsCount);
            this.Controls.Add(this.lblCloudReportsCount);
            this.Controls.Add(this.lblTotalReportsCount);
            this.Controls.Add(this.btnCancelUninstall);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnContinueRelease);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmClearStoredReports";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.Text = "Yugamiru";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnContinueRelease;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnCancelUninstall;
        private System.Windows.Forms.Label lblTotalReportsCount;
        private System.Windows.Forms.Label lblCloudReportsCount;
        private System.Windows.Forms.Label lblNotSavedReportsCount;
        private System.Windows.Forms.Button btnSaveToCloud;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
    }
}

