using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using CloudManager;

namespace Yugamiru
{
    public partial class OpenResult : Form
    {

        JointEditDoc m_JointEditDoc;
        bool SearchById_Flag = false;
        bool SearchByIdAndName_Flag = false;
        bool SearchByName_Flag = false;
        bool searchbysearch = false;  //added by rohini for GTL#248
        private int PgSize = 20;
        private int CurrentPageIndex = 1;
        private int TotalPage = 0;
        int totalbysearch;  //added by rohini for GTL#248
        // added by rohini for  GSP-1381 START
        bool search = false;
        int count1, sum, slno, total, count2;
        int rowCount = 0;
        // added by rohini for  GSP-1381 END 
        int index; //added by rohini for GSP-1393

        public OpenResult(JointEditDoc GetDocument)
        {
            //added by Sumit for GTL#251   ---START
            checkedListSearchControl1 = new CustomControls.CheckedListSearchControl();
            //added by Sumit for GTL#251   ---END
            checkedListSearchControl1.InnerCb_CheckedChanged += new System.EventHandler(checkedListSearchControl1_Click);//Added By Rajnish for GSP-1488
            InitializeComponent();
            m_JointEditDoc = GetDocument;

            /*dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "yy-MM-dd";*/
            dateTimePicker1.Enabled = false;

            /* dateTimePicker2.Format = DateTimePickerFormat.Custom;
             dateTimePicker2.CustomFormat = "yy-MM-dd";*/
            dateTimePicker2.Enabled = false;
            /* commented for JIRA # 395  
              dataGridView1.Rows.Clear();
              dataGridView1.Refresh();
              LoadDataGridWithDefaultValues();
              */



            //Added by Sumit for GTL#458   ---START
            SetDatePickers();
            //Added by Sumit for GTL#458   ---END
            textbox_PageNo.TextAlign = HorizontalAlignment.Center;
            button1.Enabled = false; // added for JIRA # 395   
            btnGetLatest.Enabled = false;//Added by Rajnish for GSP-1488
            //Edited by Sumit GTL#251   ---START
            //checkedListSearchControl1.FillData(PCNameList());
            DataTable dtPCNameList = PCNameList();
            //checkedListSearchControl1 = new CustomControls.CheckedListSearchControl();
            checkedListSearchControl1.textBox1.Clear();
            if (dtPCNameList == null || dtPCNameList.Rows.Count == 0)
            {
                checkedListSearchControl1.Enabled = false;
            }
            else
            {
                checkedListSearchControl1.FillData(dtPCNameList);
                checkedListSearchControl1.Enabled = true;
            }
            //Edited by Sumit GTL#251   ---END
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dataGridView1.Rows.Count > 0)//--Added by Rajnish for GSP-749--//
            {
                //Added by Sumit GSP-1261 -----START
                m_JointEditDoc.GetMainScreen().isSavePending = false;
                //Added by Sumit GSP-1261 -----END
                var selectedRow = dataGridView1.SelectedRows[0];

                //Edietd by Sumit GSP-1381   ---START
                //var primaryKey = int.Parse(selectedRow.Cells[3].Value.ToString());
                var primaryKey = int.Parse(selectedRow.Cells[4].Value.ToString());
                //Edietd by Sumit GSP-1381   ---END
                //Added by Sumit GSP-1146--------START
                //if(WebComCation.Utility.IsInternetConnected())
                //{
                //    if(CloudManager.SyncSchedulekeyConfigValues.TakeLatest_Before_Open.ToBool())
                //    {
                //        try
                //        {
                //            Cursor.Current = Cursors.WaitCursor;
                //            CloudManager.CloudSyncManager.SynchronizeAll();
                //        }
                //        catch (Exception eex1)
                //        {
                //            //Suppress error coz it is a background activity. It will retry on next attempt
                //        }
                //        finally
                //        {
                //            Cursor.Current = Cursors.Default;
                //        }
                //    }
                //}
                //Added by Sumit GSP-1146--------END
                m_JointEditDoc.SetUniqueId(primaryKey);

                LoadPatientDetails(primaryKey);
                LoadFrontBodyPositionStandingValues(primaryKey);
                LoadFrontBodyPositionKneedownValues(primaryKey);
                LoadSideBodyPositionValues(primaryKey);
                this.Visible = false;
                ResultView.editcoment = false;//added by rohini for GSP-1478
                ResultView.backbutton = true; //added by rohini for GSP-1459
                ResultView.oldrecord = true; //added by rohini for GSP-1308
                FunctionToChangeResultView(EventArgs.Empty);
                //Added by Sumit for GSP-1000 START
                if (CloudManager.GlobalItems.IsOtherPCData(primaryKey.ToString()))
                {
                    ResultView.isAnForeignRecord = true;
                }
                else
                {
                    ResultView.isAnForeignRecord = false;
                }
                //Added by Sumit for GSP-1000 END
            }


            /* 
             *Code to write image in a jpeg file on desktop
             *
             *
             * byte[] bytes = null;
                Image temp;
             * bytes = Convert.FromBase64String(row["imagebytes"].ToString());
             * MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
               ms.Write(bytes, 0, bytes.Length);
               temp = Image.FromStream(ms, true);

               Image<Bgr, byte> Emgu_stand_image =
                   new Image<Bgr, byte>(new Bitmap(temp));
               Emgu_stand_image = Emgu_stand_image.Resize(1024, 1280,
                   Emgu.CV.CvEnum.Inter.Linear);

               using (Bitmap bmp = new Bitmap(Emgu_stand_image.ToBitmap()))
               {
                   bmp.Save(@"C:\Users\Meena\Desktop\newimage.jpg");
               }
   */
        }
        public event EventHandler EventToChangeResultView; // creating event handler - step1
        public void FunctionToChangeResultView(EventArgs e) // defining the event handler  for triggerring/raising the event - step2
        {
            EventHandler eventHandler = EventToChangeResultView;
            if (eventHandler != null)
            {

                eventHandler(this, e);
            }
        }
        private void LoadPatientDetails(int uniqueKey)
        {
            //string query = "select patientid,name,gender,year,month,date,height,comment,BenchmarkDistance," +
            //    "MeasurementTime from patientdetails where uniqueid =" + uniqueKey;
            string query = "select patientid,patientdetails.Name,gender,year,month,date,height,patientdetails.Comment,BenchmarkDistance," +
                "MeasurementTime, tblInstallInfo.Computer_name, tblInstallInfo.Installed_by from patientdetails Inner join tblInstallInfo on tblInstallInfo.reg_user_id = PatientDetails.reg_user_id where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                #region Old Code Commented by Rajnish for GSP-700
                //m_JointEditDoc.SetDataID(row[0].ToString());
                //m_JointEditDoc.SetDataName(row[1].ToString());
                //m_JointEditDoc.SetDataGender(int.Parse(row[2].ToString()));
                //m_JointEditDoc.SetDataDoB(row[3].ToString(), row[4].ToString(), row[5].ToString());
                //m_JointEditDoc.SetDataHeight(float.Parse(row[6].ToString()));
                //m_JointEditDoc.SetDataMeasurementTime(row[9].ToString());
                //m_JointEditDoc.SetDataComment(row[7].ToString());
                //m_JointEditDoc.SetBenchmarkDistance(int.Parse(row[8].ToString()));
                #endregion

                //--Modified by Rajnish For GSP-700--//
                //Edited by Sumit GSP-1081-------START
                //m_JointEditDoc.SetDataID(row[0].ToString().Replace("\"\"", "\""));
                //m_JointEditDoc.SetDataName(row[1].ToString().Replace("\"\"", "\""));
                ResultView.uid = uniqueKey;  //added by rohini for GSP-1478
                string fullID = row[0].ToString().Replace("\"\"", "\"");
                string full_Name = row[1].ToString().Replace("\"\"", "\"");
                UtilityGlobal.Set_ID_NAME_Handler(fullID, full_Name, Properties.Resources.CURRENT_LANGUAGE);
                //m_JointEditDoc.SetDataID(UtilityGlobal.ID_NAME_Handler.GetIDForApp());//Commented by Rajnish GSP-1479
                m_JointEditDoc.SetDataID(UtilityGlobal.ID_NAME_Handler.GetFull_ID);//Added by Rajnish GSP-1479
                //m_JointEditDoc.SetDataName(UtilityGlobal.ID_NAME_Handler.GetNameForApp());//Commented by Rajnish GSP-1479
                m_JointEditDoc.SetDataName(UtilityGlobal.ID_NAME_Handler.GetFull_Name);//Added by Rajnish GSP-1479

                //Edited by Sumit GSP-1081-------END

                m_JointEditDoc.SetDataGender(int.Parse(row[2].ToString()));
                m_JointEditDoc.SetDataDoB(row[3].ToString(), row[4].ToString(), row[5].ToString());
                m_JointEditDoc.SetDataHeight(float.Parse(row[6].ToString()));
                m_JointEditDoc.SetDataMeasurementTime(row[9].ToString());
                m_JointEditDoc.SetDataComment(row[7].ToString().Replace("\"\"", "\""));
                m_JointEditDoc.SetBenchmarkDistance(int.Parse(row[8].ToString()));
                //-----------------------------------//
            }
        }
        private void LoadFrontBodyPositionStandingValues(int uniqueKey)
        {
            string query = "select * from FrontBodyPositionStanding where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                FrontBodyPosition Standing_Position = new FrontBodyPosition();
                if (bool.Parse(row[1].ToString()))
                    Standing_Position.SetKneePositionDetected();
                if (bool.Parse(row[2].ToString()))
                    Standing_Position.SetUnderBodYPositionDetected();
                if (bool.Parse(row[3].ToString()))
                    Standing_Position.SetUpperBodyPositionDetected();

                Standing_Position.m_ptChin = new Point(int.Parse(row[4].ToString()),
                                                       int.Parse(row[5].ToString()));
                Standing_Position.m_ptGlabella = new Point(int.Parse(row[6].ToString()),
                                                       int.Parse(row[7].ToString()));
                Standing_Position.m_ptLeftAnkle = new Point(int.Parse(row[8].ToString()),
                                                       int.Parse(row[9].ToString()));
                Standing_Position.m_ptLeftBelt = new Point(int.Parse(row[10].ToString()),
                                                       int.Parse(row[11].ToString()));
                Standing_Position.m_ptLeftEar = new Point(int.Parse(row[12].ToString()),
                                                       int.Parse(row[13].ToString()));
                Standing_Position.m_ptLeftHip = new Point(int.Parse(row[14].ToString()),
                                                       int.Parse(row[15].ToString()));
                Standing_Position.m_ptLeftKnee = new Point(int.Parse(row[16].ToString()),
                                                       int.Parse(row[17].ToString()));
                Standing_Position.m_ptLeftShoulder = new Point(int.Parse(row[18].ToString()),
                                                       int.Parse(row[19].ToString()));
                Standing_Position.m_ptRightAnkle = new Point(int.Parse(row[20].ToString()),
                                                       int.Parse(row[21].ToString()));
                Standing_Position.m_ptRightBelt = new Point(int.Parse(row[22].ToString()),
                                                       int.Parse(row[23].ToString()));
                Standing_Position.m_ptRightEar = new Point(int.Parse(row[24].ToString()),
                                                       int.Parse(row[25].ToString()));
                Standing_Position.m_ptRightHip = new Point(int.Parse(row[26].ToString()),
                                                       int.Parse(row[27].ToString()));
                Standing_Position.m_ptRightKnee = new Point(int.Parse(row[28].ToString()),
                                                       int.Parse(row[29].ToString()));
                Standing_Position.m_ptRightShoulder = new Point(int.Parse(row[30].ToString()),
                                                       int.Parse(row[31].ToString()));

                byte[] bytes = null;
                Image temp;
                bytes = Convert.FromBase64String(row["imagebytes"].ToString());
                MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_image =
                    new Image<Bgr, byte>(new Bitmap(temp));
                Emgu_image = Emgu_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.AllocStandingImage(Emgu_image.Bytes);
                m_JointEditDoc.SetStandingFrontBodyPosition(Standing_Position);

            }

        }
        private void LoadFrontBodyPositionKneedownValues(int uniqueKey)
        {
            string query = "select * from FrontBodyPositionKneedown where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                FrontBodyPosition Kneedown_Position = new FrontBodyPosition();
                if (bool.Parse(row[1].ToString()))
                    Kneedown_Position.SetKneePositionDetected();
                if (bool.Parse(row[2].ToString()))
                    Kneedown_Position.SetUnderBodYPositionDetected();
                if (bool.Parse(row[3].ToString()))
                    Kneedown_Position.SetUpperBodyPositionDetected();

                Kneedown_Position.m_ptChin = new Point(int.Parse(row[4].ToString()),
                                                       int.Parse(row[5].ToString()));
                Kneedown_Position.m_ptGlabella = new Point(int.Parse(row[6].ToString()),
                                                       int.Parse(row[7].ToString()));
                Kneedown_Position.m_ptLeftAnkle = new Point(int.Parse(row[8].ToString()),
                                                       int.Parse(row[9].ToString()));
                Kneedown_Position.m_ptLeftBelt = new Point(int.Parse(row[10].ToString()),
                                                       int.Parse(row[11].ToString()));
                Kneedown_Position.m_ptLeftEar = new Point(int.Parse(row[12].ToString()),
                                                       int.Parse(row[13].ToString()));
                Kneedown_Position.m_ptLeftHip = new Point(int.Parse(row[14].ToString()),
                                                       int.Parse(row[15].ToString()));
                Kneedown_Position.m_ptLeftKnee = new Point(int.Parse(row[16].ToString()),
                                                       int.Parse(row[17].ToString()));
                Kneedown_Position.m_ptLeftShoulder = new Point(int.Parse(row[18].ToString()),
                                                       int.Parse(row[19].ToString()));
                Kneedown_Position.m_ptRightAnkle = new Point(int.Parse(row[20].ToString()),
                                                       int.Parse(row[21].ToString()));
                Kneedown_Position.m_ptRightBelt = new Point(int.Parse(row[22].ToString()),
                                                       int.Parse(row[23].ToString()));
                Kneedown_Position.m_ptRightEar = new Point(int.Parse(row[24].ToString()),
                                                       int.Parse(row[25].ToString()));
                Kneedown_Position.m_ptRightHip = new Point(int.Parse(row[26].ToString()),
                                                       int.Parse(row[27].ToString()));
                Kneedown_Position.m_ptRightKnee = new Point(int.Parse(row[28].ToString()),
                                                       int.Parse(row[29].ToString()));
                Kneedown_Position.m_ptRightShoulder = new Point(int.Parse(row[30].ToString()),
                                                       int.Parse(row[31].ToString()));
                m_JointEditDoc.SetKneedownFrontBodyPosition(Kneedown_Position);

                byte[] bytes = null;
                Image temp;
                bytes = Convert.FromBase64String(row["imagebytes"].ToString());
                MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_image =
                    new Image<Bgr, byte>(new Bitmap(temp));
                Emgu_image = Emgu_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.AllocKneedownImage(Emgu_image.Bytes);


            }

        }
        private void LoadSideBodyPositionValues(int uniqueKey)
        {
            string query = "select * from SideBodyPosition where uniqueid =" + uniqueKey;
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);

            foreach (DataRow row in dt.Rows)
            {
                SideBodyPosition SideBody_Position = new SideBodyPosition();
                SideBody_Position.SetAnkleLeftBeltPosition(new Point(int.Parse(row[3].ToString()),
                                                       int.Parse(row[4].ToString())));
                SideBody_Position.SetAnklePosition(new Point(int.Parse(row[1].ToString()),
                                                       int.Parse(row[2].ToString())));
                SideBody_Position.SetAnkleRightBeltPosition(new Point(int.Parse(row[5].ToString()),
                                                       int.Parse(row[6].ToString())));
                SideBody_Position.SetBenchmark1Position(new Point(int.Parse(row[7].ToString()),
                                                       int.Parse(row[8].ToString())));
                SideBody_Position.SetBenchmark2Position(new Point(int.Parse(row[9].ToString()),
                                                       int.Parse(row[10].ToString())));
                SideBody_Position.SetChinPosition(new Point(int.Parse(row[11].ToString()),
                                                       int.Parse(row[12].ToString())));
                SideBody_Position.SetEarPosition(new Point(int.Parse(row[13].ToString()),
                                                       int.Parse(row[14].ToString())));
                SideBody_Position.SetGlabellaPosition(new Point(int.Parse(row[15].ToString()),
                                                       int.Parse(row[16].ToString())));
                SideBody_Position.SetHipPosition(new Point(int.Parse(row[17].ToString()),
                                                       int.Parse(row[18].ToString())));
                SideBody_Position.SetKneeLeftBeltPosition(new Point(int.Parse(row[21].ToString()),
                                                       int.Parse(row[22].ToString())));
                SideBody_Position.SetKneePosition(new Point(int.Parse(row[19].ToString()),
                                                       int.Parse(row[20].ToString())));
                SideBody_Position.SetKneeRightBeltPosition(new Point(int.Parse(row[23].ToString()),
                                                       int.Parse(row[24].ToString())));
                SideBody_Position.SetLeftBeltPosition(new Point(int.Parse(row[25].ToString()),
                                                       int.Parse(row[26].ToString())));
                SideBody_Position.SetRightBeltPosition(new Point(int.Parse(row[27].ToString()),
                                                       int.Parse(row[28].ToString())));
                SideBody_Position.SetShoulderPosition(new Point(int.Parse(row[29].ToString()),
                                                       int.Parse(row[30].ToString())));
                m_JointEditDoc.SetSideBodyPosition(SideBody_Position);

                byte[] bytes = null;
                Image temp;
                bytes = Convert.FromBase64String(row["imagebytes"].ToString());
                MemoryStream ms = new MemoryStream(bytes, 0, bytes.Length);
                ms.Write(bytes, 0, bytes.Length);
                temp = Image.FromStream(ms, true);

                Image<Bgr, byte> Emgu_image =
                    new Image<Bgr, byte>(new Bitmap(temp));
                Emgu_image = Emgu_image.Resize(1024, 1280,
                    Emgu.CV.CvEnum.Inter.Linear);
                m_JointEditDoc.AllocSideImage(Emgu_image.Bytes);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Added by Rajnish GSP-1164
            //textBox1.Text = "";
            //textBox2.Text = "";
            //checkBox1.Checked = false;
            //dateTimePicker1.Value = DateTime.Now;
            //dateTimePicker2.Value = DateTime.Now;
            //-------------------------//

            this.Visible = false;
            m_JointEditDoc.GetInitialScreen().Visible = true;
            m_JointEditDoc.GetInitialScreen().RefreshForms();
        }
        public void RefreshForms()
        {
            //LoadInstallationInfo();/*Added by Rajnish*/            
            //LoadAndSearchInstallationInfo("", "All");
            Reload_PCs(); //Added by rohini for GTL#340 
            this.CurrentPageIndex = 1; //Added by rohini for GTL#340
            #region added for JIRA # 395
            LoadDataGridWithDefaultValues();

            if (dataGridView1.RowCount > 0)
            {
                button1.Enabled = true;
            }
            else//--Else Condition Added by Rajnish for GTL#58-----START
            {
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
            //--Added by Rajnish for GTL#58-------------------------END
            #endregion

            //Added by Rajnish GSP-1291 (No Relevant JIRA TASK)  ------START
            FirstPage.Enabled = true;
            //Added by Rajnish GSP-1291 -------END
            search = false;   //added by rohini for GSP-1381
            textBox1.Focus(); //added by rohini for GTL#50 
            SearchById_Flag = false;
            SearchByIdAndName_Flag = false;
            SearchByName_Flag = false;
            searchbysearch = false; //added by rohini for GTL#248
            //Added by Rajnish GSP-1164----START---//
            textBox1.Text = "";
            textBox2.Text = "";
            //Edited (Moved to function) by Sumit for GTL#458   ---START
            //dateTimePicker1.ValueChanged -= new EventHandler(dateTimePicker1_ValueChanged);
            //dateTimePicker2.ValueChanged -= new EventHandler(dateTimePicker2_ValueChanged);            
            //dateTimePicker1.Value = DateTime.Now;
            //dateTimePicker2.Value = DateTime.Now;            
            //dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
            //dateTimePicker2.ValueChanged += new EventHandler(dateTimePicker2_ValueChanged);
            SetDatePickers();
            //Edited by Sumit for GTL#458   ---END
            FirstPage.PerformClick(); //--Line added by Rajnish for GSP-1232--//
            //-------------------------END-------//
            checkedListSearchControl1.InnerCb_CheckedChanged += new System.EventHandler(checkedListSearchControl1_Click);//Added By Rajnish for GSP-1488

            checkBox1.Checked = false;
            dateTimePicker1.Enabled = false;
            dateTimePicker2.Enabled = false;
            btnGetLatest.Enabled = false;//Added by Rajnish for GSP-1488

            m_JointEditDoc.GetMainScreen().RefreshMenuStrip(false);
            label2.Text = Yugamiru.Properties.Resources.OPENRESULT_NAME;
            checkBox1.Text = Yugamiru.Properties.Resources.OPENRESULT_DOC;
            label4.Text = Yugamiru.Properties.Resources.OPENRESULT_FD;
            label5.Text = Yugamiru.Properties.Resources.OPENRESULT_TD;
            IDC_Search.Text = Yugamiru.Properties.Resources.OPENRESULT_SEARCH;
            button2.Text = Yugamiru.Properties.Resources.OPENRESULT_CANCEL;
            FirstPage.Text = Yugamiru.Properties.Resources.OPENRESULT_FIRST;
            NextPage.Text = Yugamiru.Properties.Resources.OPENRESULT_NEXT;
            PreviousPage.Text = Yugamiru.Properties.Resources.OPENRESULT_PREVIOUS;
            LastPage.Text = Yugamiru.Properties.Resources.OPENRESULT_LAST;
            button1.Text = Yugamiru.Properties.Resources.OPENRESULT_SUBMIT;
            //Added by rohini for GTL#294 ---- START
            btnGetLatest.Text = Yugamiru.Properties.Resources.GET_LATEST;
            groupBox1.Text = Yugamiru.Properties.Resources.FILTER_BY_DATE;
            groupBox9.Text = Yugamiru.Properties.Resources.FILTER_BY_PC_NAME;

            //Added by rohini for GTL#294 --- END
            //Edited by Sumit GTL#251   ---START
            //Reload_PCs(); //commented by rohini for GTL#340
            //Edited by Sumit GTL#251   ---END
            IDC_Search.PerformClick();
            //Added by Sumit for GTL#254   ---START
            label3.Left = dataGridView1.Left + dataGridView1.Width - label3.Width + label3.Margin.Right;

            //label3.BackColor = Color.Green;
            //Added by Sumit for GTL#254   ---END

        }

        private void IDC_Search_Click(object sender, EventArgs e)
        {
            searchbysearch = true;  //added by rohini for GTL#248
            //string query = "select uniqueid from patientdetails ";
            string query = "select uniqueid from patientdetails inner join tblInstallInfo on tblInstallInfo.reg_user_id = tblInstallInfo.reg_user_id and tblInstallInfo.stall_id = PatientDetails.Stall_ID where Sync != 'FALSE' ";
            if (textBox1.Text != "") // search by ID
            {
                //query = query + "where patientid like '%" + textBox1.Text + "%'";
                query = query + " and patientid like '%" + textBox1.Text + "%'";
                SearchById_Flag = true;
            }
            if (textBox1.Text != "" && textBox2.Text != "") // search by ID and name
            {
                query = query + " and name like '%" + textBox2.Text + "%'";
                SearchByIdAndName_Flag = true;
            }
            if (textBox1.Text == "" && textBox2.Text != "") // search by name
            {
                query = query + " and name like '%" + textBox2.Text + "%'";
                SearchByName_Flag = true;
            }

            if (checkBox1.Checked == true) // search by date of creation
            {
                if (!SearchById_Flag && !SearchByIdAndName_Flag && !SearchByName_Flag)
                {
                    query = query + " and MeasurementTime between '" +
                        dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                        dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";

                }
                //else
                //{
                //    query = query + " and MeasurementTime between '" +
                //        dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                //        dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                //}

            }

            DataTable dtPcSelected = new DataTable();
            dtPcSelected = checkedListSearchControl1.GetSelectedData();
            if (dtPcSelected.Rows.Count > 0)
            {
                string str = string.Empty;
                foreach (DataRow dr in dtPcSelected.Rows)
                {
                    str += dr["ID"] + ",";
                }
                str = str.Remove(str.Length - 1, 1);
                query = query + " and PatientDetails.stall_id IN (" + str + ")";
            }
            else
            {
                searchbysearch = true;  //added by rohini for GTL#347
            }

            CalculateTotalPages(query, false);
            //Edited by Sumit GSP-1117-------START
            //if (this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            this.CurrentPageIndex = 1; //Added by Sumit GTL#84
            if (this.TotalPage == 0)
            {
                label3.Text = Properties.Resources.SHOWING + 0 + " - " + 0 + " of " + 0 + " " + Properties.Resources.RECORDS; //added by rohini for GTL#248
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            }
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            //query = query + " order by Measurementtime desc limit 20";
            SearchById_Flag = false; SearchByIdAndName_Flag = false; SearchByName_Flag = false;
            search = false; //added by rohini for GSP-1381
            //this.CurrentPageIndex = 1; Commented by Sumit GTL#84
            query = string.Empty;
            query = GetCurrentQuery(this.CurrentPageIndex);

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;

        }
        DataTable Records_dt = new DataTable();
        public void LoadDataGridWithDefaultValues()
        {
            //CalculateTotalPages("select patientid,name,MeasurementTime,uniqueid from patientdetails order by Measurementtime", true);
            // CalculateTotalPages("select patientid,name,MeasurementTime,uniqueid, tblInstallInfo.Computer_name, tblInstallInfo.Installation_name from patientdetails inner join tblInstallInfo On tblInstallInfo.reg_user_id = PatientDetails.reg_user_id and tblInstallInfo.stall_id = PatientDetails.Stall_ID where Sync != 'FALSE' order by Measurementtime", true); //commented by rohini for 341
            CalculateTotalPages("select patientid,name,MeasurementTime,uniqueid,PatientDetails.Stall_ID,tblInstallInfo.Computer_name, tblInstallInfo.Installation_name from patientdetails inner join tblInstallInfo On tblInstallInfo.reg_user_id = PatientDetails.reg_user_id and tblInstallInfo.stall_id = PatientDetails.Stall_ID where Sync != 'FALSE' order by Measurementtime", true); //added by rohini for 341

            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            if (this.TotalPage == 0)
            // added by rohini for GSP-1381 -- START
            {
                label3.Text = Properties.Resources.SHOWING + 0 + " - " + 0 + " of " + 0 + " " + Properties.Resources.RECORDS;
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            }
            // added by rohini for GSP-1381 -- END
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            string query = GetCurrentQuery(this.CurrentPageIndex);//"select patientid,name,MeasurementTime, uniqueid from patientdetails order by Measurementtime desc";

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            //--Added by Rajnish for GSP-700--//
            foreach (DataRow dr in dt.Rows)
            {
                dr["name"] = dr["name"].ToString().Replace("\"\"", "\"");
                dr["patientid"] = dr["patientid"].ToString().Replace("\"\"", "\"");
            }
            //--------------------------------//
            rowCount = Records_dt.Rows.Count;// added by rohini for  GSP-1381
            dataGridView1.DataSource = dt;


            dataGridView1.EnableHeadersVisualStyles = false;

            //Edited by Rohini due to GSP-1381   ---START
            #region old_commented
            //dataGridView1.Columns[0].HeaderText = "SrNo.";
            //dataGridView1.Columns[0].Width = 50;

            //dataGridView1.Columns[0].HeaderText = "Id";
            //dataGridView1.Columns[0].Width = 100;

            ////Edited Sumit GSP-1117-----START
            ////dataGridView1.Columns[1].HeaderText = "Name";
            //dataGridView1.Columns[1].HeaderText = Properties.Resources.OPEN_RESULT_NAME_COLUMN;
            ////Edited Sumit GSP-1117-----END
            //dataGridView1.Columns[1].Width = 200;

            ////Edited Sumit GSP-1117-----START
            ////dataGridView1.Columns[2].HeaderText = "Date Of Creation";
            //dataGridView1.Columns[2].HeaderText = Properties.Resources.OPEN_RESULT_DATE_OF_CREATION_COLUMN;
            ////Edited Sumit GSP-1117-----END
            //dataGridView1.Columns[2].Width = 150;

            //dataGridView1.Columns[3].HeaderText = "uniqueid";
            //dataGridView1.Columns[3].Width = 1;
            //dataGridView1.Columns[3].Visible = false;

            //dataGridView1.Columns[4].HeaderText = "PC Name";
            //dataGridView1.Columns[4].Width = 163;

            //dataGridView1.Columns[5].HeaderText = "Installation Name";
            //dataGridView1.Columns[5].Width = 181;
            #endregion
            dataGridView1.Columns[0].HeaderText = "SrNo.";
            dataGridView1.Columns[0].Width = 50;

            dataGridView1.Columns[1].HeaderText = "Id";
            dataGridView1.Columns[1].Width = 100;

            //Edited Sumit GSP-1117-----START
            //dataGridView1.Columns[1].HeaderText = "Name";
            dataGridView1.Columns[2].HeaderText = Properties.Resources.OPEN_RESULT_NAME_COLUMN;
            //Edited Sumit GSP-1117-----END
            dataGridView1.Columns[2].Width = 200;

            //Edited Sumit GSP-1117-----START
            //dataGridView1.Columns[2].HeaderText = "Date Of Creation";
            dataGridView1.Columns[3].HeaderText = Properties.Resources.OPEN_RESULT_DATE_OF_CREATION_COLUMN;
            //Edited Sumit GSP-1117-----END
            dataGridView1.Columns[3].Width = 150;

            // dataGridView1.Columns[4].HeaderText = "uniqueid"; //commented by rohini for GTL#294
            dataGridView1.Columns[4].HeaderText = Properties.Resources.UNIQUE_ID; //Added by rohini for GTL#294
            dataGridView1.Columns[4].Width = 1;
            dataGridView1.Columns[4].Visible = false;

            //dataGridView1.Columns[5].HeaderText = "PC Name";  //commented by rohini for GTL#294
            dataGridView1.Columns[5].HeaderText = Properties.Resources.PC_NAME; //Added by rohini for GTL#294
            dataGridView1.Columns[5].Width = 163;

            // dataGridView1.Columns[6].HeaderText = "Installation Name"; //commented by rohini for GTL#294
            dataGridView1.Columns[6].HeaderText = Properties.Resources.INSTALLATION_NAME; //Added by rohini for GTL#294
            dataGridView1.Columns[6].Width = 181;
            //Edited by Rohini due to GSP-1381   ---END
            //dataGridView1.Columns[3].Visible = true;

            foreach (DataGridViewColumn col in dataGridView1.Columns)
            {
                col.HeaderCell.Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
                col.HeaderCell.Style.Font = new Font("Arial", 14F, FontStyle.Bold, GraphicsUnit.Pixel);
                col.HeaderCell.Style.BackColor = Color.LightSteelBlue;

                col.DefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;

            }

            // dataGridView1.ColumnHeadersDefaultCellStyle.BackColor = Color.LightBlue;
            dataGridView1.RowsDefaultCellStyle.BackColor = Color.AliceBlue;//Color.LightYellow;
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.White;//Color.Beige;
            dataGridView1.DefaultCellStyle.Font = new Font("Arial", 12F, FontStyle.Regular, GraphicsUnit.Pixel);
            dataGridView1.CellBorderStyle = DataGridViewCellBorderStyle.None;

            dataGridView1.DefaultCellStyle.SelectionBackColor = Color.LightSkyBlue;
            dataGridView1.DefaultCellStyle.SelectionForeColor = Color.DarkBlue;

            dataGridView1.DefaultCellStyle.WrapMode = DataGridViewTriState.True;
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
            dataGridView1.AllowUserToResizeColumns = false;
            dataGridView1.AllowUserToResizeRows = false;
            if (dataGridView1.RowCount > 0)
                dataGridView1.Rows[0].Selected = true;
            //--Added by Rajnish for YV0R-11--//
            if (TotalPage == 1)
            {
                FirstPage.Enabled = false;
                PreviousPage.Enabled = false;
                textbox_PageNo.Enabled = false;
                NextPage.Enabled = false;
                LastPage.Enabled = false;
            }
            //---------------------------------//


        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            //Added by Sumit for GTL#458   ---START
            if (sender is DateTimePicker && ((DateTimePicker)(sender)).Name == "dateTimePicker1")
            {
                try
                {
                    dateTimePicker2.ValueChanged -= new EventHandler(dateTimePicker2_ValueChanged);
                    dateTimePicker2.MinDate = dateTimePicker1.Value;
                    //dateTimePicker2.Value = dateTimePicker1.Value;
                    dateTimePicker2.ValueChanged += new EventHandler(dateTimePicker2_ValueChanged);
                }
                catch (Exception ex)
                {

#if debug
                    MessageBox.Show(ex.StackTrace);
#endif
                }
            }
            //Added by Sumit for GTL#458   ---END

            search = true; // added by rohini for  GSP-1381
            DataView view = new DataView(Records_dt);
            string str = string.Empty;
            if (textBox1.Text != "")
                str = "patientid like '%" + textBox1.Text + "%' and ";
            if (textBox2.Text != "")
                str = str + "name Like '%" + textBox2.Text + "%' and ";

            view.RowFilter = str + "MeasurementTime >= '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and MeasurementTime <= '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";


            view.Sort = "Measurementtime DESC";

            //added by rohini for GTL#341 ---- START

            string str1 = string.Empty;
            DataTable dtPcSelected = new DataTable();
            dtPcSelected = checkedListSearchControl1.GetSelectedData();
            if (dtPcSelected.Rows.Count > 0)
            {

                foreach (DataRow dr in dtPcSelected.Rows)
                {
                    str1 += dr["ID"] + ",";
                }
                str1 = str1.Remove(str1.Length - 1, 1);

                view.RowFilter = str + "MeasurementTime >= '" +
                        dateTimePicker1.Value.ToString("yy-MM-dd") + "' and MeasurementTime <= '" +
                          dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "' and stall_id IN (" + str1 + ")";

                view.Sort = "Measurementtime DESC";
            }


            //added by rohini for GTL#341 ---- END

            DataView Test_View = GetTopDataViewRows(view, 21);

            dataGridView1.DataSource = Test_View;

            //edited by rohini for 1381   ---START
            //int rowCount = 0;
            rowCount = 0;
            //edited by rohini for 1381   ---END

            rowCount = view.Count;
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " " + Properties.Resources.RECORDS;// added by rohini for  GSP-1381
            TotalPage = rowCount / PgSize;
            // if any row left after calculated pages, add one more page 
            if (rowCount % PgSize > 0)
                TotalPage += 1;

            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            if (this.TotalPage == 0)
            {
                label3.Text = Properties.Resources.SHOWING + 0 + " - " + 0 + " of " + 0 + " " + Properties.Resources.RECORDS; // added by rohini for GSP-1381
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            }
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END

            //--Added by Rajnish for GSP-1235--start//
            if (TotalPage > 1)
            {
                //FirstPage.Enabled = true;
                //PreviousPage.Enabled = true;
                textbox_PageNo.Enabled = true;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            else
            {
                if (TotalPage == 1)
                {
                    FirstPage.Enabled = false;
                    PreviousPage.Enabled = false;
                    textbox_PageNo.Enabled = false;
                    NextPage.Enabled = false;
                    LastPage.Enabled = false;
                }
            }
            //--Added by Rajnish for GSP-1235--END//

            //Added by rohini for GTL#244 -- START
            if (checkBox1.Checked == false)
            {
                CurrentPageIndex = 1;
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + CurrentPageIndex + @" / " + this.TotalPage;
                FirstPage.Enabled = false;
                PreviousPage.Enabled = false;
                textbox_PageNo.Enabled = false;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            //Added by rohini for GTL#244 -- END
            //Added by rohini for GTL#244 -- START
            if (checkBox1.Checked == true)
            {
                CurrentPageIndex = 1;
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + CurrentPageIndex + @" / " + this.TotalPage;
                FirstPage.Enabled = false;
                PreviousPage.Enabled = false;
                textbox_PageNo.Enabled = false;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            //Added by rohini for GTL#244 -- END
        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {
            dateTimePicker1_ValueChanged(sender, e);

        }

        private void dateTimePicker2_CloseUp(object sender, EventArgs e)
        {

            DateTime fromdate = DateTime.Parse(dateTimePicker1.Value.ToString());
            DateTime todate1 = DateTime.Parse(dateTimePicker2.Value.ToString());
            if (fromdate <= todate1)
            {
                TimeSpan daycount = todate1.Subtract(fromdate);
                int dacount1 = Convert.ToInt32(daycount.Days) + 1;
                //MessageBox.Show(Convert.ToString(dacount1));
            }
            else
            {
                MessageBox.Show(Yugamiru.Properties.Resources.FROM_TO_DATE_VALIDATION, "Yugamiru");
            }
        }

        private void checkBox1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked == true)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker2.Enabled = true;
            }
            else
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker2.Enabled = false;

            }

        }

        private void dateTimePicker2_FormatChanged(object sender, EventArgs e)
        {

        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {



            //Edited by Sumit for GTL#458   ---START
            SetDatePickers();
            //Edited by Sumit for GTL#458   ---END

            if (checkBox1.Checked == false)
            {
                DataView view = new DataView(Records_dt);
                string str = string.Empty;
                if (textBox1.Text != "")
                {
                    str = "patientid like '" + textBox1.Text + "%' ";
                    if (textBox2.Text != "")
                        str = str + "and ";
                }

                if (textBox2.Text != "")
                    str = str + "name Like '" + textBox2.Text + "%'";

                view.RowFilter = str;
                view.Sort = "Measurementtime DESC";

                //added by rohini for GTL#341 ---- START
                string str1 = string.Empty;
                DataTable dtPcSelected = new DataTable();
                dtPcSelected = checkedListSearchControl1.GetSelectedData();
                if (dtPcSelected.Rows.Count > 0)
                {

                    foreach (DataRow dr in dtPcSelected.Rows)
                    {
                        str1 += dr["ID"] + ",";
                    }
                    str1 = str1.Remove(str1.Length - 1, 1);

                    view.RowFilter = "stall_id IN (" + str1 + ")";
                    view.Sort = "Measurementtime DESC";
                }

                //added by rohini for GTL#341 ---- END

                DataView Test_View = GetTopDataViewRows(view, 21);

                dataGridView1.DataSource = Test_View;

                int rowCount = 0;
                rowCount = view.Count;

                TotalPage = rowCount / PgSize;
                // if any row left after calculated pages, add one more page 
                if (rowCount % PgSize > 0)
                    TotalPage += 1;

                //Edited by Sumit GSP-1117-------START
                //if(this.TotalPage == 0)
                //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
                //else
                //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
                //query = query + " order by Measurementtime desc limit 20";
                if (this.TotalPage == 0)
                    textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
                else
                    textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
                //GSP-1117----------END

                //--Added by Rajnish for GSP-1235--start//
                if (TotalPage > 1)
                {
                    //FirstPage.Enabled = true;
                    //PreviousPage.Enabled = true;
                    textbox_PageNo.Enabled = true;
                    NextPage.Enabled = true;
                    LastPage.Enabled = true;
                }
                else
                {
                    if (TotalPage == 1)
                    {
                        FirstPage.Enabled = false;
                        PreviousPage.Enabled = false;
                        textbox_PageNo.Enabled = false;
                        NextPage.Enabled = false;
                        LastPage.Enabled = false;
                    }
                }
                //--Added by Rajnish for GSP-1235--END//

            }
            else
            {
                //Commented by sumit for GTL#458   ---START
                //dateTimePicker1_ValueChanged(sender, e);
                //Commented by sumit for GTL#458   ---END
            }

        }

        public void OpenResult_SizeChanged(object sender, EventArgs e)
        {
            //dataGridView1.Width = 800; //Commented by rohini for GTL#254
            dataGridView1.Width = 840; // Added by rohini for GTL#254
            dataGridView1.Height = 484;
            panel1.Left = 0;
            if (this.Width < (panel1.Left + panel1.Width + 20 + 800))
                dataGridView1.Width = 600;
            panel1.Left = (this.Width - (panel1.Width + dataGridView1.Width + 20)) / 2;
            if (panel1.Left < 0)
                panel1.Left = 0;
            panel1.Top = (this.Height - (dataGridView1.Height + 20 + panel2.Height)) / 2;
            IDC_Search.Top = panel1.Top + panel1.Height + 20;
            IDC_Search.Left = panel1.Left + (panel1.Width - IDC_Search.Width);
            button2.Left = panel1.Left;
            button2.Top = panel1.Top + panel1.Height + 20;
            btnGetLatest.Top = panel1.Top + panel1.Height + 20;
            btnGetLatest.Left = panel1.Left + (panel1.Width - IDC_Search.Width - button2.Width - 50);


            dataGridView1.Left = panel1.Left + panel1.Width + 20;
            dataGridView1.Top = panel1.Top;
            panel2.Left = (dataGridView1.Width - panel2.Width) / 2 + dataGridView1.Left;
            panel2.Top = dataGridView1.Top + dataGridView1.Height + 20;
            //  label3.Left = panel1.Right + 620; // added by rohini for GSP-1381 //Commented by rohini for GTL#256
            label3.Top = panel1.Top - 20; // added by rohini for GSP-1381
            //label3.Left = panel1.Right + 580; // added by rohini for GTL#254



            //Added by sumit for GTL#249   ---START
            //Added by Sumit GTL#229   ---START
            //checkedListSearchControl1.FillData(PCNameList());
            //checkedListSearchControl1 = new CustomControls.CheckedListSearchControl();
            ////Edited by Sumit GTL#251   ---START
            ////checkedListSearchControl1.FillData(PCNameList());
            //DataTable dtPCNameList = PCNameList();
            //if (dtPCNameList == null || dtPCNameList.Rows.Count == 0)
            //{
            //    checkedListSearchControl1.Enabled = false;
            //}
            //else
            //{
            //    checkedListSearchControl1.FillData(dtPCNameList);
            //    checkedListSearchControl1.Enabled = true;
            //}
            //Edited by Sumit GTL#251   ---END
            //Added by Sumit GTL#229   ---END

            //Added by sumit for GTL#249   ---END
            //Added by Sumit for GTL#254   ---START
            label3.Left = dataGridView1.Left + dataGridView1.Width - label3.Width;
            //label3.BackColor = Color.Green;
            //Added by Sumit for GTL#254   ---END
        }

        private void CalculateTotalPages(string query, bool CopyTotalRecord)
        {
            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            if (CopyTotalRecord)
                Records_dt = dt;

            int rowCount = 0;
            rowCount = dt.Rows.Count;
            totalbysearch = rowCount;  // added by rohini for GTL#181
            TotalPage = rowCount / PgSize;
            // if any row left after calculated pages, add one more page 
            if (rowCount % PgSize > 0)
                TotalPage += 1;
            //--Added by Rajnish for YV0R-11--//
            if (TotalPage > 1)
            {
                //FirstPage.Enabled = true;
                //PreviousPage.Enabled = true;
                textbox_PageNo.Enabled = true;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            else
            {
                if (TotalPage == 1)
                {
                    FirstPage.Enabled = false;
                    PreviousPage.Enabled = false;
                    textbox_PageNo.Enabled = false;
                    NextPage.Enabled = false;
                    LastPage.Enabled = false;
                }
            }
            //---------------------------------//
        }
        private string GetCurrentQuery(int page)
        {
            string query = string.Empty;

            if (page == 1)
            {
                //query = "select patientid,name,MeasurementTime,uniqueid from patientdetails ";
                query = "select patientid,name,MeasurementTime,uniqueid, tblInstallInfo.Computer_name, tblInstallInfo.Installation_name from patientdetails inner join tblInstallInfo On tblInstallInfo.reg_user_id = PatientDetails.reg_user_id and tblInstallInfo.stall_id = PatientDetails.Stall_ID where Sync != 'FALSE' ";

                if (textBox1.Text != "") // search by ID
                {
                    query = query + "and patientid like '%" + textBox1.Text + "%'";
                    SearchById_Flag = true;
                }
                if (textBox1.Text != "" && textBox2.Text != "") // search by ID and name
                {
                    query = query + " and name like '%" + textBox2.Text + "%'";
                    SearchByIdAndName_Flag = true;
                }
                if (textBox1.Text == "" && textBox2.Text != "") // search by name
                {
                    query = query + " and name like '%" + textBox2.Text + "%'";
                    SearchByName_Flag = true;
                }

                if (checkBox1.Checked == true) // search by date of creation
                {
                    if (!SearchById_Flag && !SearchByIdAndName_Flag && !SearchByName_Flag)
                    {
                        query = query + " and MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";

                    }
                    else
                    {
                        query = query + " and MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                    }

                }
                DataTable dtPcSelected = new DataTable();
                dtPcSelected = checkedListSearchControl1.GetSelectedData();
                if (dtPcSelected.Rows.Count > 0)
                {
                    string str = string.Empty;
                    foreach (DataRow dr in dtPcSelected.Rows)
                    {
                        str += dr["ID"] + ",";
                    }
                    str = str.Remove(str.Length - 1, 1);
                    query = query + " and PatientDetails.stall_id IN (" + str + ")";
                }
                query = query + " order by Measurementtime desc LIMIT " + PgSize;
                SearchById_Flag = false; SearchByIdAndName_Flag = false; SearchByName_Flag = false;
                search = false;   //added by rohini for GSP-1381
            }
            else
            {
                int PreviousPageOffSet = (page - 1) * PgSize;

                query = "Select" +
                    " patientid,name,MeasurementTime, uniqueid, tblInstallInfo.Computer_name, tblInstallInfo.Installation_name from" +
                    " patientdetails inner join tblInstallInfo On tblInstallInfo.reg_user_id = PatientDetails.reg_user_id and tblInstallInfo.stall_id = PatientDetails.Stall_ID where Sync != 'FALSE' AND uniqueid NOT IN" +
                    " (Select uniqueid from patientdetails inner join tblInstallInfo on tblInstallInfo.reg_user_id = tblInstallInfo.reg_user_id and tblInstallInfo.stall_id = PatientDetails.Stall_ID where Sync != 'FALSE' ";
                if (textBox1.Text != "") // search by ID
                {
                    query = query + "and patientid like '%" + textBox1.Text + "%'";
                    SearchById_Flag = true;
                }
                if (textBox1.Text != "" && textBox2.Text != "") // search by ID and name
                {
                    query = query + " and name like '%" + textBox2.Text + "%'";
                    SearchByIdAndName_Flag = true;
                }
                if (textBox1.Text == "" && textBox2.Text != "") // search by name
                {
                    query = query + " and name like '%" + textBox2.Text + "%'";
                    SearchByName_Flag = true;
                }

                if (checkBox1.Checked == true) // search by date of creation
                {
                    if (!SearchById_Flag && !SearchByIdAndName_Flag && !SearchByName_Flag)
                    {
                        query = query + " and MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                    }
                    else
                    {
                        query = query + " and MeasurementTime between '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                    }

                }
                DataTable dtPcSelected = new DataTable();
                dtPcSelected = checkedListSearchControl1.GetSelectedData();
                if (dtPcSelected.Rows.Count > 0)
                {
                    string str = string.Empty;
                    foreach (DataRow dr in dtPcSelected.Rows)
                    {
                        str += dr["ID"] + ",";
                    }
                    str = str.Remove(str.Length - 1, 1);
                    query = query + " and PatientDetails.stall_id IN (" + str + ")";
                }
                query = query + " order by Measurementtime desc LIMIT " + PreviousPageOffSet + ")";
                SearchById_Flag = false; SearchByIdAndName_Flag = false; SearchByName_Flag = false;
                search = false;   //added by rohini for GSP-1381
                if (textBox1.Text != "") // search by ID
                {
                    query = query + " and patientid like '%" + textBox1.Text + "%'";
                }
                if (textBox2.Text != "") // search by name
                {
                    query = query + " and name like '%" + textBox2.Text + "%'";
                }
                if (checkBox1.Checked == true) // search by date of creation
                {
                    query = query + " and MeasurementTime between '" +
                    dateTimePicker1.Value.ToString("yy-MM-dd") + "' and '" +
                    dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "'";
                }
                dtPcSelected = new DataTable();
                dtPcSelected = checkedListSearchControl1.GetSelectedData();
                if (dtPcSelected.Rows.Count > 0)
                {
                    string str = string.Empty;
                    foreach (DataRow dr in dtPcSelected.Rows)
                    {
                        str += dr["ID"] + ",";
                    }
                    str = str.Remove(str.Length - 1, 1);
                    query = query + " and PatientDetails.stall_id IN (" + str + ")";
                }
                query = query + " order by Measurementtime desc LIMIT " + PgSize;
            }

            return query;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void FirstPage_Click(object sender, EventArgs e)
        {
            this.CurrentPageIndex = 1;
            string query = GetCurrentQuery(this.CurrentPageIndex);

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;
            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            if (this.TotalPage == 0)
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            //--Added by Rajnish for GSP-1235 --//
            FirstPage.Enabled = false;
            PreviousPage.Enabled = false;
            if (TotalPage > 1)
            {
                LastPage.Enabled = true;
                NextPage.Enabled = true;
            }
            //----------------------------------//
        }

        private void NextPage_Click(object sender, EventArgs e)
        {

            if (this.CurrentPageIndex < this.TotalPage)
            {
                this.CurrentPageIndex++;
                string query = GetCurrentQuery(this.CurrentPageIndex);

                CDatabase database = new CDatabase();
                DataTable dt = new DataTable();
                dt = database.selectQuery(query);
                dataGridView1.DataSource = dt;
            }
            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            // added by rohini for  GTL#347---START
            if ((checkBox1.Checked == false) && ((textBox1.Text == "") && (textBox2.Text == "")))
            {
                rowCount = Records_dt.Rows.Count;
            }
            // added by rohini for  GTL#347 --END
            // added by rohini for GTL#181 -- START
            if (rowCount <= count2)
            {
                rowCount = count2;
            }
            else
            {
                //added by rohini for GTL#347 ---START

                DataTable dtPcSelected = new DataTable();
                dtPcSelected = checkedListSearchControl1.GetSelectedData();
                if (dtPcSelected.Rows.Count > 0)
                {
                    rowCount = totalbysearch;
                }
                //added by rohini for GTL#347 ---END
            }
            //label3.Text = " Showing  " + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " records"; // added by rohini for  GSP-1381
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " " + Properties.Resources.RECORDS;
            // added by rohini for GTL#181 -- END

            if (this.TotalPage == 0)
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            //--Added by Rajnish for GSP-1235 --//
            FirstPage.Enabled = true;
            PreviousPage.Enabled = true;

            if (CurrentPageIndex == TotalPage)
            {
                LastPage.Enabled = false;
                NextPage.Enabled = false;
            }
            else
            {
                LastPage.Enabled = true;
                NextPage.Enabled = true;
            }
            //----------------------------------//
        }

        private void PreviousPage_Click(object sender, EventArgs e)
        {
            if (this.CurrentPageIndex > 1)
            {
                this.CurrentPageIndex--;
                string query = GetCurrentQuery(this.CurrentPageIndex);

                CDatabase database = new CDatabase();
                DataTable dt = new DataTable();
                dt = database.selectQuery(query);
                dataGridView1.DataSource = dt;
            }
            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " " + Properties.Resources.RECORDS; // added by rohini for  GSP-1381
            if (this.TotalPage == 0)
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            //--Added by Rajnish for GSP-1235 --//
            NextPage.Enabled = true;
            LastPage.Enabled = true;

            if (CurrentPageIndex == 1)
            {
                FirstPage.Enabled = false;
                PreviousPage.Enabled = false;
            }
            else
            {
                FirstPage.Enabled = true;
                PreviousPage.Enabled = true;
            }
            //----------------------------------//
        }

        private void LastPage_Click(object sender, EventArgs e)
        {
            this.CurrentPageIndex = TotalPage;
            string query = GetCurrentQuery(this.CurrentPageIndex);

            CDatabase database = new CDatabase();
            DataTable dt = new DataTable();
            dt = database.selectQuery(query);
            dataGridView1.DataSource = dt;
            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            if (this.TotalPage == 0)
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            //--Added by Rajnish for GSP-1235 --Start//
            LastPage.Enabled = false;
            NextPage.Enabled = false;

            if (TotalPage > 1)
            {
                FirstPage.Enabled = true;
                PreviousPage.Enabled = true;
            }
            //----------------------------------END//
        }

        private void OpenResult_Load(object sender, EventArgs e)
        {
            checkedListSearchControl1.InnerCb_CheckedChanged += new System.EventHandler(checkedListSearchControl1_Click);//Added By Rajnish for GSP-1488

            //Added by Sumit for GTL#254   ---START
            label3.Left = dataGridView1.Left + dataGridView1.Width - label3.Width + label3.Margin.Right;

            //label3.BackColor = Color.Green;
            //Added by Sumit for GTL#254   ---END
        }

        private void dataGridView1_DoubleClick(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;//--Line added by Rajnish for GSP-1230 --//
            if (me.Button == MouseButtons.Left)//--Condition added by Rajnish for GSP-1230 --//
            {
                // if (dataGridView1.Rows.Count > 0) Commented by rohini for GSP-1393 
                if ((dataGridView1.Rows.Count > 0) && (index != -1))  //edited by rohini for GSP-1393
                {
                    var selectedRow = dataGridView1.SelectedRows[0];
                    //Edited by Sumit due to error GSP-1381   ---START
                    //var primaryKey = int.Parse(selectedRow.Cells[3].Value.ToString());
                    var primaryKey = int.Parse(selectedRow.Cells[4].Value.ToString());
                    //Edited by Sumit due to error GSP-1381   ---END
                    m_JointEditDoc.SetUniqueId(primaryKey);

                    LoadPatientDetails(primaryKey);
                    LoadFrontBodyPositionStandingValues(primaryKey);
                    LoadFrontBodyPositionKneedownValues(primaryKey);
                    LoadSideBodyPositionValues(primaryKey);
                    this.Visible = false;
                    //ResultView.backbutton = false; //added by rohini for GSP-1179	
                    ResultView.editcoment = false;//added by rohini for GSP-1478
                    ResultView.backbutton = true;//Added by rohini for GSP-1459
                    ResultView.oldrecord = true; //added by rohini for GSP-1308
                    FunctionToChangeResultView(EventArgs.Empty);
                    //Added by Sumit for GSP-1000 START
                    if (CloudManager.GlobalItems.IsOtherPCData(primaryKey.ToString()))
                    {
                        ResultView.isAnForeignRecord = true;
                    }
                    else
                    {
                        ResultView.isAnForeignRecord = false;
                    }
                    //Added by Sumit for GSP-1000 END
                }
            }

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            if (panel1.BorderStyle == BorderStyle.FixedSingle)
            {
                int thickness = 3;//it's up to you
                int halfThickness = thickness / 2;
                using (Pen p = new Pen(Color.AliceBlue, thickness))
                {
                    e.Graphics.DrawRectangle(p, new Rectangle(halfThickness,
                                                              halfThickness,
                                                              panel1.ClientSize.Width - thickness,
                                                              panel1.ClientSize.Height - thickness));
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            //--Added By Rajnish for GTL #442---Start--//
            var textboxSender = (TextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #442---Start--//
            search = true; //added by rohini for  GSP-1381
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " " + Properties.Resources.RECORDS; // added by rohini for  GSP-1381
            DataView view = new DataView(Records_dt);
            string str = string.Empty;
            if (textBox2.Text != "")
                str = "name Like '%" + textBox2.Text + "%' and ";

            if (checkBox1.Checked == true)
            {
                str = str + "MeasurementTime >= '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and MeasurementTime <= '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "' and ";
            }

            view.RowFilter = str + "PatientId Like '%" + textBox1.Text + "%'";
            view.Sort = "Measurementtime DESC";

            //added by rohini for GTL#341 ---- START
            string str1 = string.Empty;
            DataTable dtPcSelected = new DataTable();
            dtPcSelected = checkedListSearchControl1.GetSelectedData();
            if (dtPcSelected.Rows.Count > 0)
            {

                foreach (DataRow dr in dtPcSelected.Rows)
                {
                    str1 += dr["ID"] + ",";
                }
                str1 = str1.Remove(str1.Length - 1, 1);

                view.RowFilter = "PatientId Like '%" + textBox1.Text + "%' and stall_id IN (" + str1 + ")";
                view.Sort = "Measurementtime DESC";
            }

            //added by rohini for GTL#341 ---- END

            DataView Test_View = GetTopDataViewRows(view, 21);

            dataGridView1.DataSource = Test_View;

            rowCount = 0; // edited by rohini for  GSP-1381//int rowCount = 0;
            rowCount = view.Count;
            //added by rohini for GTL#248 ---START
            if (searchbysearch == true && textBox1.Text.Trim() == "")
            {
                rowCount = totalbysearch;
            }
            //added by rohini for GTL#248 ---END
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " " + Properties.Resources.RECORDS;  // added by rohini for  GSP-1381 
            TotalPage = rowCount / PgSize;
            // if any row left after calculated pages, add one more page 
            if (rowCount % PgSize > 0)
                TotalPage += 1;

            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            if (this.TotalPage == 0)
            {
                label3.Text = Properties.Resources.SHOWING + 0 + " - " + 0 + " of " + 0 + " " + Properties.Resources.RECORDS; // added by rohini for GSP-1381
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            }
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END
            //--Added by Rajnish for GSP-1235--start//
            if (TotalPage > 1)
            {
                //FirstPage.Enabled = true;
                //PreviousPage.Enabled = true;
                textbox_PageNo.Enabled = true;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            else
            {
                if (TotalPage == 1)
                {
                    FirstPage.Enabled = false;
                    PreviousPage.Enabled = false;
                    textbox_PageNo.Enabled = false;
                    NextPage.Enabled = false;
                    LastPage.Enabled = false;
                }
            }
            //--Added by Rajnish for GSP-1235--END//

            //Added by rohini for GTL#244 -- START
            if (textBox1.Text.Trim() == "")
            {
                CurrentPageIndex = 1;
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + CurrentPageIndex + @" / " + this.TotalPage;
                FirstPage.Enabled = false;
                PreviousPage.Enabled = false;
                textbox_PageNo.Enabled = false;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            //Added by rohini for GTL#244 -- END
        }
        private DataView GetTopDataViewRows(DataView dv, Int32 n)
        {
            DataTable dt = dv.Table.Clone();

            for (int i = 0; i < n - 1; i++)
            {
                if (i >= dv.Count)
                {
                    break;
                }
                dt.ImportRow(dv[i].Row);
            }
            return new DataView(dt, dv.RowFilter, dv.Sort, dv.RowStateFilter);
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            //--Added By Rajnish for GTL #442---Start--//
            var textboxSender = (TextBox)sender;
            var cursorPosition = textboxSender.SelectionStart;
            //textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, "[^0-9a-zA-Z ]", "");//commented by Rajnish for GSP-1413
            textboxSender.Text = System.Text.RegularExpressions.Regex.Replace(textboxSender.Text, @"[_]+|[^\w\s]+", "");//Added by Rajnish for GSP-1413
            textboxSender.SelectionStart = cursorPosition;
            //--Added By Rajnish for GTL #442---Start--//
            search = true; //added by rohini for  GSP-1381
            DataView view = new DataView(Records_dt);
            string str = string.Empty;
            if (textBox1.Text != "")
                str = "patientid like '%" + textBox1.Text + "%' and ";

            if (checkBox1.Checked == true)
            {
                str = str + "MeasurementTime >= '" +
                            dateTimePicker1.Value.ToString("yy-MM-dd") + "' and MeasurementTime <= '" +
                            dateTimePicker2.Value.ToString("yy-MM-dd 24:00") + "' and ";
            }

            view.RowFilter = str + "name Like '%" + textBox2.Text + "%'";
            view.Sort = "Measurementtime DESC";

            //added by rohini for GTL#341 ---- START
            string str1 = string.Empty;
            DataTable dtPcSelected = new DataTable();
            dtPcSelected = checkedListSearchControl1.GetSelectedData();
            if (dtPcSelected.Rows.Count > 0)
            {

                foreach (DataRow dr in dtPcSelected.Rows)
                {
                    str1 += dr["ID"] + ",";
                }
                str1 = str1.Remove(str1.Length - 1, 1);

                view.RowFilter = "name Like '%" + textBox2.Text + "%' and stall_id IN (" + str1 + ")";
                view.Sort = "Measurementtime DESC";
            }

            //added by rohini for GTL#341 ---- END

            DataView Test_View = GetTopDataViewRows(view, 21);

            dataGridView1.DataSource = Test_View;

            //int rowCount = 0;
            rowCount = 0; //edited by rohini for GSP-1381
            rowCount = view.Count;
            //added by rohini for GTL#248 ---START
            if (searchbysearch == true && textBox2.Text.Trim() == "")
            {
                rowCount = totalbysearch;
            }
            //added by rohini for GTL#248 ---END
            TotalPage = rowCount / PgSize;
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + rowCount.ToString() + " " + Properties.Resources.RECORDS;   // added by rohini for  GSP-1381 
            // if any row left after calculated pages, add one more page 
            if (rowCount % PgSize > 0)
                TotalPage += 1;

            //Edited by Sumit GSP-1117-------START
            //if(this.TotalPage == 0)
            //    textbox_PageNo.Text = "Page " + this.TotalPage + " of " + this.TotalPage;
            //else
            //    textbox_PageNo.Text = "Page " + this.CurrentPageIndex + " of " + this.TotalPage;
            //query = query + " order by Measurementtime desc limit 20";
            if (this.TotalPage == 0)
            {
                label3.Text = Properties.Resources.SHOWING + 0 + " - " + 0 + " of " + 0 + " " + Properties.Resources.RECORDS; // added by rohini for GSP-1381
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.TotalPage + @" / " + this.TotalPage;
            }
            else
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + this.CurrentPageIndex + @" / " + this.TotalPage;
            //GSP-1117----------END

            //--Added by Rajnish for GSP-1235--start//
            if (TotalPage > 1)
            {
                //FirstPage.Enabled = true;
                //PreviousPage.Enabled = true;
                textbox_PageNo.Enabled = true;
                NextPage.Enabled = true;
                LastPage.Enabled = true;
            }
            else
            {
                if (TotalPage == 1)
                {
                    FirstPage.Enabled = false;
                    PreviousPage.Enabled = false;
                    textbox_PageNo.Enabled = false;
                    NextPage.Enabled = false;
                    LastPage.Enabled = false;
                }
            }
            //--Added by Rajnish for GSP-1235--END//

            //Added by rohini for GTL#244 -- START
            if (textBox2.Text.Trim() == "")
            {

                CurrentPageIndex = 1;
                textbox_PageNo.Text = Properties.Resources.OPEN_RESULT_PAGE_OF + CurrentPageIndex + @" / " + this.TotalPage;
                FirstPage.Enabled = false;
                PreviousPage.Enabled = false;
                textbox_PageNo.Enabled = false;
                NextPage.Enabled = true;
                LastPage.Enabled = true;

            }
            //Added by rohini for GTL#244 -- END

        }

        //added by rohini for GSP-1381 --- START
        private void dataGridView1_RowsAdded(object sender, DataGridViewRowsAddedEventArgs e)
        {
            int count = Records_dt.Rows.Count;
            count1 = dataGridView1.Rows.Count;

            if (this.CurrentPageIndex == 1)
            {
                slno = 0;
                sum = count1 + 1 - count1;
                count2 = count1;

                if (this.TotalPage > 1)
                    total = count;

                //if (count1 < 20)
                //{
                //    total = count1;
                //}
                total = rowCount;
                if (checkBox1.Checked == true)
                {
                    total = rowCount;
                }
                else
                {
                    //total = count; commented by rohini for GTL#181
                    // added by rohini for GTL#181 -- START
                    if (total == 0)
                    {
                        total = count2;
                    }
                    else
                    {
                        total = count;
                    }
                    // added by rohini for GTL#181 --END
                }
            }
            else
            {
                if (TotalPage > 1)
                {
                    if (search == true)
                    {
                        slno = 0;

                        total = count1; count2 = count1 + slno;
                        if ((textBox1.Text == "") && (textBox2.Text == ""))
                        {
                            total = count;
                        }
                    }
                    else
                    {
                        slno = (CurrentPageIndex - 1) * PgSize;
                        total = count; count2 = count1 + slno;

                    }
                    if (checkBox1.Checked == true)
                    {
                        total = rowCount;
                    }
                    sum = slno + 1;
                }
                else
                {
                    slno = 0;
                    sum = count1 + 1 - count1;
                    total = count; count2 = count1;
                    if (this.TotalPage > 1)
                    {
                        total = count;
                    }
                    if (count1 < 20)
                        total = count1;
                }

            }
            for (int i = 0; i < count1; ++i)
            {

                dataGridView1.Rows[i].Cells["SrNo"].Value = slno + 1;
                slno++;
            }

            // added by rohini for GTL#347 -- START
            DataTable dtPcSelected = new DataTable();
            dtPcSelected = checkedListSearchControl1.GetSelectedData();
            if (dtPcSelected.Rows.Count > 0)
            {

                total = totalbysearch;

            }
            //  added by rohini for GTL#181 -- START
            if (searchbysearch == true)
            {
                total = totalbysearch;
                if (total <= count2)
                {
                    count2 = total;
                }
            }
            //  added by rohini for GTL#181 -- END
            //added by rohini for GTL#347 -- END
            label3.Text = Properties.Resources.SHOWING + sum.ToString() + " - " + count2.ToString() + " of " + total.ToString() + " " + Properties.Resources.RECORDS;
        }

        //event Added by Sumit GTL#229
        private void OpenResult_VisibleChanged(object sender, EventArgs e)
        {
            //Added by Sumit GTL#229   ---START
            //checkedListSearchControl1 = null;
            //checkedListSearchControl1 = new CustomControls.CheckedListSearchControl();
            ////Edited by Sumit GTL#251   ---START
            ////checkedListSearchControl1.FillData(PCNameList());
            //DataTable dtPCNameList = PCNameList();
            //if (dtPCNameList == null || dtPCNameList.Rows.Count == 0)
            //{
            //    checkedListSearchControl1.Enabled = false;
            //}
            //else
            //{
            //    checkedListSearchControl1.FillData(dtPCNameList);
            //    checkedListSearchControl1.Enabled = true;
            //}
            //Edited by Sumit GTL#251   ---END
            //checkedListSearchControl1.Form1_Load(null, null);




            //checkedListSearchControl1.textBox1.Text = "";
            //checkedListSearchControl1.textBox1.Refresh();
            //checkedListSearchControl1.textBox1.Clear();
            //checkedListSearchControl1.Refresh();
            //this.Refresh();
            //groupBox9.Refresh();

            //checkedListSearchControl1.Invalidate(true);
            //Added by Sumit GTL#229   ---END

            //Added by Sumit for GTL#254   ---START
            label3.Left = dataGridView1.Left + dataGridView1.Width - label3.Width + label3.Margin.Right;

            //label3.BackColor = Color.Green;
            //Added by Sumit for GTL#254   ---END

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        //added by rohini for GSP-1381 --- END

        public DataTable PCNameList()
        {
            DataTable dtInstallInfo = new DataTable();
            //commented by rohini for GSP-1481 ---start

            string query = "select stall_id as ID, Computer_name as Name from tblInstallInfo where Sync != 'FALSE'";
            CDatabase database = new CDatabase();

            dtInstallInfo = database.selectQuery(query);
            //commented by rohini for GSP-1481 --- end

            //dtInstallInfo = GetInstallInfoTable();  //added by rohini for GSP-1481
            return dtInstallInfo;
        }

        //Event added by Sumit for GTL#254   
        private void label3_TextChanged(object sender, EventArgs e)
        {
            //Added by Sumit for GTL#254   ---START
            label3.Left = dataGridView1.Left + dataGridView1.Width - label3.Width + label3.Margin.Right;

            //label3.BackColor = Color.Green;
            //Added by Sumit for GTL#254   ---END
        }

        static List<PC> lstPCsToSync = new List<PC>();

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            index = e.RowIndex;
        }

        private void btnGetLatest_Click(object sender, EventArgs e)
        {

            if (!WebComCation.Utility.IsInternetConnected())
            {
                //  MessageBox.Show("Internet connection is required for this operation."); //Commented byy rohini for GTL#294
                MessageBox.Show(Properties.Resources.INTERNET_CONNECTION_IS_REQUIRED_FOR_THIS_OPERATION); //Added by rohini for GTL#294
                return;
            }

            Cursor.Current = Cursors.WaitCursor;
            DataTable dtPcSelected = new DataTable();
            dtPcSelected = checkedListSearchControl1.GetSelectedData();
            if (dtPcSelected.Rows.Count > 0)
            {
                string str = string.Empty;
                foreach (DataRow dr in dtPcSelected.Rows)
                {
                    str += dr["ID"] + ",";
                }
                str = str.Remove(str.Length - 1, 1);
                bool success = false;

                SQLiteConnection sqlite;
                DataTable dt = new DataTable();
                SQLiteDataAdapter ad;
                string Server_User_ID = "";
                try
                {

                    //Edited by Sumit for GSP-1181   ---START
                    //sqlite = new SQLiteConnection("Data Source=" + Constants.db_file);
                    sqlite = Utility.SQLiteGetSecureCon("Data Source=" + Constants.db_file);
                    //Edited by Sumit for GSP-1181   ---END
                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = "select * from tblInstallInfo where stall_id IN (" + str + ")";  //set the passed query
                    ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(dt); //fill the datasource                
                    sqlite.Close();
                    lstPCsToSync = new List<PC>();

                    foreach (DataRow dr in dt.Rows)
                    {
                        PC pc = new PC();
                        pc.SyncID = dr["ID"].ToString();

                        pc.Comments = dr["Comment"].ToString();
                        pc.InstallationDate = dr["Date_of_Install"].ToString();
                        pc.InstallationName = dr["Installation_Name"].ToString();
                        pc.InstalledBy = dr["Installed_By"].ToString();
                        pc.Language = dr["Language"].ToString();
                        pc.MaxUserID = dr["Max_User_ID"].ToString();//Presently not updating DB
                        pc.PCName = dr["Computer_Name"].ToString();
                        pc.Reg_User_ID = dr["Reg_User_ID"].ToString();
                        pc.Stall_ID = dr["Stall_ID"].ToString();
                        pc.Synced = dr["Sync"].ToString();//For present logic will be true only but soon will need all PCs
                        lstPCsToSync.Add(pc);
                    }
                    success = true;

                    foreach (PC p in lstPCsToSync)
                    {
                        CloudSyncManager.SyncSelectedOne(p);
                    }
                    LoadDataGridWithDefaultValues();
                }
                catch (SQLiteException ex)
                {
                    throw ex;
                }
                finally
                {
                    Cursor.Current = Cursors.Default;
                }
            }
        }

        private void OpenResult_MouseUp(object sender, MouseEventArgs e)
        {
            //Event Added by Sumit GSP-1146
            //Added by Sumit GSP-1146-----START
            CloudManager.SynchronizationScheduler.lastActivityTime = DateTime.Now;
            //Added by Sumit GSP-1146-----START
        }

        /*Event Added by Rajnish for GSP-1488-----------------Start*/
        private void checkedListSearchControl1_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable dtPcSelected = new DataTable();
                dtPcSelected = checkedListSearchControl1.GetSelectedData();
                if (dtPcSelected.Rows.Count > 0)
                {
                    btnGetLatest.Enabled = true;
                }
                else
                {
                    btnGetLatest.Enabled = false;
                }
            }
            catch { }
        }

        private void checkedListSearchControl1_MouseClick(object sender, MouseEventArgs e)
        {

        }

        /*Event Added by Rajnish for GSP-1488-----------------END*/

        //Function Added by sumit for GTL#251
        public void Reload_PCs()
        {
            this.groupBox9.Controls.Clear();
            //CustomControls.CheckedListSearchControl checkedListSearchControl1 = new CustomControls.CheckedListSearchControl();
            checkedListSearchControl1 = new CustomControls.CheckedListSearchControl();
            DataTable dtPCNameList = null;
            if (!WebComCation.Utility.IsInternetConnected(5))//condition Added by Rajnish for GSP-1483
            {
                dtPCNameList = PCNameList();
            }
            else
            {
                dtPCNameList = GetInstallInfoTable();//Added by Rajnish for GSP-1481
            }


            if (dtPCNameList == null || dtPCNameList.Rows.Count == 0)
            {
                checkedListSearchControl1.Enabled = false;
            }
            else
            {
                checkedListSearchControl1.FillData(dtPCNameList);
                checkedListSearchControl1.Enabled = true;
            }
            checkedListSearchControl1.textBox1.Text = "";
            checkedListSearchControl1.textBox1.Refresh();
            checkedListSearchControl1.textBox1.Clear();
            checkedListSearchControl1.Refresh();
            //this.Refresh();
            groupBox9.Controls.Add(checkedListSearchControl1);
            checkedListSearchControl1.Dock = DockStyle.Fill;
            //groupBox9.Refresh();
        }

        /// <summary>
        /// Implemented by Sumit for GTL#458
        /// </summary>
        void SetDatePickers()
        {
            try
            {
                dateTimePicker1.ValueChanged -= new EventHandler(dateTimePicker1_ValueChanged);
                dateTimePicker2.ValueChanged -= new EventHandler(dateTimePicker2_ValueChanged);
                DateTime dtNow = DateTime.Now;
                dateTimePicker1.MaxDate = dtNow;
                dateTimePicker2.MaxDate = dtNow;
                dateTimePicker1.Value = dtNow;
                dateTimePicker2.Value = dtNow;
                dateTimePicker1.ValueChanged += new EventHandler(dateTimePicker1_ValueChanged);
                dateTimePicker2.ValueChanged += new EventHandler(dateTimePicker2_ValueChanged);
            }
            catch (Exception ex)
            {

#if debug
                MessageBox.Show(ex.StackTrace);
#endif
            }
        }


        //added by rohini for GSP-1481 --- start
        SQLiteConnection sqlite;
        public DataTable GetInstallInfoTable()
        {
            try
            {
                CloudManager.InstallInfoManager.SyncInstallInfo();
            }
            catch (Exception ex)
            {
                // MessageBox.Show("Working with old data" + Environment.NewLine + "No Internet.Cannot sync with latest data."); 
                MessageBox.Show(ex.Message);
#if debug
                    MessageBox.Show(ex.StackTrace);
#endif
            }

            DataTable dt = new DataTable();
            SQLiteDataAdapter ad;
            try
            {
                if (CloudManager.GlobalItems.NoOtherPCAvaialble)//--Condition Added by rajnish for GSP - 1486
                {
                    //--Added by rajnish for GSP - 1486----------Start
                    sqlite = CloudManager.Utility.SQLiteGetSecureCon("Data Source=" + WebComCation.Utility.db_file);
                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = " delete from tblInstallInfo where sync != 'THIS'";
                    cmd.ExecuteNonQuery();

                    cmd.CommandText = "select ID, computer_name as [Name] from tblInstallInfo where sync = 'THIS'";  //set the passed query
                    ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(dt); //fill the datasource
                    sqlite.Close();
                    //--Added by rajnish for GSP - 1486----------END
                }
                else
                {
                    sqlite = CloudManager.Utility.SQLiteGetSecureCon("Data Source=" + WebComCation.Utility.db_file);

                    SQLiteCommand cmd;
                    sqlite.Open();  //Initiate connection to the db
                    cmd = sqlite.CreateCommand();
                    cmd.CommandText = "select stall_id as ID, Computer_name as Name from tblInstallInfo where Sync != 'FALSE'"; /* "select * from tblInstallInfo";*/  //set the passed query*/
                    ad = new SQLiteDataAdapter(cmd);
                    ad.Fill(dt); //fill the datasource
                    sqlite.Close();
                }
            }
            catch (SQLiteException ex)
            {
                MessageBox.Show(ex.Message);
            }

            return dt;
        }
        //added by rohini for GSP-1481 --- End
    }
}
