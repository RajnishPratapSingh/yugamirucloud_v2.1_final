﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace Yugamiru
{
    //Class added by sumit on 25-Feb-2019 for general purpose use
    public static class UtilityGlobal
    {
        public static clsIDNameHandler ID_NAME_Handler;
        public static void Set_ID_NAME_Handler(string fullID, string fullName, string AppLanguage)
        {
            ID_NAME_Handler = new clsIDNameHandler(fullID, fullName, AppLanguage);
        }
        /// <summary>
        /// Added by Sumit GTL#118          
        /// </summary>
        public static void ReleaseMemoryNow()
        {
            System.Threading.Thread.Sleep(1);
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.WaitForFullGCComplete();
            GC.Collect();
        }

        /// <summary>
        /// Implemented By Sumit For GTL#130.
        /// This method returns 3 Char Month name Like Jan or Feb etc
        /// </summary>
        /// <param name="month">Sequence number of the month like 1 for jan and 12 for december etc.</param>
        /// <returns></returns>
        public static string GetShortMonthName(int month)
        {
            string strMonth = "";

            if (month == 1)
            {
                strMonth = "Jan";
            }
            else if (month == 2)
            {
                strMonth = "Feb";
            }
            else if (month == 3)
            {
                strMonth = "Mar";
            }
            else if (month == 4)
            {
                strMonth = "Apr";
            }
            else if (month == 5)
            {
                strMonth = "May";
            }
            else if (month == 6)
            {
                strMonth = "Jun";
            }
            else if (month == 7)
            {
                strMonth = "Jul";
            }
            else if (month == 8)
            {
                strMonth = "Aug";
            }
            else if (month == 9)
            {
                strMonth = "Sep";
            }
            else if (month == 10)
            {
                strMonth = "Oct";
            }
            else if (month == 11)
            {
                strMonth = "Nov";
            }
            else if (month == 12)
            {
                strMonth = "Dec";
            }

            return strMonth;
        }

        //added by rohini for GSP-1379 START
        public static void start_process()
        {
            Process process = new Process();
            process.Exited += Process_Exited;
            process.StartInfo.FileName = System.Windows.Forms.Application.StartupPath + "\\yugamiruPGB.exe";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;
            process.Start();
            process.CloseMainWindow();
        }


        private static void Process_Exited(object sender, EventArgs e)
        {


        }

        public static void end_process()
        {
            Process[] ps = Process.GetProcesses();
            foreach (Process kp in ps)
            {
                if (kp.ProcessName.ToUpper().Contains("YUGAMIRUPGB"))
                {
                    kp.CloseMainWindow();
                }
            }
        }

        public static void end_process1()
        {
            System.Diagnostics.Process[] allp = System.Diagnostics.Process.GetProcesses();
            foreach (System.Diagnostics.Process p in allp)
            {
                if (p.ProcessName.ToUpper().Contains("yugamiruPGB".ToUpper()))
                {
                    p.CloseMainWindow();
                }
            }
        }
        //added by rohini for GSP-1379 END

        /// <summary>
        /// Added by Sumit for GSP-1181
        /// Function returns the connection string as per solution build mode (DEBUG = nonProtected or  RELEASE = protected)
        /// </summary>
        /// <param name="dbpath"></param>
        /// <returns></returns>
        public static System.Data.SQLite.SQLiteConnection SQLiteGetSecureCon(string dbpath)
        {

            System.Data.SQLite.SQLiteConnection con;

#if DEBUG
            if (dbpath.ToUpper().Contains("Data Source".ToUpper()))
            {
                con = new System.Data.SQLite.SQLiteConnection(dbpath);
            }
            else
            {
                con = new System.Data.SQLite.SQLiteConnection("Data Source=" + dbpath);
            }

#else
            if(dbpath.ToUpper().Contains("Data Source".ToUpper()))
            {
            con = new System.Data.SQLite.SQLiteConnection(dbpath.Replace(";", "") + ";Version=3;password=" + ConvertToEncrypt("6G5s4Tdf45sT") + ";");
            }
            else
            {
            con = new System.Data.SQLite.SQLiteConnection("Data Source=" + dbpath.Replace(";", "") + ";Version=3;password=" + ConvertToEncrypt("6G5s4Tdf45sT") + ";");
            }
            
#endif
            return con;

        }
        public static string ConvertToEncrypt(string valueToEncrypt)
        {
            byte[] bt = UTF8Encoding.UTF8.GetBytes(valueToEncrypt);
            string s = Convert.ToBase64String(bt);
            return s;
        }
    }


}
